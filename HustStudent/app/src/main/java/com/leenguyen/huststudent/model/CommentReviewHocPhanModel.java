package com.leenguyen.huststudent.model;

public class CommentReviewHocPhanModel {
    private String name, content, timeStamp;
    private int like;

    public CommentReviewHocPhanModel() {
    }

    public CommentReviewHocPhanModel(String name, String content, int like, String timeStamp) {
        this.name = name;
        this.content = content;
        this.like = like;
        this.timeStamp = timeStamp;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }
}
