package com.leenguyen.huststudent.menu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.LichThiAdapter;
import com.leenguyen.huststudent.databinding.ActivityLichThiBinding;
import com.leenguyen.huststudent.databinding.DialogThemLichThiBinding;
import com.leenguyen.huststudent.databinding.DialogThongTinLichThiHocPhanBinding;
import com.leenguyen.huststudent.model.LichThiModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class LichThiActivity extends AppCompatActivity {
    private ActivityLichThiBinding binding;
    private LichThiAdapter adapter;
    private ArrayList<LichThiModel> listLichThi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lich_thi);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        boolean isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVipMember && !isNoAds) {
            binding.adView.setVisibility(View.VISIBLE);
            initAdsBanner();
        } else {
            binding.adView.setVisibility(View.GONE);
        }
        initLayout();
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    private void initLayout() {
        binding.btnHienThiFull.setChecked(false);
        binding.txtTitle.setText("Lịch thi của thím");
        binding.edtSearch.setVisibility(View.GONE);
        binding.txtChuY.setVisibility(View.VISIBLE);
        showLichThi();
        binding.btnBack.setOnClickListener(v -> onBackPressed());

        binding.lvLichThi.setOnItemClickListener((parent, view, position, id) -> showDialogThongTinChiTiet(position));

        binding.btnHienThiFull.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                binding.txtTitle.setText("Lịch thi toàn trường");
                showFullLichThi();
                binding.edtSearch.setVisibility(View.VISIBLE);
                binding.txtChuY.setVisibility(View.GONE);
                binding.fabAddLichThi.setVisibility(View.GONE);
            } else {
                binding.txtTitle.setText("Lịch thi của thím");
                showLichThi();
                binding.edtSearch.setVisibility(View.GONE);
                binding.txtChuY.setVisibility(View.VISIBLE);
                binding.fabAddLichThi.setVisibility(View.VISIBLE);
            }
        });

        binding.edtSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (binding.edtSearch.getText() != null) {
                    searchLichThi(binding.edtSearch.getText().toString().trim());
                }
                hideKeyboard(LichThiActivity.this);
                return true;
            }
            return false;
        });

        binding.fabAddLichThi.setOnClickListener(v -> showDialogAddLichThi());
    }

    private void showDialogAddLichThi() {
        Dialog dialog = new Dialog(this);
        DialogThemLichThiBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_them_lich_thi, null, false);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(bindingDialog.getRoot());

        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());
        bindingDialog.btnLuu.setOnClickListener(v -> {
            String lichThiTuThem = Utils.getInstance().getValueFromSharedPreferences(LichThiActivity.this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LICH_THI_TU_THEM);
            try {
                JSONArray lichThiTuThemNewJsonA = new JSONArray();
                if (lichThiTuThem != null && !lichThiTuThem.equals("")) {
                    JSONArray lichThiTuThemJsonA = new JSONArray(lichThiTuThem);
                    if (lichThiTuThemJsonA.length() > 0) {
                        for (int i = 0; i < lichThiTuThemJsonA.length(); i++) {
                            JSONObject object = lichThiTuThemJsonA.getJSONObject(i);
                            if (!object.getString(JsonUtils.KEY_MA_LOP).equals(bindingDialog.edtMaLop.getText().toString().trim())) {
                                lichThiTuThemNewJsonA.put(object);
                            }
                        }
                    }
                }

                JSONObject object = new JSONObject();
                object.put(JsonUtils.KEY_VIEN, "");
                object.put(JsonUtils.KEY_MA_LOP, bindingDialog.edtMaLop.getText().toString().trim());
                object.put(JsonUtils.KEY_MA_HOC_PHAN, "");
                object.put(JsonUtils.KEY_TEN_HOC_PHAN, bindingDialog.edtTenHocPhan.getText().toString().trim());
                object.put(JsonUtils.KEY_GHI_CHU, "");
                object.put(JsonUtils.KEY_NHOM, "");
                object.put(JsonUtils.KEY_DOT_MO, "");
                object.put(JsonUtils.KEY_TUAN, "");
                object.put(JsonUtils.KEY_THU, bindingDialog.edtThoiGian.getText().toString().trim());
                object.put(JsonUtils.KEY_NGAY_THI, "");
                object.put(JsonUtils.KEY_KIP_THI, bindingDialog.edtKipThi.getText().toString().trim());
                object.put(JsonUtils.KEY_SO_LUONG_DANG_KY, "");
                object.put(JsonUtils.KEY_PHONG_THI, bindingDialog.edtPhongThi.getText().toString().trim());
                lichThiTuThemNewJsonA.put(object);
                Utils.getInstance().saveToSharedPreferences(LichThiActivity.this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LICH_THI_TU_THEM, lichThiTuThemNewJsonA.toString());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                dialog.dismiss();
                showLichThi();
            }
        });
        dialog.show();
    }

    private void searchLichThi(String toString) {
        try {
            boolean isHasLichThi = false;
            if (listLichThi != null && listLichThi.size() > 0) {
                for (int i = 0; i < listLichThi.size(); i++) {
                    LichThiModel lichThiModel = listLichThi.get(i);
                    if (lichThiModel.getMaLop().trim().contains(toString)) {
                        isHasLichThi = true;
                        binding.lvLichThi.smoothScrollToPosition(i);
                        adapter.setHighLightPosition(i);
                        adapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
            if (!isHasLichThi) {
                Toast.makeText(this, getString(R.string.khong_tim_thay_lich_thi), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.d("RAKAN", "searchLichThi: " + e.toString());
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.khong_tim_thay_lich_thi), Toast.LENGTH_LONG).show();
        }
    }

    private static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void showDialogThongTinChiTiet(int position) {
        Dialog dialog = new Dialog(this);
        DialogThongTinLichThiHocPhanBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_thong_tin_lich_thi_hoc_phan, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(bindingDialog.getRoot());

        LichThiModel lichThiModel = listLichThi.get(position);
        bindingDialog.txtTenHocPhan.setText(lichThiModel.getTenHocPhan());
        bindingDialog.txtVien.setText("Viện: " + lichThiModel.getVien());
        bindingDialog.txtMaLop.setText("Mã lớp: " + lichThiModel.getMaLop());
        bindingDialog.txtMaHocPhan.setText("Mã học phần: " + lichThiModel.getMaHocPhan());
        bindingDialog.txtGhiChu.setText("Ghi chú: " + lichThiModel.getGhiChu());
        bindingDialog.txtNhom.setText("Nhóm: " + lichThiModel.getNhom());
        bindingDialog.txtDotMo.setText("Đợt mở: " + lichThiModel.getDotMo());
        bindingDialog.txtTuan.setText("Tuần: " + lichThiModel.getTuan());
        bindingDialog.txtThu.setText("Thứ: " + lichThiModel.getThu());
        bindingDialog.txtNgayThi.setText("Ngày thi: " + lichThiModel.getNgayThi());
        bindingDialog.txtKipThi.setText("Kíp thi: " + lichThiModel.getKipThi());
        bindingDialog.txtSoLuongDangKy.setText("Số lượng ĐK: " + lichThiModel.getSoLuongDangKy());
        bindingDialog.txtPhongThi.setText("Phòng thi: " + lichThiModel.getPhongThi());
        bindingDialog.txtTenHocPhan.setSelected(true);
        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());
        if (lichThiModel.getVien().equals("") &&
                lichThiModel.getMaHocPhan().equals("") &&
                lichThiModel.getGhiChu().equals("") &&
                lichThiModel.getNhom().equals("") &&
                lichThiModel.getDotMo().equals("")) {
            bindingDialog.loButton.setVisibility(View.VISIBLE);
        } else {
            bindingDialog.loButton.setVisibility(View.GONE);
        }
        bindingDialog.btnCancel.setOnClickListener(v -> dialog.dismiss());
        bindingDialog.btnContinues.setOnClickListener(v -> {
            xoaLichThi(lichThiModel.getMaLop());
            dialog.dismiss();
        });
        dialog.show();
    }

    private void xoaLichThi(String maLop) {
        String dataLichThiTuThem = Utils.getInstance().getValueFromSharedPreferences(LichThiActivity.this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LICH_THI_TU_THEM);
        try {
            JSONArray lichThiTuThemJsonA = new JSONArray(dataLichThiTuThem);
            JSONArray lichThiTuThemJsonANew = new JSONArray();
            for (int i = 0; i < lichThiTuThemJsonA.length(); i++) {
                JSONObject object = lichThiTuThemJsonA.getJSONObject(i);
                if (!object.getString(JsonUtils.KEY_MA_LOP).equals(maLop)) {
                    lichThiTuThemJsonANew.put(object);
                }
            }
            Utils.getInstance().saveToSharedPreferences(LichThiActivity.this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LICH_THI_TU_THEM, lichThiTuThemJsonANew.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            showLichThi();
        }

    }

    private void showLichThi() {
        String dataLichThi = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LICH_THI_FULL);
        String dataLichThiTuThem = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LICH_THI_TU_THEM);
        String dataLichSaved = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LICH_THI_SAVED);
        String dataTkb = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_TKB);
        listLichThi = new ArrayList<>();
        listLichThi.clear();
        try {
            JSONArray tkbJsonA = new JSONArray(dataTkb);
            JSONArray lichThiListJsonA = new JSONArray(dataLichThi);
            if (lichThiListJsonA.length() > 0) {
                if (tkbJsonA.length() > 0) {
                    for (int i = 0; i < tkbJsonA.length(); i++) {
                        JSONObject tkbHocPhanJsonO = tkbJsonA.getJSONObject(i);
                        String maLopHoc = tkbHocPhanJsonO.getString(JsonUtils.MA_LOP).trim();
                        String nhom = tkbHocPhanJsonO.getString(JsonUtils.NHOM).trim();
                        for (int j = 0; j < lichThiListJsonA.length(); j++) {
                            JSONObject lichThiJsonO = lichThiListJsonA.getJSONObject(j);
                            if (maLopHoc.equals(lichThiJsonO.getString(JsonUtils.KEY_MA_LOP).trim()) &&
                                    nhom.equals(lichThiJsonO.getString(JsonUtils.KEY_NHOM).trim())) {
                                JSONObject lichThiSVJsonO = lichThiListJsonA.getJSONObject(j);
                                addLichThiHocPhanToListLichThi(lichThiSVJsonO);
                            }
                        }
                    }
                    Utils.getInstance().saveToSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LICH_THI_SAVED, listLichThi.toString());
                } else if (dataLichSaved != null && !dataLichSaved.equals("")) {
                    JSONArray lichThiListSavedJsonA = new JSONArray(dataLichSaved);
                    for (int i = 0; i < lichThiListSavedJsonA.length(); i++) {
                        JSONObject lichThiJsonO = lichThiListSavedJsonA.getJSONObject(i);
                        addLichThiHocPhanToListLichThi(lichThiJsonO);
                    }
                } else {
                    //Khi TKB bi xoa va k co lich thi nao duoc luu
                    String dataKiemTraNhapDiem = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_KIEM_TRA_NHAP_DIEM_KI_MOI);
                    JSONArray kiemTraNhapDiemJsonA = new JSONArray(dataKiemTraNhapDiem);
                    for (int i = 0; i < kiemTraNhapDiemJsonA.length(); i++) {
                        JSONObject diemMonMoiJsonO = kiemTraNhapDiemJsonA.getJSONObject(i);
                        String maLopHoc = diemMonMoiJsonO.getString(JsonUtils.MA_LOP).trim();
//                        String nhom = diemMonMoiJsonO.getString(JsonUtils.NHOM).trim();
                        for (int j = 0; j < lichThiListJsonA.length(); j++) {
                            JSONObject lichThiJsonO = lichThiListJsonA.getJSONObject(j);
                            if (maLopHoc.equals(lichThiJsonO.getString(JsonUtils.KEY_MA_LOP).trim())) {
                                JSONObject lichThiSVJsonO = lichThiListJsonA.getJSONObject(j);
                                addLichThiHocPhanToListLichThi(lichThiSVJsonO);
                            }
                        }
                    }
                }
            }

            JSONArray lichThiTuThemListJsonA = new JSONArray(dataLichThiTuThem);
            if (lichThiTuThemListJsonA.length() > 0) {
                for (int i = 0; i < lichThiTuThemListJsonA.length(); i++) {
                    JSONObject lichThiSVJsonO = lichThiTuThemListJsonA.getJSONObject(i);
                    addLichThiHocPhanToListLichThi(lichThiSVJsonO);
                }
            }
        } catch (Exception e) {
            Log.e("RAKAN", "showLichThi: " + e.toString());
            e.printStackTrace();
        } finally {
            if (listLichThi.size() > 0) {
                binding.lvLichThi.setVisibility(View.VISIBLE);
                binding.loKhongCoDuLieu.setVisibility(View.GONE);
                adapter = new LichThiAdapter(this, R.layout.item_lich_thi, listLichThi);
                binding.lvLichThi.setAdapter(adapter);
            } else {
                binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
                Glide.with(this).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
                binding.lvLichThi.setVisibility(View.GONE);
            }
        }
    }

    private void addLichThiHocPhanToListLichThi(JSONObject lichThiSVJsonO) {
        try {
            LichThiModel lichThiModel = new LichThiModel(
                    lichThiSVJsonO.getString(JsonUtils.KEY_VIEN),
                    lichThiSVJsonO.getString(JsonUtils.KEY_MA_LOP),
                    lichThiSVJsonO.getString(JsonUtils.KEY_MA_HOC_PHAN),
                    lichThiSVJsonO.getString(JsonUtils.KEY_TEN_HOC_PHAN),
                    lichThiSVJsonO.getString(JsonUtils.KEY_GHI_CHU),
                    lichThiSVJsonO.getString(JsonUtils.KEY_NHOM),
                    lichThiSVJsonO.getString(JsonUtils.KEY_DOT_MO),
                    lichThiSVJsonO.getString(JsonUtils.KEY_TUAN),
                    lichThiSVJsonO.getString(JsonUtils.KEY_THU),
                    lichThiSVJsonO.getString(JsonUtils.KEY_NGAY_THI),
                    lichThiSVJsonO.getString(JsonUtils.KEY_KIP_THI),
                    lichThiSVJsonO.getString(JsonUtils.KEY_SO_LUONG_DANG_KY),
                    lichThiSVJsonO.getString(JsonUtils.KEY_PHONG_THI));
            listLichThi.add(lichThiModel);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("initLichThiList", e.toString());
        }
    }

    private void showFullLichThi() {
        String dataLichThi = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LICH_THI_FULL);
        listLichThi = new ArrayList<>();
        listLichThi.clear();
        try {
            JSONArray lichThiListJsonA = new JSONArray(dataLichThi);
            for (int j = 0; j < lichThiListJsonA.length(); j++) {
                JSONObject lichThiSVJsonO = lichThiListJsonA.getJSONObject(j);
                addLichThiHocPhanToListLichThi(lichThiSVJsonO);
            }

            if (listLichThi.size() > 0) {
                binding.lvLichThi.setVisibility(View.VISIBLE);
                binding.loKhongCoDuLieu.setVisibility(View.GONE);
                adapter = new LichThiAdapter(this, R.layout.item_lich_thi, listLichThi);
                binding.lvLichThi.setAdapter(adapter);
            } else {
                binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
                Glide.with(this).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
                binding.lvLichThi.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e("RAKAN", "showFullLichThi: " + e.toString());
            e.printStackTrace();
        }
    }
}