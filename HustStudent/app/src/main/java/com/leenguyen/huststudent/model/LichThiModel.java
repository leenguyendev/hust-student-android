package com.leenguyen.huststudent.model;

public class LichThiModel {
    private String vien, maLop, maHocPhan, tenHocPhan, ghiChu, nhom, dotMo, tuan, thu, ngayThi, kipThi, soLuongDangKy, phongThi;

    public LichThiModel() {
    }

    public LichThiModel(String vien, String maLop, String maHocPhan, String tenHocPhan, String ghiChu, String nhom, String dotMo, String tuan, String thu, String ngayThi, String kipThi, String soLuongDangKy, String phongThi) {
        this.vien = vien;
        this.maLop = maLop;
        this.maHocPhan = maHocPhan;
        this.tenHocPhan = tenHocPhan;
        this.ghiChu = ghiChu;
        this.nhom = nhom;
        this.dotMo = dotMo;
        this.tuan = tuan;
        this.thu = thu;
        this.ngayThi = ngayThi;
        this.kipThi = kipThi;
        this.soLuongDangKy = soLuongDangKy;
        this.phongThi = phongThi;
    }

    public String getVien() {
        return vien;
    }

    public void setVien(String vien) {
        this.vien = vien;
    }

    public String getMaLop() {
        return maLop;
    }

    public void setMaLop(String maLop) {
        this.maLop = maLop;
    }

    public String getMaHocPhan() {
        return maHocPhan;
    }

    public void setMaHocPhan(String maHocPhan) {
        this.maHocPhan = maHocPhan;
    }

    public String getTenHocPhan() {
        return tenHocPhan;
    }

    public void setTenHocPhan(String tenHocPhan) {
        this.tenHocPhan = tenHocPhan;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getNhom() {
        return nhom;
    }

    public void setNhom(String nhom) {
        this.nhom = nhom;
    }

    public String getDotMo() {
        return dotMo;
    }

    public void setDotMo(String dotMo) {
        this.dotMo = dotMo;
    }

    public String getTuan() {
        return tuan;
    }

    public void setTuan(String tuan) {
        this.tuan = tuan;
    }

    public String getThu() {
        return thu;
    }

    public void setThu(String thu) {
        this.thu = thu;
    }

    public String getNgayThi() {
        return ngayThi;
    }

    public void setNgayThi(String ngayThi) {
        this.ngayThi = ngayThi;
    }

    public String getKipThi() {
        return kipThi;
    }

    public void setKipThi(String kipThi) {
        this.kipThi = kipThi;
    }

    public String getSoLuongDangKy() {
        return soLuongDangKy;
    }

    public void setSoLuongDangKy(String soLuongDangKy) {
        this.soLuongDangKy = soLuongDangKy;
    }

    public String getPhongThi() {
        return phongThi;
    }

    public void setPhongThi(String phongThi) {
        this.phongThi = phongThi;
    }
}
