package com.leenguyen.huststudent.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.DiemThiToeicAdapter;
import com.leenguyen.huststudent.databinding.ActivityDiemThiToeicBinding;
import com.leenguyen.huststudent.model.DiemThiToeicModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class DiemThiToeicActivity extends AppCompatActivity {
    private ActivityDiemThiToeicBinding binding;
    private ArrayList<DiemThiToeicModel> diemThiToeicModels;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_diem_thi_toeic);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        boolean isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVipMember && !isNoAds) {
            initAdsBanner();
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            binding.loAds.setLayoutParams(layoutParams);
        }
        initLayout();
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    private void initLayout() {
        showListDiemThiToeic();
        binding.btnBack.setOnClickListener(view -> onBackPressed());
        binding.lvDiemThiToeic.setOnItemClickListener((adapterView, view, i, l) -> showDetailDialog(diemThiToeicModels.get(i)));
    }

    @SuppressLint("SetTextI18n")
    private void showDetailDialog(DiemThiToeicModel diemThiToeicModel) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_diem_toeic_detail);
        TextView txtMaSV = dialog.findViewById(R.id.txtMaSV);
        TextView txtHoTen = dialog.findViewById(R.id.txtHoTenSV);
        TextView txtNgaySinh = dialog.findViewById(R.id.txtNgaySinhSV);
        TextView txtGhiChu = dialog.findViewById(R.id.txtGhiChu);
        TextView txtNgayThi = dialog.findViewById(R.id.txtNgayThi);
        TextView txtDiemDoc = dialog.findViewById(R.id.txtDiemDoc);
        TextView txtDiemNghe = dialog.findViewById(R.id.txtDiemNghe);
        TextView txtDiemTong = dialog.findViewById(R.id.txtDiemTong);
        TextView txtHocKy = dialog.findViewById(R.id.txtHocKy);
        ImageView btnClose = dialog.findViewById(R.id.btnClose);

        txtMaSV.setText("Mã SV: " + diemThiToeicModel.getMaSV());
        txtHoTen.setText("Họ tên: " + diemThiToeicModel.getHoTen());
        txtNgaySinh.setText("Ngày sinh: " + diemThiToeicModel.getNgaySinh());
        txtGhiChu.setText("Ghi chú: " + diemThiToeicModel.getGhiChu());
        txtNgayThi.setText("Ngày thi: " + diemThiToeicModel.getNgayThi());
        txtDiemDoc.setText("Điểm đọc: " + diemThiToeicModel.getDiemDoc());
        txtDiemNghe.setText("Điểm nghe: " + diemThiToeicModel.getDiemNghe());
        txtDiemTong.setText("Điểm tổng: " + diemThiToeicModel.getDiemTong());
        txtHocKy.setText("Học kỳ: " + diemThiToeicModel.getHocKy());
        btnClose.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    private void showListDiemThiToeic() {
        String dataDiemThiToeic = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_DATA_DIEM_THI_TOEIC);
        try {
            diemThiToeicModels = new ArrayList<>();
            JSONArray diemThiToeicJsonA = new JSONArray(dataDiemThiToeic);
            if (diemThiToeicJsonA.length() > 0) {
                for (int i = 0; i < diemThiToeicJsonA.length(); i++) {
                    JSONObject object = diemThiToeicJsonA.getJSONObject(i);
                    String maSV = object.getString(JsonUtils.KEY_MA_SV);
                    String hoTen = object.getString(JsonUtils.KEY_HO_TEN_SV);
                    String hocKy = object.getString(JsonUtils.KEY_HOC_KY);
                    String ghiChu = object.getString(JsonUtils.KEY_GHI_CHU);
                    String ngayThi = object.getString(JsonUtils.KEY_NGAY_THI);
                    String diemNghe = object.getString(JsonUtils.KEY_DIEM_NGHE);
                    String diemDoc = object.getString(JsonUtils.KEY_DIEM_DOC);
                    String diemTong = object.getString(JsonUtils.KEY_DIEM_TONG);
                    String ngaySinh = object.getString(JsonUtils.KEY_NGAY_SINH_SV);
                    DiemThiToeicModel diemThiToeicModel = new DiemThiToeicModel(maSV, hoTen, ngaySinh, hocKy, ghiChu, ngayThi, diemNghe, diemDoc, diemTong);
                    diemThiToeicModels.add(diemThiToeicModel);
                }
                DiemThiToeicAdapter diemThiToeicAdapter = new DiemThiToeicAdapter(this, R.layout.item_diem_thi_toeic, diemThiToeicModels);
                binding.lvDiemThiToeic.setAdapter(diemThiToeicAdapter);

                binding.loKhongCoDuLieu.setVisibility(View.GONE);
                binding.loStickerHeader.setVisibility(View.VISIBLE);
                Glide.with(this).load(R.drawable.ic_ami_no).into(binding.imgStickerHeader);
            } else {
                binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
                Glide.with(this).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
                binding.loStickerHeader.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e("DiemThiToeicActivity", "showListDiemThiToeic: " + e.toString());
        }
    }
}
