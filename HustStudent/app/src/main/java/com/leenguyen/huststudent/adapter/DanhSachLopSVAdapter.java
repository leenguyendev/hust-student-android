package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.SinhVienInfoModel;

import java.util.ArrayList;

public class DanhSachLopSVAdapter extends ArrayAdapter<SinhVienInfoModel> {
    private Context context;
    private ArrayList<SinhVienInfoModel> sinhVienInfoModels;
    private int layoutResource;

    public DanhSachLopSVAdapter(@NonNull Context context, int resource, @NonNull ArrayList<SinhVienInfoModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.sinhVienInfoModels = objects;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtHoTenSV = row.findViewById(R.id.txtHoTenSV);
        TextView txtMaSV = row.findViewById(R.id.txtMaSV);
        TextView txtTrangThai = row.findViewById(R.id.txtTrangThai);
        TextView txtSTT = row.findViewById(R.id.txtSTT);

        txtSTT.setText(String.valueOf(position + 1));
        txtMaSV.setText(sinhVienInfoModels.get(position).getMaSV());
        txtHoTenSV.setText(sinhVienInfoModels.get(position).getHoSV() + " " +
                sinhVienInfoModels.get(position).getDemSV() + " " +
                sinhVienInfoModels.get(position).getTenSV());
        txtTrangThai.setText(sinhVienInfoModels.get(position).getTrangThaiHocTapSV());
        return row;
    }
}
