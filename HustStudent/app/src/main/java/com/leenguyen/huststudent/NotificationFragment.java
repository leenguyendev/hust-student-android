package com.leenguyen.huststudent;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bumptech.glide.Glide;
import com.leenguyen.huststudent.databinding.FragmentNotificationBinding;
import com.leenguyen.huststudent.utils.Utils;

public class NotificationFragment extends Fragment {
    private FragmentNotificationBinding binding;
    private boolean isFABOpen = false;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
        initLayout();
        return binding.getRoot();
    }

    private void initLayout() {
        openWebView(Constant.URL_CTSV);
        binding.fabSelectWeb.setOnClickListener(v -> {
            if (!isFABOpen) {
                showFabMenu();
            } else {
                closeFABMenu();
            }
        });

        binding.loFab1.setOnClickListener(v -> openWebView(Constant.URL_CTT_DT));

        binding.loFab2.setOnClickListener(v -> openWebView(Constant.URL_CTSV));

        binding.loFab3.setOnClickListener(v -> openWebView(Constant.URL_HUST));

        binding.fabBGLayout.setOnClickListener(v -> closeFABMenu());
    }

    private void showFabMenu() {
        isFABOpen = true;
        binding.fabBGLayout.setVisibility(View.VISIBLE);
        binding.fabSelectWeb.animate().rotationBy(45);
        binding.loFab1.setVisibility(View.VISIBLE);
        binding.loFab2.setVisibility(View.VISIBLE);
        binding.loFab3.setVisibility(View.VISIBLE);
        binding.loFab1.animate().translationY(-getResources().getDimension(R.dimen.standard_60));
        binding.loFab2.animate().translationY(-getResources().getDimension(R.dimen.standard_120));
        binding.loFab3.animate().translationY(-getResources().getDimension(R.dimen.standard_180));
    }

    private void closeFABMenu() {
        isFABOpen = false;
        binding.fabBGLayout.setVisibility(View.GONE);
        binding.loFab1.animate().translationY(0);
        binding.loFab2.animate().translationY(0);
        binding.loFab3.animate().translationY(0);
        binding.fabSelectWeb.animate().rotation(0);
        binding.loFab3.animate().translationY(0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (!isFABOpen) {
                    binding.loFab1.setVisibility(View.GONE);
                    binding.loFab2.setVisibility(View.GONE);
                    binding.loFab3.setVisibility(View.GONE);
                }
            }
        });

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void openWebView(String url) {
        Utils.getInstance().showLoadingDialog(requireContext());
        if (isFABOpen) {
            closeFABMenu();
        }
        if (Utils.getInstance().isOnline(requireContext())) {
            binding.loKhongCoDuLieu.setVisibility(View.GONE);
        } else {
            binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
            Glide.with(requireContext()).load(R.drawable.ic_ami_cry).into(binding.imgSticker);
        }
        binding.webView.setWebChromeClient(new WebViewChromeClient());
        binding.webView.setWebViewClient(new WebClient());
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setLoadWithOverviewMode(true);
        binding.webView.getSettings().setDomStorageEnabled(true);
        binding.webView.getSettings().setUseWideViewPort(true);
        binding.webView.loadUrl(url);
    }


    private class WebViewChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            binding.progressBar.setProgress(newProgress);
            if (newProgress == 100) {
                Utils.getInstance().hideLoadingDialog();
            }
        }
    }

    public class WebClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            binding.progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            binding.progressBar.setVisibility(View.GONE);
        }
    }
}
