package com.leenguyen.huststudent.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.CacLopDangKyAdapter;
import com.leenguyen.huststudent.adapter.TKBTamThoiAdapter;
import com.leenguyen.huststudent.databinding.ActivityTBKTamThoiBinding;
import com.leenguyen.huststudent.databinding.DialogHocPhanDangKyBinding;
import com.leenguyen.huststudent.model.CacLopDangKyModel;
import com.leenguyen.huststudent.model.TKBTamThoiModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TKBTamThoiActivity extends AppCompatActivity {
    private ActivityTBKTamThoiBinding binding;

    private ArrayList<CacLopDangKyModel> cacLopDangKyModels;
    private CacLopDangKyAdapter cacLopDangKyAdapter;

    private ArrayList<TKBTamThoiModel> tkbTamThoiModels;
    private TKBTamThoiAdapter tkbTamThoiAdapter;

    private JSONArray cacLopDangKyJsonA;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_t_b_k_tam_thoi);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        boolean isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVipMember && !isNoAds) {
            initAdsBanner();
        } else {
            binding.adView.setVisibility(View.GONE);
        }
        initLayout();
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    private void initLayout() {
        binding.btnBack.setOnClickListener(view -> onBackPressed());
        String dataTKBTamThoi = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_TKB_TAM_THOI);
        if (dataTKBTamThoi != null && !dataTKBTamThoi.equals("")) {
            try {
                JSONObject dataTKBTamThoiJsonO = new JSONObject(dataTKBTamThoi);
                cacLopDangKyJsonA = dataTKBTamThoiJsonO.getJSONArray(JsonUtils.KEY_CAC_LOP_DANG_KY);
                JSONArray tkbCacLopDangKyJsonA = dataTKBTamThoiJsonO.getJSONArray(JsonUtils.KEY_TKB_CAC_LOP_DANG_KY);
                if (cacLopDangKyJsonA.length() > 0) {
                    showListCacLopDangKy(cacLopDangKyJsonA);
                    binding.loKhongCoLopDangKy.setVisibility(View.GONE);
                } else {
                    binding.loKhongCoLopDangKy.setVisibility(View.VISIBLE);
                    Glide.with(this).load(R.drawable.ic_ami_ok).into(binding.imgStickerKhongCoLopDangKy);
                }
                if (tkbCacLopDangKyJsonA.length() > 0) {
                    showListTKBCacLopDangKy(tkbCacLopDangKyJsonA);
                    binding.loKhongCoTKBCaLopDangKy.setVisibility(View.GONE);
                } else {
                    binding.loKhongCoTKBCaLopDangKy.setVisibility(View.VISIBLE);
                    Glide.with(this).load(R.drawable.ic_ami_ok).into(binding.imgStickerKhongCoTKBCacLopDangKy);
                }

                if (cacLopDangKyJsonA.length() == 0 && tkbCacLopDangKyJsonA.length() == 0) {
                    binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
                    Glide.with(this).load(R.drawable.ic_ami_ok).into(binding.imgStickerKhongCoDuLieu);
                    binding.loTKBTamThoi.setVisibility(View.GONE);
                } else {
                    binding.loKhongCoDuLieu.setVisibility(View.GONE);
                    binding.loTKBTamThoi.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        binding.lvHocPhanDangKy.setOnItemClickListener((parent, view, position, id) -> showDialogDetailHPDangKy(cacLopDangKyModels.get(position)));

        binding.lvLichHoc.setOnItemClickListener((parent, view, position, id) -> showToastTenHocPhan(tkbTamThoiModels.get(position).getLopHoc()));
    }

    private void showToastTenHocPhan(String lopHoc) {
        for (int i = 0; i < cacLopDangKyJsonA.length(); i++) {
            try {
                JSONObject lopDangKyJsonO = cacLopDangKyJsonA.getJSONObject(i);
                String maLop = lopDangKyJsonO.getString(JsonUtils.KEY_MA_LOP);
                if (maLop.equals(lopHoc)) {
                    Toast.makeText(this, lopDangKyJsonO.getString(JsonUtils.KEY_TEN_LOP), Toast.LENGTH_LONG).show();
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void showDialogDetailHPDangKy(CacLopDangKyModel lopDangKyModel) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final DialogHocPhanDangKyBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_hoc_phan_dang_ky, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        bindingDialog.txtTenHP.setText(lopDangKyModel.getTenLop());
        bindingDialog.txtMaLop.setText("Mã lớp: " + lopDangKyModel.getMaLop());
        bindingDialog.txtMaLopKem.setText("Mã lớp kèm: " + lopDangKyModel.getMaLopKem());
        bindingDialog.txtMaHP.setText("Mã HP: " + lopDangKyModel.getMaHP());
        bindingDialog.txtLoaiLop.setText("Loại lớp: " + lopDangKyModel.getLoaiLop());
        bindingDialog.txtThongTinLop.setText("Thông tin lớp: " + lopDangKyModel.getThongTinLop());
        bindingDialog.txtYeuCau.setText("Yêu cầu: " + lopDangKyModel.getYeuCau());
        bindingDialog.txtTrangThaiDangKy.setText("Trạng thái ĐK: " + lopDangKyModel.getTrangThaiDK());
        bindingDialog.txtLoaiDangKy.setText("Loại ĐK: " + lopDangKyModel.getLoaiDK());
        bindingDialog.txtTinChi.setText("Số tín chỉ: " + lopDangKyModel.getTinCHi());

        bindingDialog.btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        bindingDialog.txtTenHP.setSelected(true);
        dialog.show();
    }

    private void showListTKBCacLopDangKy(JSONArray tkbCacLopDangKyJsonA) {
        tkbTamThoiModels = new ArrayList<>();
        for (int i = 0; i < tkbCacLopDangKyJsonA.length(); i++) {
            try {
                JSONObject tkbJsonO = tkbCacLopDangKyJsonA.getJSONObject(i);
                TKBTamThoiModel tkbTamThoiModel = new TKBTamThoiModel(
                        tkbJsonO.getString(JsonUtils.KEY_THU),
                        tkbJsonO.getString(JsonUtils.KEY_THOI_GIAN),
                        tkbJsonO.getString(JsonUtils.KEY_TUAN_HOC),
                        tkbJsonO.getString(JsonUtils.KEY_PHONG_HOC),
                        tkbJsonO.getString(JsonUtils.KEY_LOP_HOC));
                tkbTamThoiModels.add(tkbTamThoiModel);
            } catch (JSONException e) {
                Log.d("RAKAN", "showListTKBCacLopDangKy: " + e.toString());
            }
        }
        tkbTamThoiAdapter = new TKBTamThoiAdapter(this, R.layout.item_tkb_tam_thoi, tkbTamThoiModels);
        binding.lvLichHoc.setAdapter(tkbTamThoiAdapter);

    }

    private void showListCacLopDangKy(JSONArray cacLopDangKyJsonA) {
        cacLopDangKyModels = new ArrayList<>();
        for (int i = 0; i < cacLopDangKyJsonA.length(); i++) {
            try {
                JSONObject lopDangKyJsonO = cacLopDangKyJsonA.getJSONObject(i);
                CacLopDangKyModel lopDangKyModel = new CacLopDangKyModel(lopDangKyJsonO.getString(JsonUtils.KEY_MA_LOP),
                        lopDangKyJsonO.getString(JsonUtils.KEY_MA_LOP_KEM),
                        lopDangKyJsonO.getString(JsonUtils.KEY_TEN_LOP),
                        lopDangKyJsonO.getString(JsonUtils.KEY_MA_HP),
                        lopDangKyJsonO.getString(JsonUtils.KEY_LOAI_LOP),
                        lopDangKyJsonO.getString(JsonUtils.KEY_THONG_TIN_LOP),
                        lopDangKyJsonO.getString(JsonUtils.KEY_YEU_CAU),
                        lopDangKyJsonO.getString(JsonUtils.KEY_TRANG_THAI_DANG_KY),
                        lopDangKyJsonO.getString(JsonUtils.KEY_LOAI_DANG_KY),
                        lopDangKyJsonO.getString(JsonUtils.KEY_TIN_CHI));
                cacLopDangKyModels.add(lopDangKyModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        cacLopDangKyAdapter = new CacLopDangKyAdapter(this, R.layout.item_cac_lop_dang_ky, cacLopDangKyModels);
        binding.lvHocPhanDangKy.setAdapter(cacLopDangKyAdapter);
    }
}