package com.leenguyen.huststudent.model;

public class HustRankingModel {
    private String mssv, name, cpa, startDayTurnOn;
    private boolean isPrivate;

    public HustRankingModel(String mssv, String name, String cpa, boolean isPrivate, String startDayTurnOn) {
        this.mssv = mssv;
        this.name = name;
        this.cpa = cpa;
        this.isPrivate = isPrivate;
        this.startDayTurnOn = startDayTurnOn;
    }

    public HustRankingModel() {
    }

    public String getStartDayTurnOn() {
        return startDayTurnOn;
    }

    public void setStartDayTurnOn(String startDayTurnOff) {
        this.startDayTurnOn = startDayTurnOff;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getMssv() {
        return mssv;
    }

    public void setMssv(String mssv) {
        this.mssv = mssv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpa() {
        return cpa;
    }

    public void setCpa(String cpa) {
        this.cpa = cpa;
    }
}
