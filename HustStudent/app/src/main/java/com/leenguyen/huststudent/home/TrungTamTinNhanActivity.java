package com.leenguyen.huststudent.home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.os.Build;
import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.databinding.ActivityTrungTamTinNhanBinding;
import com.leenguyen.huststudent.model.QuangCaoModel;

import java.util.ArrayList;
import java.util.List;

public class TrungTamTinNhanActivity extends AppCompatActivity {
    private ActivityTrungTamTinNhanBinding binding;
    private DatabaseReference quangCaoDatabase;
    private ArrayList<QuangCaoModel> quangCaoModels;
    private TrungTamTinNhanAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trung_tam_tin_nhan);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        quangCaoDatabase = FirebaseDatabase.getInstance("https://hust-student-ad.firebaseio.com/").getReference();
        loadData();
        initLayout();
    }

    private void loadData() {
        quangCaoDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                quangCaoModels = new ArrayList<>();
                List<String> keys = new ArrayList<>();
                for (DataSnapshot keyNote : snapshot.getChildren()) {
                    keys.add(keyNote.getKey());
                    QuangCaoModel quangCao = keyNote.getValue(QuangCaoModel.class);
                    quangCaoModels.add(quangCao);
                    showListTinNhan();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void showListTinNhan() {
        if (quangCaoModels.size() > 0) {
            adapter = new TrungTamTinNhanAdapter(TrungTamTinNhanActivity.this, R.layout.item_tin_nhan, quangCaoModels);
            binding.lvTinNhan.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    private void initLayout() {
        binding.btnBack.setOnClickListener(v -> onBackPressed());
    }
}