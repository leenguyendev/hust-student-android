package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.HocPhanModel;

import java.util.ArrayList;
import java.util.Random;

public class BangDiemAdapter extends ArrayAdapter<HocPhanModel> {
    private Context context;
    private ArrayList<HocPhanModel> hocPhanModels;
    private int layoutResource;

    public BangDiemAdapter(@NonNull Context context, int resource, @NonNull ArrayList<HocPhanModel> hocPhanModels) {
        super(context, resource, hocPhanModels);
        this.context = context;
        this.hocPhanModels = hocPhanModels;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtTenHP = row.findViewById(R.id.txtTenHP);
        TextView txtsoTC = row.findViewById(R.id.txtSoTC);
        TextView txtDiem = row.findViewById(R.id.txtDiem);
        TextView txtLoaiHP = row.findViewById(R.id.txtLoaiHP);
        RelativeLayout loDivider = row.findViewById(R.id.loDivider);

        Random random = new Random();
        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        loDivider.setBackgroundColor(color);

        txtTenHP.setText(hocPhanModels.get(position).getTenHP());
        txtsoTC.setText(context.getString(R.string.so_tin_chi) + " " + hocPhanModels.get(position).getSoTinChi());
        if (hocPhanModels.get(position).isHPCaiThien()) {
            txtDiem.setText(hocPhanModels.get(position).getDiemCaiThien());
            txtLoaiHP.setVisibility(View.VISIBLE);
            txtLoaiHP.setText(context.getString(R.string.hoc_phan_cai_thien));
            txtTenHP.setTextColor(getContext().getResources().getColor(R.color.colorOrange));
            txtLoaiHP.setTextColor(getContext().getResources().getColor(R.color.colorOrange));
            txtsoTC.setTextColor(getContext().getResources().getColor(R.color.colorOrange));
            txtDiem.setTextColor(getContext().getResources().getColor(R.color.colorOrange));
        } else {
            txtDiem.setText(hocPhanModels.get(position).getDiemTongKet());
            if (hocPhanModels.get(position).isHPNew()) {
                txtLoaiHP.setVisibility(View.VISIBLE);
                txtLoaiHP.setText(context.getString(R.string.hoc_phan_moi));
                txtTenHP.setTextColor(getContext().getResources().getColor(R.color.colorGreen));
                txtLoaiHP.setTextColor(getContext().getResources().getColor(R.color.colorGreen));
                txtsoTC.setTextColor(getContext().getResources().getColor(R.color.colorGreen));
                txtDiem.setTextColor(getContext().getResources().getColor(R.color.colorGreen));
            } else {
                txtLoaiHP.setVisibility(View.GONE);
            }
            if (hocPhanModels.get(position).getDiemTongKet().equals("F")) {
                txtTenHP.setTextColor(getContext().getResources().getColor(R.color.colorRed));
                txtsoTC.setTextColor(getContext().getResources().getColor(R.color.colorRed));
                txtDiem.setTextColor(getContext().getResources().getColor(R.color.colorRed));
            }
        }
        return row;
    }
}
