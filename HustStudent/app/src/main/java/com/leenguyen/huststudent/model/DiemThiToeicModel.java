package com.leenguyen.huststudent.model;

public class DiemThiToeicModel  {
    private String maSV, hoTen, ngaySinh, hocKy, ghiChu, ngayThi, diemNghe, diemDoc, diemTong;

    public DiemThiToeicModel(String maSV, String hoTen, String ngaySinh, String hocKy, String ghiChu, String ngayThi, String diemNghe, String diemDoc, String diemTong) {
        this.maSV = maSV;
        this.hoTen = hoTen;
        this.ngaySinh = ngaySinh;
        this.hocKy = hocKy;
        this.ghiChu = ghiChu;
        this.ngayThi = ngayThi;
        this.diemNghe = diemNghe;
        this.diemDoc = diemDoc;
        this.diemTong = diemTong;
    }

    public String getMaSV() {
        return maSV;
    }

    public void setMaSV(String maSV) {
        this.maSV = maSV;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getHocKy() {
        return hocKy;
    }

    public void setHocKy(String hocKy) {
        this.hocKy = hocKy;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getNgayThi() {
        return ngayThi;
    }

    public void setNgayThi(String ngayThi) {
        this.ngayThi = ngayThi;
    }

    public String getDiemNghe() {
        return diemNghe;
    }

    public void setDiemNghe(String diemNghe) {
        this.diemNghe = diemNghe;
    }

    public String getDiemDoc() {
        return diemDoc;
    }

    public void setDiemDoc(String diemDoc) {
        this.diemDoc = diemDoc;
    }

    public String getDiemTong() {
        return diemTong;
    }

    public void setDiemTong(String diemTong) {
        this.diemTong = diemTong;
    }
}
