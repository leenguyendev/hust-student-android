package com.leenguyen.huststudent.model;

public class QuangCaoModel {
    private String name, content, endDate, link;

    public QuangCaoModel() {
    }

    public QuangCaoModel(String name, String content, String endDate, String link) {
        this.name = name;
        this.content = content;
        this.endDate = endDate;
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
