package com.leenguyen.huststudent.model;

public class UserInfoLoginModel {
    private String maSV, password;

    public UserInfoLoginModel(String maSV, String password) {
        this.maSV = maSV;
        this.password = password;
    }

    public String getMaSV() {
        return maSV;
    }

    public void setMaSV(String maSV) {
        this.maSV = maSV;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
