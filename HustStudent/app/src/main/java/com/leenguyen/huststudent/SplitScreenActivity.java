package com.leenguyen.huststudent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.os.Build;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.leenguyen.huststudent.databinding.ActivitySplitScreenBinding;
import com.leenguyen.huststudent.utils.Utils;

import java.util.Timer;
import java.util.TimerTask;

public class SplitScreenActivity extends AppCompatActivity {
    private boolean isLogined = false;
    private ActivitySplitScreenBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_split_screen);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        Glide.with(this).load(R.drawable.ic_ami_hello).into(binding.imgLogo);
        String alreadyLogin = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.SHARE_PREFERENCES_DATA_ALREADY_USER_LOGIN);
        isLogined = alreadyLogin.equals("1");
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (isLogined) {
                    Utils.getInstance().sendIntentActivity(getApplicationContext(), MainActivity.class);
                } else {
                    Utils.getInstance().sendIntentActivity(getApplicationContext(), LoginActivity.class);
                }
                finish();
            }
        }, 2000);
    }
}
