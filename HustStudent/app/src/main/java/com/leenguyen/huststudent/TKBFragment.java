package com.leenguyen.huststudent;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NavUtils;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;

import com.bumptech.glide.Glide;
import com.leenguyen.huststudent.adapter.ThoiKhoaBieuAdapter;
import com.leenguyen.huststudent.databinding.DialogSettingsTuanHocBinding;
import com.leenguyen.huststudent.databinding.DialogThoiKhoaBieuBinding;
import com.leenguyen.huststudent.databinding.FragmentTkbBinding;
import com.leenguyen.huststudent.menu.SettingsActivity;
import com.leenguyen.huststudent.model.ThoiKhoaBieuModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TKBFragment extends Fragment {
    private FragmentTkbBinding binding;
    private String dataTkb;
    private ArrayList<ThoiKhoaBieuModel> thoiKhoaBieuModels;
    private String tuanHocHienTai;
    private int thu;
    private boolean hasThiNghiemHomNay = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tkb, container, false);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        initLayout();
    }

    @SuppressLint("SetTextI18n")
    private void initLayout() {
        if (getContext() == null) {
            return;
        }

        tuanHocHienTai = Utils.getInstance().getTuanHocHienTai(getContext());
        if (tuanHocHienTai.equals("0")) {
            binding.txtDesc.setText("Bạn cần chạm vào vòng tròn tuần để cài đặt tuần hiện tại");
        }
        binding.txtTuanHocHienTai.setText(tuanHocHienTai);

        Calendar calendar = Calendar.getInstance();
        thu = calendar.get(Calendar.DAY_OF_WEEK);
        Date date = new Date();
        String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
        if (thu == Calendar.SUNDAY) {
            binding.txtThu.setText("CN" + " ngày " + currentDate);
        } else {
            binding.txtThu.setText("Thứ " + thu + " ngày " + currentDate);
        }

        dataTkb = Utils.getInstance().getValueFromSharedPreferences(getContext(),
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_DATA_TKB);
        if (dataTkb == null || dataTkb.equals("") || dataTkb.equals("[]")) {
            binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
            binding.imgReminder.setVisibility(View.GONE);
            binding.txtDesc.setText("Không có thời khóa biểu");
        } else {
            binding.loKhongCoDuLieu.setVisibility(View.GONE);
            if (thoiKhoaBieuModels != null) {
                thoiKhoaBieuModels = null;
            }
            showThoiKhoaBieu();
        }

        if (!tuanHocHienTai.equals("0"))
            checkThiNghiemHomNay();

        binding.lvTkb.setOnItemClickListener((parent, view, position, id) -> showDialogTKBDetail(thoiKhoaBieuModels.get(position)));

        binding.loTuanHienTai.setOnClickListener(v -> showDialogSettingsTuanHoc());
    }

    private void checkThiNghiemHomNay() {
        if (thoiKhoaBieuModels == null || thoiKhoaBieuModels.size() == 0) {
            return;
        }
        for (ThoiKhoaBieuModel thoiKhoaBieuModel : thoiKhoaBieuModels) {
            String tuanHoc = thoiKhoaBieuModel.getTuanHoc();
            ArrayList<String> listTuanHoc = new ArrayList<>();
            //Case 1,2,3,12 or 1-2,3-6 or 1,2,4-9
            StringBuilder builder = new StringBuilder();
            String startTime = null;
            String endTime = null;
            boolean isRangeWeek = false;
            for (int i = 0; i < tuanHoc.length(); i++) {
                if ((String.valueOf(tuanHoc.charAt(i)).equals(",") || String.valueOf(tuanHoc.charAt(i)).equals(".")) && !isRangeWeek) {
                    listTuanHoc.add(String.valueOf(builder).trim());
                    builder = new StringBuilder();
                } else if (String.valueOf(tuanHoc.charAt(i)).equals("-")) {
                    startTime = builder.toString().trim();
                    isRangeWeek = true;
                    builder = new StringBuilder();
                } else if ((String.valueOf(tuanHoc.charAt(i)).equals(",") || String.valueOf(tuanHoc.charAt(i)).equals(".")) && isRangeWeek) {
                    endTime = builder.toString().trim();
                    for (int j = Integer.parseInt(startTime); j <= Integer.parseInt(endTime); j++) {
                        listTuanHoc.add(String.valueOf(j));
                    }
                    startTime = null;
                    endTime = null;
                    isRangeWeek = false;
                    builder = new StringBuilder();
                } else if (i == tuanHoc.length() - 1) {
                    builder.append(tuanHoc.charAt(i));
                    if (!isRangeWeek) {
                        listTuanHoc.add(String.valueOf(builder));
                    } else {
                        endTime = builder.toString().trim();
                        for (int j = Integer.parseInt(startTime); j <= Integer.parseInt(endTime); j++) {
                            listTuanHoc.add(String.valueOf(j));
                        }
                    }
                } else {
                    builder.append(tuanHoc.charAt(i));
                }
            }

            if (listTuanHoc.size() > 0) {
                for (int i = 0; i < listTuanHoc.size(); i++) {
                    if (tuanHocHienTai.equals(listTuanHoc.get(i))
                            && thoiKhoaBieuModel.getLoaiLop().equals("TN")
                            && thoiKhoaBieuModel.getThoiGian().length() > 4
                            && String.valueOf(thoiKhoaBieuModel.getThoiGian().charAt(4)).equals(String.valueOf(thu))) {
                        hasThiNghiemHomNay = true;
                        break;
                    }
                }
            }
            if (hasThiNghiemHomNay) {
                break;
            }
        }
        if (hasThiNghiemHomNay) {
            binding.txtDesc.setText(getString(R.string.hom_nay_co_tn));
            binding.imgReminder.setVisibility(View.VISIBLE);
            Glide.with(requireContext()).load(R.drawable.ic_reminder_3).into(binding.imgReminder);
        } else {
            binding.txtDesc.setText(getString(R.string.hom_nay_khong_co_tn));
            binding.imgReminder.setVisibility(View.GONE);
        }
    }

    private void showDialogSettingsTuanHoc() {
        final Dialog dialog = new Dialog(requireContext());
        final DialogSettingsTuanHocBinding bindingDialog = DataBindingUtil.inflate(getLayoutInflater(), R.layout.dialog_settings_tuan_hoc, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(bindingDialog.getRoot());

        bindingDialog.pkNumber.setValue(Integer.parseInt(Utils.getInstance().getTuanHocHienTai(requireContext())));

        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());

        bindingDialog.btnLuu.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
            Utils.getInstance().saveToSharedPreferences(requireContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_WEEK_OF_YEAR, String.valueOf(weekOfYear));
            Utils.getInstance().saveToSharedPreferences(requireContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_TUAN_HOC_HIEN_TAI, String.valueOf(bindingDialog.pkNumber.getValue()));
            binding.txtTuanHocHienTai.setText(String.valueOf(bindingDialog.pkNumber.getValue()));
            Utils.getInstance().sendBroadcastUpdateWidget(requireContext());
            initLayout();
            dialog.dismiss();
        });
        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    private void showDialogTKBDetail(ThoiKhoaBieuModel thoiKhoaBieuModel) {
        if (getContext() == null) {
            return;
        }
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final DialogThoiKhoaBieuBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_thoi_khoa_bieu, null, false);
        dialog.setContentView(bindingDialog.getRoot());

        bindingDialog.txtTenLop.setText(thoiKhoaBieuModel.getTenLop());
        bindingDialog.txtThoiGian.setText("Thời gian: " + thoiKhoaBieuModel.getThoiGian());
        bindingDialog.txtTuanHoc.setText("Tuần học: " + thoiKhoaBieuModel.getTuanHoc());
        bindingDialog.txtPhongHoc.setText("Phòng học: " + thoiKhoaBieuModel.getPhongHoc());
        bindingDialog.txtMaLop.setText("Mã lớp: " + thoiKhoaBieuModel.getMaLop());
        bindingDialog.txtLoaiLop.setText("Loại lớp: " + thoiKhoaBieuModel.getLoaiLop());
        bindingDialog.txtNhom.setText("Nhóm: " + thoiKhoaBieuModel.getNhom());
        bindingDialog.txtMaHP.setText("Mã HP: " + thoiKhoaBieuModel.getMaHP());
        bindingDialog.txtGhiChu.setText("Ghi chú: " + thoiKhoaBieuModel.getGhiChu());
        bindingDialog.txtHinhThucDay.setText("Hình thức dạy: " + thoiKhoaBieuModel.getHinhThucDay());
        bindingDialog.txtGiangVien.setText("Giảng viên: " + thoiKhoaBieuModel.getGiangVien());
        bindingDialog.txtLinkOnline.setText("Link online: " + thoiKhoaBieuModel.getLinkOnline());
        bindingDialog.txtMaCode.setText("Mã code: " + thoiKhoaBieuModel.getMaCode());

        bindingDialog.txtTenLop.setSelected(true);

        bindingDialog.btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showThoiKhoaBieu() {
        try {
            JSONArray tkb = new JSONArray(dataTkb);
            if (tkb.length() > 0) {
                thoiKhoaBieuModels = new ArrayList<>();
                for (int i = 0; i < tkb.length(); i++) {
                    JSONObject object = tkb.getJSONObject(i);
                    String thoiGian = object.getString(JsonUtils.THOI_GIAN);
                    String tuanHoc = object.getString(JsonUtils.TUAN_HOC);
                    String phongHoc = object.getString(JsonUtils.PHONG_HOC);
                    String maLop = object.getString(JsonUtils.MA_LOP);
                    String loaiLop = object.getString(JsonUtils.LOAI_LOP);
                    String nhom = object.getString(JsonUtils.NHOM);
                    String maHP = object.getString(JsonUtils.MA_HP);
                    String tenLop = object.getString(JsonUtils.TEN_LOP);
                    String ghiChu = object.getString(JsonUtils.GHI_CHU);
                    String hinhThucDay = object.getString(JsonUtils.HINH_THUC_DAY);
                    String giangVien = object.getString(JsonUtils.GIANG_VIEN);
                    String linkOnline = object.getString(JsonUtils.LINK_ONLINE);
                    String maCode = object.getString(JsonUtils.MA_CODE);
                    ThoiKhoaBieuModel thoiKhoaBieuModel = new ThoiKhoaBieuModel(thoiGian, tuanHoc, phongHoc, maLop, loaiLop, nhom, maHP, tenLop, ghiChu, hinhThucDay, giangVien, linkOnline, maCode);
                    thoiKhoaBieuModels.add(thoiKhoaBieuModel);
                }
                if (getContext() != null) {
                    ThoiKhoaBieuAdapter adapter = new ThoiKhoaBieuAdapter(getContext(), R.layout.item_thoi_khoa_bieu, thoiKhoaBieuModels);
                    binding.lvTkb.setAdapter(adapter);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
