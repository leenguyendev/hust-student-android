package com.leenguyen.huststudent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.databinding.ActivityLoginBinding;
import com.leenguyen.huststudent.databinding.DialogConfirmBinding;
import com.leenguyen.huststudent.model.HustRankingModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.JsoupUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityLoginBinding binding;
    private Bitmap bitmap;
    private HashMap<String, String> cookiesLogin;
    private String maDangNhap;
    private boolean isUpdate;
    private DatabaseReference rankingDatabase, adsIdDatabase;
    private boolean isPrivate = false;
    private String startDayTurnOn;
    private String loginForm;
    private DatabaseReference vipMemberDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        rankingDatabase = FirebaseDatabase.getInstance("https://hust-student-ranking.firebaseio.com/").getReference();
        adsIdDatabase = FirebaseDatabase.getInstance("https://hust-student-admob-id.firebaseio.com/").getReference();
        vipMemberDatabase = FirebaseDatabase.getInstance("https://hust-student-vip-member.firebaseio.com/").getReference();
        Intent intent = getIntent();
        isUpdate = intent.getStringExtra("type") != null && intent.getStringExtra("type").equals("update");
        initLayout();
        binding.btnLogIn.setOnClickListener(this);
        binding.imgRefresh.setOnClickListener(this);
        binding.imgListUser.setOnClickListener(this);
        binding.imgEye.setOnClickListener(this);
        binding.txtGapVanDeDangNhap.setOnClickListener(this);
        binding.btnNotAccount.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    private void initLayout() {
        Utils.getInstance().showLoadingDialog(this);
        if (Utils.getInstance().isOnline(getApplicationContext())) {
            binding.txtNotice.setVisibility(View.GONE);
            new AccessToHost().execute();
        } else {
            Utils.getInstance().hideLoadingDialog();
            binding.txtNotice.setText(getString(R.string.notice_login_no_internet));
            binding.txtNotice.setVisibility(View.VISIBLE);
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_LogIn:
                if (binding.edtStudentCode.getText().toString().equals("") ||
                        binding.edtPassword.getText().toString().equals("") ||
                        binding.edtCaptcha.getText().toString().equals("")) {
                    binding.txtNotice.setText(getString(R.string.notice_login_fill_data));
                    binding.txtNotice.setVisibility(View.VISIBLE);
                } else if (!Utils.getInstance().isOnline(getApplicationContext())) {
                    binding.txtNotice.setText(getString(R.string.notice_login_no_internet));
                    binding.txtNotice.setVisibility(View.VISIBLE);
                } else if (binding.edtStudentCode.getText().toString().length() != 8) {
                    binding.txtNotice.setText(getString(R.string.ma_so_sv_khong_dung));
                    binding.txtNotice.setVisibility(View.VISIBLE);
                } else {
                    showAccessLoginDialog();
                    binding.txtNotice.setVisibility(View.GONE);
                }
                break;
            case R.id.imgRefresh:
                initLayout();
                break;
            case R.id.imgListUser:
                break;
            case R.id.txtGapVanDeDangNhap:
                Utils.getInstance().sendIntentActivity(getApplicationContext(), GapVanDeDangNhapActivity.class);
                break;
            case R.id.imgEye:
                if (binding.edtPassword.getInputType() == 0x00000081) {
                    binding.edtPassword.setInputType(0x00000001);
                    binding.imgEye.setImageResource(R.drawable.ic_show_pass);
                } else {
                    binding.edtPassword.setInputType(0x00000081);
                    binding.imgEye.setImageResource(R.drawable.ic_hide_pass);
                }
                break;
            case R.id.btn_Not_Account:
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.setAction(MainActivity.ACTION_OPEN_MENU);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    private void showAccessLoginDialog() {
        final Dialog dialog = new Dialog(this);
        final DialogConfirmBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_confirm, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(bindingDialog.getRoot());
        bindingDialog.txtTitle.setText(getString(R.string.confirm_login_title));
        bindingDialog.txtDesc.setText(getString(R.string.confirm_login_desc));
        bindingDialog.btnContinues.setEnabled(bindingDialog.cbReadAndAccess.isChecked());
        bindingDialog.cbReadAndAccess.setOnCheckedChangeListener((buttonView, isChecked) -> bindingDialog.btnContinues.setEnabled(bindingDialog.cbReadAndAccess.isChecked()));
        bindingDialog.btnCancel.setOnClickListener(v -> dialog.dismiss());
        bindingDialog.btnContinues.setOnClickListener(v -> {
            dialog.dismiss();
            if (!dialog.isShowing()) {
                binding.txtNotice.setVisibility(View.GONE);
                if (isUpdate) {
                    checkAccount();
                } else {
                    String isFirstTime = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_FIRST_TIME);
                    if (isFirstTime == null || isFirstTime.equals("")) {
                        Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_FIRST_TIME, Constant.VALUE_SHARE_PREFERENCES_IS_FIRST_TIME_TRUE);
                    }
                }
                //Ma hoa md5
                String keyMaHoa = maHoaMD5(binding.edtStudentCode.getText() + "." + binding.edtPassword.getText());
                //Login
                HashMap<String, String> accountAndCaptcha = new HashMap<>();
                accountAndCaptcha.put(Constant.KEY_MA_HOA_MD5, keyMaHoa);
                accountAndCaptcha.put(Constant.KEY_USERNAME, binding.edtStudentCode.getText().toString());
                accountAndCaptcha.put(Constant.KEY_PASSWORD, binding.edtPassword.getText().toString());
                accountAndCaptcha.put(Constant.KEY_CAPTCHA, binding.edtCaptcha.getText().toString());
                new Login().execute(accountAndCaptcha);
            }
        });
        dialog.show();
    }

    //Nếu update mà tài khoản khác thì cần xoá thêm data tài khoản cũ đang lưu
    private void checkAccount() {
        String dataUser = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.SHARE_PREFERENCES_DATA_USER);
        try {
            JSONObject accountJsonO = new JSONObject(dataUser);
            String mssv = accountJsonO.getString(JsonUtils.KEY_MA_SV);
            if (!mssv.equals(binding.edtStudentCode.getText().toString())) {
                Utils.getInstance().saveToSharedPreferences(this,
                        Constant.SHARE_PREFERENCES_DATA,
                        Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN, "");
                Utils.getInstance().saveToSharedPreferences(this,
                        Constant.SHARE_PREFERENCES_DATA,
                        Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI, "");
                Utils.getInstance().saveToSharedPreferences(this,
                        Constant.SHARE_PREFERENCES_DATA,
                        Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_KHONG_TINH_DIEM, "");
            }
        } catch (Exception e) {
            Log.e("RAKAN", "checkAccount: " + e.toString());
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class AccessToHost extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            Connection.Response connection;
            try {
                connection = Jsoup.connect(Constant.URL_LOGIN)
                        .method(Connection.Method.GET)
                        .timeout(15000)
                        .execute();
                if (connection.statusCode() == Constant.STATUS_CODE_OK) {
                    //Save cookies
                    cookiesLogin = new HashMap<>();
                    cookiesLogin.putAll(connection.cookies());
                    Document document = connection.parse();
                    //Get Captcha
                    Element captcha = document.getElementById(Constant.ID_CAPTCHA);
                    InputStream inputStream = new java.net.URL(captcha.absUrl("src")).openStream();
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    //Get form data login
                    String valueFormDataLogin3 = document.select("input[id=__VIEWSTATE]").first().attr("value");
                    String valueFormDataLogin5 = document.select("input[id=__EVENTVALIDATION]").first().attr("value");
                    Utils.getInstance().saveToSharedPreferences(LoginActivity.this, Constant.SHARE_PREFERENCES_FORM_DATA_LOGIN, Constant.KEY_SHARE_PREFERENCES_FORM_DATA_LOGIN_3, valueFormDataLogin3);
                    Utils.getInstance().saveToSharedPreferences(LoginActivity.this, Constant.SHARE_PREFERENCES_FORM_DATA_LOGIN, Constant.KEY_SHARE_PREFERENCES_FORM_DATA_LOGIN_5, valueFormDataLogin5);
                    return true;
                }
            } catch (IOException e) {
                Log.e("RAKAN", "AccessToHost: " + e.toString());
                e.printStackTrace();
            }
            return false;
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(Boolean isConnectOK) {
            super.onPostExecute(isConnectOK);
            Utils.getInstance().hideLoadingDialog();
            if (isConnectOK) {
                binding.imgCaptcha.setImageBitmap(bitmap);
            } else {
                binding.txtNotice.setText(getString(R.string.notice_login_no_internet));
            }
            if (bitmap == null) {
                binding.txtNotice.setText("Trang ctt-sis đang được cập nhật nên tạm thời app không đăng nhập được, quay lại sau nha thím :(");
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class Login extends AsyncTask<HashMap, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getInstance().showLoadingDialog(LoginActivity.this);
        }

        @Override
        protected Boolean doInBackground(HashMap... accountAndCaptcha) {
            String userName, password, captcha, keyMaHoa;
            boolean isLoginSuccess = false;
            try {
                JSONObject accountJsonO = new JSONObject(accountAndCaptcha[0]);
                userName = accountJsonO.getString(Constant.KEY_USERNAME);
                password = accountJsonO.getString(Constant.KEY_PASSWORD);
                captcha = accountJsonO.getString(Constant.KEY_CAPTCHA);
                keyMaHoa = accountJsonO.getString(Constant.KEY_MA_HOA_MD5);

                //Ma hoa thong tin dang nhap => ma dang nhap
                String passwordMaHoa = keyMaHoa;
                maDangNhap = "{&quot;data&quot;:&quot;12|#|user|4|9|1" + userName + "pass|4|25|1" + passwordMaHoa + "#&quot;}";
                //Login to ctt-daotao
                isLoginSuccess = JsoupUtils.getInstance().login(getApplicationContext(), userName, password, captcha, maDangNhap, cookiesLogin, 0);
                loginForm = "0";
                if (!isLoginSuccess) {
                    maDangNhap = "{&quot;data&quot;:&quot;12|#|user|4|9|1" + userName + "pass|4|45|1" + passwordMaHoa + "#&quot;}";
                    isLoginSuccess = JsoupUtils.getInstance().login(getApplicationContext(), userName, password, captcha, maDangNhap, cookiesLogin, 1);
                    loginForm = "1";
                    if (!isLoginSuccess) {
                        maDangNhap = "{&quot;data&quot;:&quot;12|#|user|4|9|1" + userName + "pass|4|33|1" + passwordMaHoa + "#&quot;}";
                        isLoginSuccess = JsoupUtils.getInstance().login(getApplicationContext(), userName, password, captcha, maDangNhap, cookiesLogin, 2);
                        loginForm = "2";
                        if (!isLoginSuccess) {
                            maDangNhap = "{&quot;data&quot;:&quot;12|#|user|4|9|1" + userName + "pass|4|57|1" + passwordMaHoa + "#&quot;}";
                            isLoginSuccess = JsoupUtils.getInstance().login(getApplicationContext(), userName, password, captcha, maDangNhap, cookiesLogin, 2);
                            loginForm = "2";
                            if (!isLoginSuccess) {
                                maDangNhap = "{&quot;rawValue&quot;:&quot;" + userName + "&quot;,&quot;validationState&quot;:&quot;&quot;}";
                                isLoginSuccess = JsoupUtils.getInstance().login(getApplicationContext(), userName, password, captcha, maDangNhap, cookiesLogin, 3);
                                loginForm = "3";
                            }
                        }
                    }
                }
                if (isLoginSuccess) {
                    checkVipMember();
                    checkNoAdsMember();
                    sendDataToTopStudent();
                    saveUser();
                    Utils.getInstance().sendBroadcastUpdateWidget(LoginActivity.this);
                    Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                            Constant.SHARE_PREFERENCES_DATA,
                            Constant.SHARE_PREFERENCES_DATA_ALREADY_USER_LOGIN, "1");
                    Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                            Constant.SHARE_PREFERENCES_DATA,
                            Constant.KEY_SHARE_PREFERENCES_LAST_TIME_UPDATE_DATA,
                            java.text.DateFormat.getDateTimeInstance().format(new Date()));
                }
            } catch (Exception e) {
                Log.e("RAKAN", "Login: " + e.toString());
                e.printStackTrace();
            }
            return isLoginSuccess;
        }

        @Override
        protected void onPostExecute(Boolean isLoginSuccess) {
            super.onPostExecute(isLoginSuccess);
            if (isLoginSuccess) {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    Utils.getInstance().hideLoadingDialog();
                    Utils.getInstance().sendIntentActivity(LoginActivity.this, MainActivity.class);
                }, 6969);
            } else {
                Utils.getInstance().hideLoadingDialog();
                initLayout();
                Utils.getInstance().showNoticeDialog(LoginActivity.this, getString(R.string.login_fail_title), "Thông tin đăng nhập không đúng hoặc trang web đang bảo trì.");
            }
        }
    }

    private void checkNoAdsMember() {
        String mssv = binding.edtStudentCode.getText().toString();
        adsIdDatabase.child("noAdsListMember").child(mssv).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getValue() == null) {
                    Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS, "0");
                } else {
                    Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS, "1");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void checkVipMember() {
        try {
            String mssv = binding.edtStudentCode.getText().toString();
            vipMemberDatabase.child("danhSach").child(mssv).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.getValue() == null) {
                        Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER, "0");
                    } else {
                        Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER, "1");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDataToTopStudent() {
        final String mssv = binding.edtStudentCode.getText().toString();
        final String name, cpa;

        String thongTinSV = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        String bangDiemTongKet = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_TONG_KET);
        try {
            JSONObject thongTinSVJsonO = new JSONObject(thongTinSV);
            name = thongTinSVJsonO.getString(JsonUtils.KEY_HO_TEN_SV).trim();
            JSONArray bangDiemTongKetJsonA = new JSONArray(bangDiemTongKet);
            if (bangDiemTongKetJsonA.length() > 0) {
                JSONObject diemTongKetLastJsonO = bangDiemTongKetJsonA.getJSONObject(0);
                if (diemTongKetLastJsonO != null) {
                    cpa = diemTongKetLastJsonO.getString(JsonUtils.KEY_CPA);
                } else {
                    cpa = "0";
                }
            } else {
                cpa = "0";
            }
            String khoaHoc = Utils.getInstance().getKhoaHoc(this);
            rankingDatabase.child("k" + khoaHoc).child(mssv).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    HustRankingModel hustRankingModel = snapshot.getValue(HustRankingModel.class);
                    if (hustRankingModel != null) {
                        isPrivate = hustRankingModel.isPrivate();
                    }
                    if (hustRankingModel != null) {
                        startDayTurnOn = hustRankingModel.getStartDayTurnOn();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
            rankingDatabase.child("k" + khoaHoc).child(mssv).setValue(new HustRankingModel(mssv, name, cpa, isPrivate, startDayTurnOn));
        } catch (Exception e) {
            Log.e("RAKAN", "sendDataToTopStudent: " + e.toString());
            e.printStackTrace();
        }
    }

    public static String maHoaMD5(String str) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(str.getBytes());
            byte[] messageDigest = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                StringBuilder h = new StringBuilder(Integer.toHexString(0xFF & aMessageDigest));
                while (h.length() < 2)
                    h.insert(0, "0");
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void saveUser() {
        String maSv = binding.edtStudentCode.getText().toString();
        String password = binding.edtPassword.getText().toString();
        try {
            JSONObject accountJsonO = new JSONObject();
            accountJsonO.put(JsonUtils.KEY_MA_SV, maSv);
            accountJsonO.put(JsonUtils.KEY_PASSWORD, password);
            accountJsonO.put(JsonUtils.KEY_MA_DANG_NHAP, maDangNhap);
            accountJsonO.put(JsonUtils.KEY_LOGIN_FORM, loginForm);
            Utils.getInstance().saveToSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.SHARE_PREFERENCES_DATA_USER, accountJsonO.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
