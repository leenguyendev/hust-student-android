package com.leenguyen.huststudent.api;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface RetrofitApi {
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://ctt-sis.hust.edu.vn")
            .addConverterFactory(ScalarsConverterFactory.create())
            .build();

    RetrofitApi service = retrofit.create(RetrofitApi.class);

    @GET("/")
    Call<String> getThongTinSVApi(@Header("Cookie") String cookies);

    @GET("/Students/CheckTuition.aspx")
    Call<String> getThongTinCongNoHocPhi(@Header("Cookie") String cookies);

    @GET("/Students/TimetablesTemp.aspx")
    Call<String> getThoiKhoaBieuTamThoi(@Header("Cookie") String cookies);

    @GET("/Students/StudentCheckInputGradeTerm.aspx")
    Call<String> getKiemTraNhapDiemKiMoiNhat(@Header("Cookie") String cookies);

    @GET("/Students/StudentCourseGrade.aspx")
    Call<String> getBangDiemHocPhan(@Header("Cookie") String cookies);

    @GET("/Students/StudentCourseMarks.aspx")
    Call<String> getBangDiemCaNhan(@Header("Cookie") String cookies);

    @GET("/Students/ToeicMarks.aspx")
    Call<String> getKetQuaThiToeic(@Header("Cookie") String cookies);

    @GET("/Students/Timetables.aspx")
    Call<String> getThoiKhoaBieu(@Header("Cookie") String cookies);

    @GET("/Students/StudentGroupInfo.aspx")
    Call<String> getThongTinLopSV(@Header("Cookie") String cookies);
}
