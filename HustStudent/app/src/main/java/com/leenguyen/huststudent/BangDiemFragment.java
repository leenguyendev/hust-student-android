package com.leenguyen.huststudent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leenguyen.huststudent.adapter.BangDiemAdapter;
import com.leenguyen.huststudent.databinding.DialogConfirmBinding;
import com.leenguyen.huststudent.databinding.DialogDetailBangDiemHocPhanBinding;
import com.leenguyen.huststudent.databinding.FragmentBangDiemBinding;
import com.leenguyen.huststudent.model.HocPhanModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class BangDiemFragment extends Fragment {
    private FragmentBangDiemBinding binding;
    private ArrayList<HocPhanModel> hocPhanModels;
    private BangDiemAdapter bangDiemAdapter;
    private String bangDiemHP;
    private String bangHPCaiThien;
    private String bangHPNew;
    private Dialog detailDialog, addHPThuCongDialog;
    private final String[] listDiemChu = {"F", "D", "D+", "C", "C+", "B", "B+", "A", "A+"};
    private double total = 0;
    private int soTCTotal = 0;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bang_diem, container, false);
        initLayout();
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        String isFirstTime = Utils.getInstance().getValueFromSharedPreferences(requireContext(),
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_FIRST_TIME_OPEN_BANG_DIEM);
        if (isFirstTime == null || isFirstTime.equals("") || !isFirstTime.equals("1")) {
            showDialogHuongDan();
            Utils.getInstance().saveToSharedPreferences(requireContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_IS_FIRST_TIME_OPEN_BANG_DIEM, "1");
        }
    }

    private void initLayout() {
        showCPA();
        showListBangDiemHocPhan();
        binding.btnAll.setChecked(true);

        binding.listBangDiem.setOnItemClickListener((parent, view, position, id) -> showDetailHPDialog(position));

        binding.imgAdd.setOnClickListener(view -> showDialogAddHPMoiThuCong());

        binding.btnAll.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                showHPFilter("All");
            }
        });
        binding.btnA.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                showHPFilter("A");
            }
        });
        binding.btnBPlus.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                showHPFilter("B+");
            }
        });
        binding.btnB.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                showHPFilter("B");
            }
        });
        binding.btnCPlus.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                showHPFilter("C+");
            }
        });
        binding.btnC.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                showHPFilter("C");
            }
        });
        binding.btnDPlus.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                showHPFilter("D+");
            }
        });
        binding.btnD.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                showHPFilter("D");
            }
        });
        binding.btnF.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                showHPFilter("F");
            }
        });
    }

    private void showDialogAddHPMoiThuCong() {
        if (getContext() == null) {
            return;
        }
        addHPThuCongDialog = new Dialog(getContext());
        addHPThuCongDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addHPThuCongDialog.setContentView(R.layout.dialog_them_hoc_phan_thu_cong);
        ImageView btnClose = addHPThuCongDialog.findViewById(R.id.btnClose);
        final EditText edtMaHP = addHPThuCongDialog.findViewById(R.id.edtMaHP);
        final EditText edtTenHP = addHPThuCongDialog.findViewById(R.id.edtTenHP);
        final EditText edtSoTC = addHPThuCongDialog.findViewById(R.id.edtSoTC);
        final TextView txtDiemCaiThien = addHPThuCongDialog.findViewById(R.id.txtDiemCaiThien);
        final SeekBar seekBar = addHPThuCongDialog.findViewById(R.id.seekbar);
        Button btnLuu = addHPThuCongDialog.findViewById(R.id.btnLuu);
        final TextView txtNotice = addHPThuCongDialog.findViewById(R.id.txtNotice);

        txtDiemCaiThien.setText(listDiemChu[seekBar.getProgress()]);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtDiemCaiThien.setText(listDiemChu[seekBar.getProgress()]);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        btnClose.setOnClickListener(view -> addHPThuCongDialog.dismiss());

        btnLuu.setOnClickListener(view -> {
            if (edtMaHP.getText().toString().equals("") || edtTenHP.getText().toString().equals("") || edtSoTC.getText().toString().equals("")) {
                txtNotice.setVisibility(View.VISIBLE);
            } else {
                txtNotice.setVisibility(View.GONE);
                saveHPMoi(edtMaHP.getText().toString(), edtTenHP.getText().toString(), edtSoTC.getText().toString(), listDiemChu[seekBar.getProgress()]);
                addHPThuCongDialog.dismiss();
                showCPA();
                showListBangDiemHocPhan();
            }
        });
        addHPThuCongDialog.setCancelable(false);
        addHPThuCongDialog.show();
    }

    private void saveHPMoi(String maHP, String tenHP, String tinChiHP, String diemTongKet) {
        if (getContext() == null) {
            return;
        }
        bangHPNew = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI);
        try {
            JSONArray bangHPNewLast = new JSONArray();
            if (bangHPNew != null && !bangHPNew.equals("")) {
                JSONArray bangHPNewJsonA = new JSONArray(bangHPNew);
                if (bangHPNewJsonA.length() > 0) {
                    for (int i = 0; i < bangHPNewJsonA.length(); i++) {
                        JSONObject object = bangHPNewJsonA.getJSONObject(i);
                        if (!object.getString(JsonUtils.KEY_MA_HP).equals(maHP)) {
                            bangHPNewLast.put(object);
                        }
                    }
                }
            }

            JSONObject object = new JSONObject();
            object.put(JsonUtils.KEY_MA_HP, maHP);
            object.put(JsonUtils.KEY_TEN_HP, tenHP);
            object.put(JsonUtils.KEY_TIN_CHI_HP, tinChiHP);
            object.put(JsonUtils.KEY_DIEM_TONG_KET, diemTongKet);
            bangHPNewLast.put(object);
            Utils.getInstance().saveToSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI, bangHPNewLast.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCPA() {
        if (getContext() == null) {
            return;
        }
        total = 0;
        soTCTotal = 0;
        try {
            bangHPCaiThien = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN);
            if (bangHPCaiThien != null && !bangHPCaiThien.equals("")) {
                JSONArray bangHPCaiThienJsonA = new JSONArray(bangHPCaiThien);
                if (bangHPCaiThienJsonA.length() > 0) {
                    for (int i = 0; i < bangHPCaiThienJsonA.length(); i++) {
                        JSONObject hPCaiThienJsonO = bangHPCaiThienJsonA.getJSONObject(i);
                        int soTC = Integer.parseInt(hPCaiThienJsonO.getString(JsonUtils.KEY_TIN_CHI_HP));
                        String diemHienTai = hPCaiThienJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        String diemCaiThien = hPCaiThienJsonO.getString(JsonUtils.KEY_DIEM_CAI_THIEN);
                        total += soTC * (Utils.getInstance().convertDiemChuToDiemSo(diemCaiThien) - Utils.getInstance().convertDiemChuToDiemSo(diemHienTai));
                    }
                }
            }

            bangHPNew = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI);
            if (bangHPNew != null && !bangHPNew.equals("")) {
                JSONArray bangHPNewJsonA = new JSONArray(bangHPNew);
                if (bangHPNewJsonA.length() > 0) {
                    for (int i = 0; i < bangHPNewJsonA.length(); i++) {
                        JSONObject hPNewJsonO = bangHPNewJsonA.getJSONObject(i);
                        int soTC = Integer.parseInt(hPNewJsonO.getString(JsonUtils.KEY_TIN_CHI_HP));
                        String diem = hPNewJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        soTCTotal += soTC;
                        total += soTC * Utils.getInstance().convertDiemChuToDiemSo(diem);
                    }
                }
            }

            bangDiemHP = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN);
            if (bangDiemHP != null && !bangDiemHP.equals("")) {
                JSONArray bangDiemHPJsonA = new JSONArray(bangDiemHP);
                if (bangDiemHPJsonA.length() > 0) {
                    for (int i = 0; i < bangDiemHPJsonA.length(); i++) {
                        JSONObject diemHPJsonO = bangDiemHPJsonA.getJSONObject(i);
                        int soTC = Integer.parseInt(diemHPJsonO.getString(JsonUtils.KEY_TIN_CHI_HP));
                        String diem = diemHPJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        //fix issue bảng điểm có điểm lạ I, X, R, W => k tính điểm
                        for (int i1 = 0; i1 < listDiemChu.length; i1++) {
                            if (listDiemChu[i1].equals(diem)) {
                                soTCTotal += soTC;
                                total += soTC * Utils.getInstance().convertDiemChuToDiemSo(diem);
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String cpa = String.valueOf((double) Math.round(total / soTCTotal * 100) / 100);
        binding.txtCPA.setText(cpa);
        showXepLoai();
    }

    @SuppressLint("SetTextI18n")
    private void showListBangDiemHocPhan() {
        if (getContext() == null) {
            return;
        }
        int tongSoTC = 0;
        if (hocPhanModels != null) {
            hocPhanModels = null;
        }
        hocPhanModels = new ArrayList<>();
        try {
            bangHPNew = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI);
            if (bangHPNew != null && !bangHPNew.equals("")) {
                JSONArray bangHPNewJsonA = new JSONArray(bangHPNew);
                if (bangHPNewJsonA.length() > 0) {
                    for (int i = bangHPNewJsonA.length() - 1; i >= 0; i--) {
                        JSONObject hPNewJsonO = bangHPNewJsonA.getJSONObject(i);
                        String maHP = hPNewJsonO.getString(JsonUtils.KEY_MA_HP);
                        String tenHP = hPNewJsonO.getString(JsonUtils.KEY_TEN_HP);
                        String soTC = hPNewJsonO.getString(JsonUtils.KEY_TIN_CHI_HP);
                        String diemTongKet = hPNewJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        HocPhanModel hocPhanModel = new HocPhanModel(maHP, tenHP, null, null, null, null, soTC, diemTongKet, null, false, true);
                        hocPhanModels.add(hocPhanModel);
                        tongSoTC += Integer.parseInt(soTC);
                    }
                }
            }

            bangHPCaiThien = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN);
            if (bangHPCaiThien != null && !bangHPCaiThien.equals("")) {
                JSONArray bangHPCaiThienJsonA = new JSONArray(bangHPCaiThien);
                if (bangHPCaiThienJsonA.length() > 0) {
                    for (int i = bangHPCaiThienJsonA.length() - 1; i >= 0; i--) {
                        JSONObject hPCaiThienJsonO = bangHPCaiThienJsonA.getJSONObject(i);
                        String maHP = hPCaiThienJsonO.getString(JsonUtils.KEY_MA_HP);
                        String tenHP = hPCaiThienJsonO.getString(JsonUtils.KEY_TEN_HP);
                        String soTC = hPCaiThienJsonO.getString(JsonUtils.KEY_TIN_CHI_HP);
                        String diemTongKet = hPCaiThienJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        String diemCaiThien = hPCaiThienJsonO.getString(JsonUtils.KEY_DIEM_CAI_THIEN);
                        HocPhanModel hocPhanModel = new HocPhanModel(maHP, tenHP, null, null, null, null, soTC, diemTongKet, diemCaiThien, true, false);
                        hocPhanModels.add(hocPhanModel);
                    }
                }
            }

            bangDiemHP = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN);
            if (bangDiemHP != null && !bangDiemHP.equals("")) {
                JSONArray bangDiemHPJsonA = new JSONArray(bangDiemHP);
                if (bangDiemHPJsonA.length() > 0) {
                    for (int i = 0; i < bangDiemHPJsonA.length(); i++) {
                        JSONObject diemHPJsonO = bangDiemHPJsonA.getJSONObject(i);
                        String maHP = diemHPJsonO.getString(JsonUtils.KEY_MA_HP);
                        String tenHP = diemHPJsonO.getString(JsonUtils.KEY_TEN_HP);
                        String soTC = diemHPJsonO.getString(JsonUtils.KEY_TIN_CHI_HP);
                        String diem = diemHPJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        String hocKy = diemHPJsonO.getString(JsonUtils.KEY_HOC_KY);
                        HocPhanModel hocPhanModel = new HocPhanModel(maHP, tenHP, hocKy, null, null, null, soTC, diem, null, false, false);
                        hocPhanModels.add(hocPhanModel);
                        tongSoTC += Integer.parseInt(soTC);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (hocPhanModels.size() > 0 && getContext() != null) {
            binding.loKhongCoDuLieu.setVisibility(View.GONE);
            bangDiemAdapter = new BangDiemAdapter(getContext(), R.layout.item_bang_diem_hoc_phan, hocPhanModels);
            binding.listBangDiem.setAdapter(bangDiemAdapter);
            binding.txtTongSoTC.setText(tongSoTC + " tín chỉ");
        } else {
            binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
        }
    }

    @SuppressLint("SetTextI18n")
    private void showDetailHPDialog(int position) {
        if (hocPhanModels.size() > 0 && getContext() != null) {
            final HocPhanModel hocPhanModel = hocPhanModels.get(position);
            detailDialog = new Dialog(getContext());
            detailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            final DialogDetailBangDiemHocPhanBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_detail_bang_diem_hoc_phan, null, false);
            detailDialog.setContentView(bindingDialog.getRoot());

            String hocKy = "", lopHoc = "", diemQuaTrinh = "", diemThi = "";
            bindingDialog.txtTenHP.setText(hocPhanModel.getTenHP());
            bindingDialog.txtSoTC.setText(getString(R.string.so_tin_chi) + " " + hocPhanModel.getSoTinChi());
            bindingDialog.txtDiemTongKet.setText(getString(R.string.diem_tong_ket) + " " + hocPhanModel.getDiemTongKet());
            bindingDialog.txtMaHP.setText(getString(R.string.ma_hoc_phan) + " " + hocPhanModel.getMaHocPhan());
            if (getContext() != null && !hocPhanModel.isHPNew() && !hocPhanModel.isHPCaiThien()) {
                String bangDiemCaNhan = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_CA_NHAN);
                try {
                    JSONArray bangDiemCaNhanJsonA = new JSONArray(bangDiemCaNhan);
                    if (bangDiemCaNhanJsonA.length() > 0) {
                        JSONObject hocPhanJsonO;
                        for (int i = 0; i < bangDiemCaNhanJsonA.length(); i++) {
                            hocPhanJsonO = bangDiemCaNhanJsonA.getJSONObject(i);
                            String maHP = hocPhanJsonO.getString(JsonUtils.KEY_MA_HP);
                            String diemTongKet = hocPhanJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                            if (maHP.equals(hocPhanModel.getMaHocPhan()) && diemTongKet.equals(hocPhanModel.getDiemTongKet())) {
                                lopHoc = hocPhanJsonO.getString(JsonUtils.KEY_LOP_HOC);
                                diemQuaTrinh = hocPhanJsonO.getString(JsonUtils.KEY_DIEM_QUA_TRINH);
                                diemThi = hocPhanJsonO.getString(JsonUtils.KEY_DIEM_THI);
                                hocKy = hocPhanJsonO.getString(JsonUtils.KEY_HOC_KY);
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            bindingDialog.txtHocKy.setText(getString(R.string.hoc_ky) + " " + hocKy);
            bindingDialog.txtLopHoc.setText(getString(R.string.lop_hoc) + " " + lopHoc);
            bindingDialog.txtDiemQT.setText(getString(R.string.diem_qua_trinh) + " " + diemQuaTrinh);
            bindingDialog.txtDiemThi.setText(getString(R.string.diem_thi) + " " + diemThi);

            if (hocPhanModel.isHPCaiThien()) {
                bindingDialog.txtDiemCaiThien.setText(hocPhanModel.getDiemCaiThien());
            } else {
                bindingDialog.txtDiemCaiThien.setText(hocPhanModel.getDiemTongKet());
                bindingDialog.btnYes.setEnabled(false);
            }

            for (int i = 0; i < listDiemChu.length; i++) {
                if (hocPhanModel.isHPCaiThien()) {
                    if (listDiemChu[i].equals(hocPhanModel.getDiemCaiThien())) {
                        bindingDialog.seekbar.setProgress(i);
                        break;
                    }
                } else {
                    if (listDiemChu[i].equals(hocPhanModel.getDiemTongKet())) {
                        bindingDialog.seekbar.setProgress(i);
                        break;
                    }
                }
            }

            bindingDialog.seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    bindingDialog.txtDiemCaiThien.setText(listDiemChu[i]);
                    if (hocPhanModel.isHPNew()) {
                        showCPACaiThien(hocPhanModel, Utils.getInstance().convertDiemChuToDiemSo(listDiemChu[i]));
                        if (hocPhanModel.getDiemTongKet().equals(listDiemChu[i])) {
                            bindingDialog.btnYes.setEnabled(false);
                        } else {
                            bindingDialog.btnYes.setEnabled(true);
                        }
                    } else {
                        int diemTKPosition = 0;
                        for (int i1 = 0; i1 < listDiemChu.length; i1++) {
                            if (hocPhanModel.getDiemTongKet().equals(listDiemChu[i1])) {
                                diemTKPosition = i1;
                                break;
                            }
                        }
                        if (i <= diemTKPosition) {
                            bindingDialog.btnYes.setEnabled(false);
                            if (i < diemTKPosition) {
                                bindingDialog.loCaiThienLui.setVisibility(View.VISIBLE);
                                bindingDialog.loDiemCaiThien.setVisibility(View.GONE);
                                Glide.with(getContext()).load(R.drawable.ic_ami_cry).into(bindingDialog.imgSticker);
                            } else {
                                bindingDialog.loCaiThienLui.setVisibility(View.GONE);
                                bindingDialog.loDiemCaiThien.setVisibility(View.VISIBLE);
                            }
                        } else {
                            bindingDialog.btnYes.setEnabled(true);
                            showCPACaiThien(hocPhanModel, Utils.getInstance().convertDiemChuToDiemSo(listDiemChu[i]));
                            bindingDialog.loCaiThienLui.setVisibility(View.GONE);
                            bindingDialog.loDiemCaiThien.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            bindingDialog.btnYes.setOnClickListener(view -> {
                if (hocPhanModel.isHPNew()) {
                    saveHPMoi(hocPhanModel.getMaHocPhan(), hocPhanModel.getTenHP(), hocPhanModel.getSoTinChi(), listDiemChu[bindingDialog.seekbar.getProgress()]);
                } else {
                    saveHPCaiThien(hocPhanModel, listDiemChu[bindingDialog.seekbar.getProgress()]);
                }

                if (binding.btnAll.isChecked()) {
                    showListBangDiemHocPhan();
                }
                showCPA();
                detailDialog.dismiss();
            });

            bindingDialog.btnNo.setOnClickListener(view -> {
                if (hocPhanModel.isHPCaiThien()) {
                    deleteHPCaiThien(hocPhanModel);
                } else if (hocPhanModel.isHPNew()) {
                    deleteHocPhanNew(hocPhanModel);
                } else {
                    showDialogConfirmDeleteHP(hocPhanModel);
                }
                detailDialog.dismiss();
            });

            bindingDialog.txtTenHP.setSelected(true);
            detailDialog.setCancelable(false);
            bindingDialog.btnClose.setOnClickListener(view -> {
                showCPA();
                detailDialog.dismiss();
            });
            detailDialog.show();
        }
    }

    private void showDialogConfirmDeleteHP(final HocPhanModel hocPhanModel) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        DialogConfirmBinding dialogConfirmBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_confirm, null, false);
        dialog.setContentView(dialogConfirmBinding.getRoot());

        dialogConfirmBinding.cbReadAndAccess.setVisibility(View.GONE);
        dialogConfirmBinding.txtTitle.setText(getString(R.string.chu_y));
        dialogConfirmBinding.txtDesc.setText(getString(R.string.xoa_hoc_phan_chinh));
        dialogConfirmBinding.btnContinues.setText(getString(R.string.xoa));

        dialogConfirmBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialogConfirmBinding.btnContinues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteHocPhan(hocPhanModel);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void deleteHocPhan(HocPhanModel hocPhanModel) {
        if (getContext() == null) {
            return;
        }
        bangDiemHP = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN);
        String bangHocPhanKhongTinhDiem = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_KHONG_TINH_DIEM);
        try {
            JSONArray bangDiemHPJsonA = new JSONArray(bangDiemHP);
            JSONArray bangDiemHPAfterDelete = new JSONArray();
            JSONArray bangHPKhongTinhDiemJsonA;
            if (bangHocPhanKhongTinhDiem == null || bangHocPhanKhongTinhDiem.equals("")) {
                bangHPKhongTinhDiemJsonA = new JSONArray();
            } else {
                bangHPKhongTinhDiemJsonA = new JSONArray(bangHocPhanKhongTinhDiem);
            }
            for (int i = 0; i < bangDiemHPJsonA.length(); i++) {
                JSONObject object = bangDiemHPJsonA.getJSONObject(i);
                if (!object.getString(JsonUtils.KEY_MA_HP).equals(hocPhanModel.getMaHocPhan())) {
                    bangDiemHPAfterDelete.put(object);
                } else {
                    bangHPKhongTinhDiemJsonA.put(object);
                }
            }
            Utils.getInstance().saveToSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN, bangDiemHPAfterDelete.toString());
            Utils.getInstance().saveToSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_KHONG_TINH_DIEM, bangHPKhongTinhDiemJsonA.toString());
            showCPA();
            if (binding.btnAll.isChecked())
                showListBangDiemHocPhan();
        } catch (Exception e) {
            Log.e("RAKAN", e.toString());
            e.printStackTrace();
        }
    }

    private void deleteHocPhanNew(HocPhanModel hocPhanModel) {
        if (getContext() == null) {
            return;
        }
        bangHPNew = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI);
        try {
            JSONArray bangHPNewJsonA = new JSONArray(bangHPNew);
            JSONArray bangHPNewAfterDelete = new JSONArray();
            for (int i = 0; i < bangHPNewJsonA.length(); i++) {
                JSONObject object = bangHPNewJsonA.getJSONObject(i);
                if (!object.getString(JsonUtils.KEY_MA_HP).equals(hocPhanModel.getMaHocPhan())) {
                    bangHPNewAfterDelete.put(object);
                }
            }
            Utils.getInstance().saveToSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI, bangHPNewAfterDelete.toString());
            showCPA();
            if (binding.btnAll.isChecked())
                showListBangDiemHocPhan();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteHPCaiThien(HocPhanModel hocPhanModel) {
        if (getContext() == null) {
            return;
        }
        bangHPCaiThien = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN);
        try {
            JSONArray bangHPCaThien = new JSONArray(bangHPCaiThien);
            JSONArray bangHPCaThienNew = new JSONArray();
            for (int i = 0; i < bangHPCaThien.length(); i++) {
                JSONObject object = bangHPCaThien.getJSONObject(i);
                if (!object.getString(JsonUtils.KEY_MA_HP).equals(hocPhanModel.getMaHocPhan())) {
                    bangHPCaThienNew.put(object);
                }
            }
            Utils.getInstance().saveToSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN, bangHPCaThienNew.toString());
            showCPA();
            if (binding.btnAll.isChecked())
                showListBangDiemHocPhan();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveHPCaiThien(HocPhanModel hocPhanModel, String diemCaiThien) {
        try {
            JSONObject hpCaiThienJsonO = new JSONObject();
            hpCaiThienJsonO.put(JsonUtils.KEY_MA_HP, hocPhanModel.getMaHocPhan());
            hpCaiThienJsonO.put(JsonUtils.KEY_TEN_HP, hocPhanModel.getTenHP());
            hpCaiThienJsonO.put(JsonUtils.KEY_TIN_CHI_HP, hocPhanModel.getSoTinChi());
            hpCaiThienJsonO.put(JsonUtils.KEY_DIEM_TONG_KET, hocPhanModel.getDiemTongKet());
            hpCaiThienJsonO.put(JsonUtils.KEY_DIEM_CAI_THIEN, diemCaiThien);

            if (getContext() != null) {
                JSONArray newBangHPCaiThienJsonA = new JSONArray();
                bangHPCaiThien = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN);
                if (bangHPCaiThien != null && !bangHPCaiThien.equals("")) {
                    JSONArray bangHPCaiThienJsonA = new JSONArray(bangHPCaiThien);
                    if (bangHPCaiThienJsonA.length() > 0) {
                        for (int i = 0; i < bangHPCaiThienJsonA.length(); i++) {
                            JSONObject object = bangHPCaiThienJsonA.getJSONObject(i);
                            if (!object.getString(JsonUtils.KEY_MA_HP).equals(hocPhanModel.getMaHocPhan())) {
                                newBangHPCaiThienJsonA.put(object);
                            }
                        }
                    }
                }

                newBangHPCaiThienJsonA.put(hpCaiThienJsonO);
                Utils.getInstance().saveToSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN, newBangHPCaiThienJsonA.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCPACaiThien(HocPhanModel hocPhanModel, double diemCaiThienNew) {
        showCPA(); //to reset total and soTCTotal
        if (hocPhanModel.isHPCaiThien()) {
            total = total + (diemCaiThienNew - Utils.getInstance().convertDiemChuToDiemSo(hocPhanModel.getDiemCaiThien())) * Integer.parseInt(hocPhanModel.getSoTinChi());
            binding.txtCPA.setText(String.valueOf((double) Math.round(total / soTCTotal * 100) / 100));
        } else {
            boolean isHPCaiThien = false;
            try {
                JSONArray bangDiemHPCaiThienJsonA = new JSONArray(bangHPCaiThien);
                JSONObject object = null;
                for (int i = 0; i < bangDiemHPCaiThienJsonA.length(); i++) {
                    object = bangDiemHPCaiThienJsonA.getJSONObject(i);
                    if (hocPhanModel.getMaHocPhan().equals(object.getString(JsonUtils.KEY_MA_HP))) {
                        isHPCaiThien = true;
                        break;
                    }
                }
                if (isHPCaiThien) {
                    total = total + (diemCaiThienNew - Utils.getInstance().convertDiemChuToDiemSo(object.getString(JsonUtils.KEY_DIEM_CAI_THIEN))) * Integer.parseInt(hocPhanModel.getSoTinChi());
                    binding.txtCPA.setText(String.valueOf((double) Math.round(total / soTCTotal * 100) / 100));
                } else {
                    total = total + (diemCaiThienNew - Utils.getInstance().convertDiemChuToDiemSo(hocPhanModel.getDiemTongKet())) * Integer.parseInt(hocPhanModel.getSoTinChi());
                    binding.txtCPA.setText(String.valueOf((double) Math.round(total / soTCTotal * 100) / 100));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        showXepLoai();
    }

    private void showDialogHuongDan() {
        Utils.getInstance().showNoticeDialog(getContext(),
                "Mẹo", "Bảng điểm học phần dùng để xem điểm, thêm học phần mới và chỉnh sửa điểm học phần đã học để dự tính trước điểm CPA.\n - Nhấn vào nút (+) để thêm học phần mới.\n - Nhấn vào học phần đã học để sửa điểm.");
    }

    private void showXepLoai() {
        double cpa = Double.parseDouble(binding.txtCPA.getText().toString().trim());
        if (cpa >= 3.6) {
            binding.txtXepLoai.setText("Xuất Sắc");
        } else if (cpa >= 3.2) {
            binding.txtXepLoai.setText("Giỏi");
        } else if (cpa >= 2.5) {
            binding.txtXepLoai.setText("Khá");
        } else if (cpa >= 2) {
            binding.txtXepLoai.setText("Trung Bình");
        } else if (cpa >= 1.5) {
            binding.txtXepLoai.setText("Trung Bình Yếu");
        } else if (cpa >= 1) {
            binding.txtXepLoai.setText("Yếu");
        } else {
            binding.txtXepLoai.setText("Kém");
        }

    }

    private void showHPFilter(String diem) {
        if (diem.equals("All")) {
            showListBangDiemHocPhan();
            return;
        }
        int tongSoTC = 0;
        hocPhanModels = new ArrayList<>();
        if (getContext() == null) {
            return;
        }
        try {
            bangHPCaiThien = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN);
            if (bangHPCaiThien != null && !bangHPCaiThien.equals("")) {
                JSONArray bangHPCaiThienJsonA = new JSONArray(bangHPCaiThien);
                if (bangHPCaiThienJsonA.length() > 0) {
                    for (int i = bangHPCaiThienJsonA.length() - 1; i >= 0; i--) {
                        JSONObject hPCaiThienJsonO = bangHPCaiThienJsonA.getJSONObject(i);
                        String maHP = hPCaiThienJsonO.getString(JsonUtils.KEY_MA_HP);
                        String tenHP = hPCaiThienJsonO.getString(JsonUtils.KEY_TEN_HP);
                        String soTC = hPCaiThienJsonO.getString(JsonUtils.KEY_TIN_CHI_HP);
                        String diemTongKet = hPCaiThienJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        String diemCaiThien = hPCaiThienJsonO.getString(JsonUtils.KEY_DIEM_CAI_THIEN);
                        if (diemCaiThien.equals(diem)) {
                            HocPhanModel hocPhanModel = new HocPhanModel(maHP, tenHP, null, null, null, null, soTC, diemTongKet, diemCaiThien, true, false);
                            hocPhanModels.add(hocPhanModel);
                            tongSoTC += Integer.parseInt(soTC);
                        }
                    }
                }
            }

            bangHPNew = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI);
            if (bangHPNew != null && !bangHPNew.equals("")) {
                JSONArray bangHPNewJsonA = new JSONArray(bangHPNew);
                if (bangHPNewJsonA.length() > 0) {
                    for (int i = bangHPNewJsonA.length() - 1; i >= 0; i--) {
                        JSONObject hPNewJsonO = bangHPNewJsonA.getJSONObject(i);
                        String maHP = hPNewJsonO.getString(JsonUtils.KEY_MA_HP);
                        String tenHP = hPNewJsonO.getString(JsonUtils.KEY_TEN_HP);
                        String soTC = hPNewJsonO.getString(JsonUtils.KEY_TIN_CHI_HP);
                        String diemTongKet = hPNewJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        if (diemTongKet.equals(diem)) {
                            HocPhanModel hocPhanModel = new HocPhanModel(maHP, tenHP, null, null, null, null, soTC, diemTongKet, null, false, true);
                            hocPhanModels.add(hocPhanModel);
                            tongSoTC += Integer.parseInt(soTC);
                        }
                    }
                }
            }

            bangDiemHP = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN);
            if (bangDiemHP != null && !bangDiemHP.equals("")) {
                JSONArray bangDiemHPJsonA = new JSONArray(bangDiemHP);
                if (bangDiemHPJsonA.length() > 0) {
                    for (int i = 0; i < bangDiemHPJsonA.length(); i++) {
                        JSONObject diemHPJsonO = bangDiemHPJsonA.getJSONObject(i);
                        String maHP = diemHPJsonO.getString(JsonUtils.KEY_MA_HP);
                        String tenHP = diemHPJsonO.getString(JsonUtils.KEY_TEN_HP);
                        String soTC = diemHPJsonO.getString(JsonUtils.KEY_TIN_CHI_HP);
                        String diemHocPhan = diemHPJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        String hocKy = diemHPJsonO.getString(JsonUtils.KEY_HOC_KY);

                        if (diemHocPhan.equals(diem)) {
                            HocPhanModel hocPhanModel = new HocPhanModel(maHP, tenHP, hocKy, null, null, null, soTC, diem, null, false, false);
                            hocPhanModels.add(hocPhanModel);
                            tongSoTC += Integer.parseInt(soTC);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("RAKAN", "showHPFilter: " + e.toString());
        }
        if (getContext() != null) {
            if (hocPhanModels.size() == 0) {
                return;
            }
            bangDiemAdapter = new BangDiemAdapter(getContext(), R.layout.item_bang_diem_hoc_phan, hocPhanModels);
            binding.listBangDiem.setAdapter(bangDiemAdapter);
            binding.txtTongSoTC.setText(tongSoTC + " tín chỉ");
        }
    }
}

