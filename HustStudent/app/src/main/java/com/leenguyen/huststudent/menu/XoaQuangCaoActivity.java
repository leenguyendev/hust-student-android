package com.leenguyen.huststudent.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.leenguyen.huststudent.R;

import com.leenguyen.huststudent.databinding.ActivityXoaQuangCaoBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import vn.momo.momo_partner.AppMoMoLib;
import vn.momo.momo_partner.MoMoParameterNamePayment;

public class XoaQuangCaoActivity extends AppCompatActivity {
    private ActivityXoaQuangCaoBinding binding;
    String zptranstoken, code;

    //MoMo
    private String amount = "1000";
    private String fee = "0";
    private String merchantName = "Lê Xuân Nhất";
    private String merchantCode = "MOMOFDUN20211109";
    private String merchantNameLabel = "Lê Xuân Nhất";
    private String description = "Dang ky xoa quang cao HUST Student";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_xoa_quang_cao);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        binding.btnBack.setOnClickListener(v -> onBackPressed());
        binding.txtSoDienThoai.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("", binding.txtSoDienThoai.getText());
            if (clipboard != null) {
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getApplicationContext(), "Đã sao chép số điện thoại", Toast.LENGTH_LONG).show();
            }
        });

//        //Zalo pay ----------------------------------------------------------------------------------------------------
//        StrictMode.ThreadPolicy policy = new
//                StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
//        ZaloPaySDK.init(2554, Environment.SANDBOX);
//        CreateOrder orderApi = new CreateOrder();
//        try {
//            JSONObject data = orderApi.createOrder("50000");
//            code = data.getString("returncode");
//            zptranstoken = data.getString("zptranstoken");
//            Log.d("RAKAN", "zptranstoken: " + zptranstoken + " code: " + code);
//        } catch (Exception e) {
//
//        }
//        binding.zaloPay.setOnClickListener(v -> {
//            try {
//                ZaloPaySDK.getInstance().payOrder(XoaQuangCaoActivity.this, zptranstoken, "Merchant Demo V2://app", new PayOrderListener() {
//                    @Override
//                    public void onPaymentSucceeded(final String transactionId, final String transToken, final String appTransID) {
//                        runOnUiThread(() -> new AlertDialog.Builder(XoaQuangCaoActivity.this)
//                                .setTitle("Payment Success")
//                                .setMessage(String.format("TransactionId: %s - TransToken: %s", transactionId, transToken))
//                                .setPositiveButton("OK", (dialog, which) -> {
//                                })
//                                .setNegativeButton("Cancel", null).show());
////                            IsLoading();
//                    }
//
//                    @Override
//                    public void onPaymentCanceled(String s, String s1) {
//                        try {
//                            new AlertDialog.Builder(XoaQuangCaoActivity.this)
//                                    .setTitle("User Cancel Payment")
//                                    .setMessage(String.format("zpTransToken: %s \n", zptranstoken))
//                                    .setPositiveButton("OK", (dialog, which) -> {
//                                    })
//                                    .setNegativeButton("Cancel", null).show();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                    @Override
//                    public void onPaymentError(ZaloPayError zaloPayError, String s, String s1) {
//                        try {
//                            new AlertDialog.Builder(XoaQuangCaoActivity.this)
//                                    .setTitle("Payment Fail")
//                                    .setMessage(String.format("ZaloPayErrorCode: %s \nTransToken: %s", zaloPayError.toString(), zptranstoken))
//                                    .setPositiveButton("OK", (dialog, which) -> {
//                                    })
//                                    .setNegativeButton("Cancel", null).show();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        //MoMo ---------------------------------------------------------------------------------------------------------
//        AppMoMoLib.getInstance().setEnvironment(AppMoMoLib.ENVIRONMENT.PRODUCTION); // AppMoMoLib.ENVIRONMENT.PRODUCTION
//        binding.momo.setOnClickListener(v -> requestPayment());
    }

    //Get token through MoMo app
    private void requestPayment() {
        AppMoMoLib.getInstance().setAction(AppMoMoLib.ACTION.PAYMENT);
        AppMoMoLib.getInstance().setActionType(AppMoMoLib.ACTION_TYPE.GET_TOKEN);

        Map<String, Object> eventValue = new HashMap<>();
        //client Required
        eventValue.put(MoMoParameterNamePayment.MERCHANT_NAME, merchantName);
        eventValue.put(MoMoParameterNamePayment.MERCHANT_CODE, merchantCode);
        eventValue.put(MoMoParameterNamePayment.AMOUNT, amount);
        eventValue.put(MoMoParameterNamePayment.DESCRIPTION, description);
        //client Optional
        eventValue.put(MoMoParameterNamePayment.FEE, fee);
        eventValue.put(MoMoParameterNamePayment.MERCHANT_NAME_LABEL, merchantNameLabel);

        eventValue.put(MoMoParameterNamePayment.REQUEST_ID,  merchantCode+"-"+ UUID.randomUUID().toString());
        eventValue.put(MoMoParameterNamePayment.PARTNER_CODE, "CGV19072017");

        JSONObject objExtraData = new JSONObject();
        try {
            objExtraData.put("site_code", "008");
            objExtraData.put("site_name", "CGV Cresent Mall");
            objExtraData.put("screen_code", 0);
            objExtraData.put("screen_name", "Special");
            objExtraData.put("movie_name", "Kẻ Trộm Mặt Trăng 3");
            objExtraData.put("movie_format", "2D");
            objExtraData.put("ticket", "{\"ticket\":{\"01\":{\"type\":\"std\",\"price\":110000,\"qty\":3}}}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        eventValue.put(MoMoParameterNamePayment.EXTRA_DATA, objExtraData.toString());
        eventValue.put(MoMoParameterNamePayment.REQUEST_TYPE, "payment");
        eventValue.put(MoMoParameterNamePayment.LANGUAGE, "vi");
        eventValue.put(MoMoParameterNamePayment.EXTRA, "");
        //Request momo app
        AppMoMoLib.getInstance().requestMoMoCallBack(this, eventValue);
    }

//    //Get token callback from MoMo app an submit to server side
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == AppMoMoLib.getInstance().REQUEST_CODE_MOMO && resultCode == -1) {
//            if (data != null) {
//                if (data.getIntExtra("status", -1) == 0) {
//                    //TOKEN IS AVAILABLE
//                    Log.d("RAKAN", "message: " + "Get token " + data.getStringExtra("message"));
//                    String token = data.getStringExtra("data"); //Token response
//                    String phoneNumber = data.getStringExtra("phonenumber");
//                    String env = data.getStringExtra("env");
//                    if (env == null) {
//                        env = "app";
//                    }
//
//                    if (token != null && !token.equals("")) {
//                        // TODO: send phoneNumber & token to your server side to process payment with MoMo server
//                        // IF Momo topup success, continue to process your order
//                    } else {
//                        Log.d("RAKAN", "message: not_receive_info");
//                    }
//                } else if (data.getIntExtra("status", -1) == 1) {
//                    //TOKEN FAIL
//                    String message = data.getStringExtra("message") != null ? data.getStringExtra("message") : "Thất bại";
//                    Log.d("RAKAN", "message: " + message);
//                } else if (data.getIntExtra("status", -1) == 2) {
//                    //TOKEN FAIL
//                    Log.d("RAKAN", "message: not_receive_info");
//                } else {
//                    //TOKEN FAIL
//                    Log.d("RAKAN", "message: not_receive_info");
//                }
//            } else {
//                Log.d("RAKAN", "message: not_receive_info");
//            }
//        } else {
//            Log.d("RAKAN", "message: not_receive_info_err");
//        }
//    }
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        ZaloPaySDK.getInstance().onResult(intent);
//    }

}