package com.leenguyen.huststudent.menu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.databinding.ActivityHustVipBinding;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONObject;

public class HustVipActivity extends AppCompatActivity {
    private ActivityHustVipBinding binding;
    private DatabaseReference vipMemberDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hust_vip);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        vipMemberDatabase = FirebaseDatabase.getInstance("https://hust-student-vip-member.firebaseio.com/").getReference();
        initLayout();
    }

    private void initLayout() {
        binding.btnBack.setOnClickListener(v -> onBackPressed());

        binding.btnDanhSachKhoaHoc.setOnClickListener(view -> Utils.getInstance().sendIntentActivity(getApplicationContext(), DanhSachKhoaHocActivity.class));

        binding.txtSoDienThoai.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("", binding.txtSoDienThoai.getText());
            if (clipboard != null) {
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getApplicationContext(), "Đã sao chép số điện thoại", Toast.LENGTH_LONG).show();
            }
        });

        if (!Utils.getInstance().isOnline(this)) {
            Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), getString(R.string.can_co_ket_noi_mang));
        }

        vipMemberDatabase.child("hocPhi").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                binding.txtHocPhi.setText(String.format(getString(R.string.cach_dang_ky_vip), snapshot.getValue()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        vipMemberDatabase.child("password").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                binding.txtPassword.setText(snapshot.getValue(String.class));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        String mssv;
        String studentInfoData = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        try {
            JSONObject studentInfoJsonO = new JSONObject(studentInfoData);
            mssv = studentInfoJsonO.getString(JsonUtils.KEY_MA_SO_SV);
            vipMemberDatabase.child("danhSach").child(mssv).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.getValue() == null) {
                        binding.txtTenDangNhapVaPass.setVisibility(View.GONE);
                        binding.txtUesname.setVisibility(View.GONE);
                        binding.txtPassword.setVisibility(View.GONE);
                        binding.txtChuY.setVisibility(View.GONE);
                    } else {
                        binding.txtTenDangNhapVaPass.setVisibility(View.VISIBLE);
                        binding.txtUesname.setVisibility(View.VISIBLE);
                        binding.txtPassword.setVisibility(View.VISIBLE);
                        binding.txtChuY.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RAKAN", "HustVipActivity: " + e.toString());
        }
    }
}