package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.TKBTamThoiModel;

import java.util.ArrayList;

public class TKBTamThoiAdapter extends ArrayAdapter<TKBTamThoiModel> {
    private Context context;
    private ArrayList<TKBTamThoiModel> tkbTamThoiModels;
    private int layoutResource;

    public TKBTamThoiAdapter(@NonNull Context context, int resource, @NonNull ArrayList<TKBTamThoiModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.tkbTamThoiModels = objects;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtLopHoc = row.findViewById(R.id.txtLopHoc);
        TextView txtThoiGian = row.findViewById(R.id.txtThoiGian);
        TextView txtPhongHoc = row.findViewById(R.id.txtPhongHoc);
        txtLopHoc.setText(tkbTamThoiModels.get(position).getLopHoc());
        txtThoiGian.setText("Thứ " + tkbTamThoiModels.get(position).getThu() + "  " + tkbTamThoiModels.get(position).getThoiGian() + "  " + tkbTamThoiModels.get(position).getTuanHoc());
        txtPhongHoc.setText(tkbTamThoiModels.get(position).getPhongHoc());
        return row;
    }

}
