package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.HocPhiModel;

import java.util.ArrayList;

public class HocPhiAdapter extends ArrayAdapter<HocPhiModel> {
    private Context context;
    private ArrayList<HocPhiModel> hocPhiModels;
    private int layoutResource;

    public HocPhiAdapter(@NonNull Context context, int resource, @NonNull ArrayList<HocPhiModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.hocPhiModels = objects;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtTenHocPhan = row.findViewById(R.id.txtHocPhan);
        TextView txtHocPhiMotTinChi = row.findViewById(R.id.txtHocPhiMotTinChi);
        TextView txtSoTinChiHocPhi = row.findViewById(R.id.txtSoTinChiHocPhi);
        TextView txtTongHocPhiHocPhan = row.findViewById(R.id.txtTongTienHocPhan);

        txtTenHocPhan.setText(hocPhiModels.get(position).getTenHP());
        txtHocPhiMotTinChi.setText("HP/TC: " + hocPhiModels.get(position).getSoTienMotTCHocPhi());
        txtSoTinChiHocPhi.setText("TCHP: " + hocPhiModels.get(position).getSoTinChiHocPhi());
        txtTongHocPhiHocPhan.setText(hocPhiModels.get(position).getTongSoTienHocPhan());

        return row;
    }
}
