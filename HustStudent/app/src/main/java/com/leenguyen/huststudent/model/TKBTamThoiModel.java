package com.leenguyen.huststudent.model;

public class TKBTamThoiModel {
    private String thu, thoiGian, tuanHoc, phongHoc, lopHoc;

    public TKBTamThoiModel(String thu, String thoiGian, String tuanHoc, String phongHoc, String lopHoc) {
        this.thu = thu;
        this.thoiGian = thoiGian;
        this.tuanHoc = tuanHoc;
        this.phongHoc = phongHoc;
        this.lopHoc = lopHoc;
    }

    public String getThu() {
        return thu;
    }

    public void setThu(String thu) {
        this.thu = thu;
    }

    public String getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(String thoiGian) {
        this.thoiGian = thoiGian;
    }

    public String getTuanHoc() {
        return tuanHoc;
    }

    public void setTuanHoc(String tuanHoc) {
        this.tuanHoc = tuanHoc;
    }

    public String getPhongHoc() {
        return phongHoc;
    }

    public void setPhongHoc(String phongHoc) {
        this.phongHoc = phongHoc;
    }

    public String getLopHoc() {
        return lopHoc;
    }

    public void setLopHoc(String lopHoc) {
        this.lopHoc = lopHoc;
    }
}
