package com.leenguyen.huststudent.find_love;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.databinding.ActivityDacDiemBanThanVaNguoiAyBinding;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONObject;

public class DacDiemBanThanVaNguoiAyActivity extends AppCompatActivity {
    private ActivityDacDiemBanThanVaNguoiAyBinding binding;
    private DatabaseReference loveDatabase;
    private String kyTuSo_1, kyTuSo_2, kyTuSo_3, kyTuSo_4;
    private String hoSoTimNguoiYeu;
    private DatabaseReference vipMemberDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dac_diem_ban_than_va_nguoi_ay);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        loveDatabase = FirebaseDatabase.getInstance("https://hust-student-love.firebaseio.com/").getReference();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hoSoTimNguoiYeu = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_HO_SO_TIM_NGUOI_YEU);
        initLayout();
        updateButtonLamBaiTracNghiemStatus();
    }

    private void updateButtonLamBaiTracNghiemStatus() {
        if (hoSoTimNguoiYeu.length() == 34) {
            String cauTraLoiTracNghiem = hoSoTimNguoiYeu.substring(4);
            if (!cauTraLoiTracNghiem.contains("0")) {
                binding.btnBaiTracNghiem.setBackgroundResource(R.drawable.selected_button_green);
                binding.btnBaiTracNghiem.setText("Cập nhật câu trả lời");
            } else {
                binding.btnBaiTracNghiem.setBackgroundResource(R.drawable.bg_select_button);
                binding.btnBaiTracNghiem.setText("Làm bài trắc nghiệm tính cách");
            }
        }
    }

    private void initLayout() {
        hienThiLuaChonCu();

        binding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.btnBaiTracNghiem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().sendIntentActivity(DacDiemBanThanVaNguoiAyActivity.this, TracNghiemTinhCachActivity.class);
            }
        });

        binding.rdbgChieuCaobanThan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdbChieuCaoBanThan_1:
                        kyTuSo_1 = "1";
                        break;
                    case R.id.rdbChieuCaoBanThan_2:
                        kyTuSo_1 = "2";
                        break;
                    case R.id.rdbChieuCaoBanThan_3:
                        kyTuSo_1 = "3";
                        break;
                    case R.id.rdbChieuCaoBanThan_4:
                        kyTuSo_1 = "4";
                        break;
                }
            }
        });

        binding.rdbgCanNangBanThan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdbCanNangBanThan_1:
                        kyTuSo_2 = "1";
                        break;
                    case R.id.rdbCanNangBanThan_2:
                        kyTuSo_2 = "2";
                        break;
                    case R.id.rdbCanNangBanThan_3:
                        kyTuSo_2 = "3";
                        break;
                    case R.id.rdbCanNangBanThan_4:
                        kyTuSo_2 = "4";
                        break;
                }
            }
        });

        binding.rdbgChieuCaoNguoiAy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdbChieuCaoNguoiAy_1:
                        kyTuSo_3 = "1";
                        break;
                    case R.id.rdbChieuCaoNguoiAy_2:
                        kyTuSo_3 = "2";
                        break;
                    case R.id.rdbChieuCaoNguoiAy_3:
                        kyTuSo_3 = "3";
                        break;
                    case R.id.rdbChieuCaoNguoiAy_4:
                        kyTuSo_3 = "4";
                        break;
                }
            }
        });

        binding.rdbgCanNangNguoiAy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdbCanNangNguoiAy_1:
                        kyTuSo_4 = "1";
                        break;
                    case R.id.rdbCanNangNguoiAy_2:
                        kyTuSo_4 = "2";
                        break;
                    case R.id.rdbCanNangNguoiAy_3:
                        kyTuSo_4 = "3";
                        break;
                    case R.id.rdbCanNangNguoiAy_4:
                        kyTuSo_4 = "4";
                        break;
                }
            }
        });

        binding.btnCapNhatHoSo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capNhatHoSo();
            }
        });
    }

    private void capNhatHoSo() {
        if (hoSoTimNguoiYeu.length() != 34) {
            return;
        }
        String cauTraLoiTracNghiemTinhCach = hoSoTimNguoiYeu.substring(4);
        String hoSoTimNguoiYeuNew = kyTuSo_1 + kyTuSo_2 + kyTuSo_3 + kyTuSo_4 + cauTraLoiTracNghiemTinhCach;
        Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_HO_SO_TIM_NGUOI_YEU,
                hoSoTimNguoiYeuNew);

        String dataThongTinCaNhanSV = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        String mssv, gioiTinh;
        try {
            JSONObject thongTinCaNhanSV = new JSONObject(dataThongTinCaNhanSV);
            mssv = thongTinCaNhanSV.getString(JsonUtils.KEY_MA_SO_SV).trim();
            gioiTinh = thongTinCaNhanSV.getString(JsonUtils.KEY_GIOI_TINH).trim();
            String gioiTinhCheck;
            if (gioiTinh.equals("Nữ")) {
                gioiTinhCheck = "nu";
            } else {
                gioiTinhCheck = "nam";
            }
            if (Utils.getInstance().isOnline(getApplicationContext())) {
                loveDatabase.child(gioiTinhCheck).child(mssv).child("dacDiemCaNhanVaNguoiAy").setValue(hoSoTimNguoiYeuNew);
                if (hoSoTimNguoiYeuNew.contains("0")) {
                    Toast.makeText(getApplicationContext(), "Thím chưa trả lời hết câu hỏi", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Cập nhật thông tin tìm người yêu thành công, bắt đầu tìm người yêu thôi ^.^", Toast.LENGTH_LONG).show();
                    finish();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Cập nhật thất bại vì không có kết nối mạng", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            Log.e("RAKAN", "e " + e.toString());
            e.printStackTrace();
        }
    }

    private void hienThiLuaChonCu() {
        Log.d("RAKAN", "hoSoTimNguoiYeu: " + hoSoTimNguoiYeu);
        if (hoSoTimNguoiYeu.length() == 34) {
            kyTuSo_1 = String.valueOf(hoSoTimNguoiYeu.charAt(0));
            kyTuSo_2 = String.valueOf(hoSoTimNguoiYeu.charAt(1));
            kyTuSo_3 = String.valueOf(hoSoTimNguoiYeu.charAt(2));
            kyTuSo_4 = String.valueOf(hoSoTimNguoiYeu.charAt(3));
            if (kyTuSo_1.equals("1")) {
                binding.rdbChieuCaoBanThan1.setChecked(true);
            } else if (kyTuSo_1.equals("2")) {
                binding.rdbChieuCaoBanThan2.setChecked(true);
            } else if (kyTuSo_1.equals("3")) {
                binding.rdbChieuCaoBanThan3.setChecked(true);
            } else if (kyTuSo_1.equals("4")) {
                binding.rdbChieuCaoBanThan4.setChecked(true);
            } else {
                binding.rdbChieuCaoBanThan1.setChecked(false);
                binding.rdbChieuCaoBanThan2.setChecked(false);
                binding.rdbChieuCaoBanThan3.setChecked(false);
                binding.rdbChieuCaoBanThan4.setChecked(false);
            }

            if (kyTuSo_2.equals("1")) {
                binding.rdbCanNangBanThan1.setChecked(true);
            } else if (kyTuSo_2.equals("2")) {
                binding.rdbCanNangBanThan2.setChecked(true);
            } else if (kyTuSo_2.equals("3")) {
                binding.rdbCanNangBanThan3.setChecked(true);
            } else if (kyTuSo_2.equals("4")) {
                binding.rdbCanNangBanThan4.setChecked(true);
            } else {
                binding.rdbCanNangBanThan1.setChecked(false);
                binding.rdbCanNangBanThan2.setChecked(false);
                binding.rdbCanNangBanThan3.setChecked(false);
                binding.rdbCanNangBanThan4.setChecked(false);
            }

            if (kyTuSo_3.equals("1")) {
                binding.rdbChieuCaoNguoiAy1.setChecked(true);
            } else if (kyTuSo_3.equals("2")) {
                binding.rdbChieuCaoNguoiAy2.setChecked(true);
            } else if (kyTuSo_3.equals("3")) {
                binding.rdbChieuCaoNguoiAy3.setChecked(true);
            } else if (kyTuSo_3.equals("4")) {
                binding.rdbChieuCaoNguoiAy4.setChecked(true);
            } else {
                binding.rdbChieuCaoNguoiAy1.setChecked(false);
                binding.rdbChieuCaoNguoiAy2.setChecked(false);
                binding.rdbChieuCaoNguoiAy3.setChecked(false);
                binding.rdbChieuCaoNguoiAy4.setChecked(false);
            }

            if (kyTuSo_4.equals("1")) {
                binding.rdbCanNangNguoiAy1.setChecked(true);
            } else if (kyTuSo_4.equals("2")) {
                binding.rdbCanNangNguoiAy2.setChecked(true);
            } else if (kyTuSo_4.equals("3")) {
                binding.rdbCanNangNguoiAy3.setChecked(true);
            } else if (kyTuSo_4.equals("4")) {
                binding.rdbCanNangNguoiAy4.setChecked(true);
            } else {
                binding.rdbCanNangNguoiAy1.setChecked(false);
                binding.rdbCanNangNguoiAy2.setChecked(false);
                binding.rdbCanNangNguoiAy3.setChecked(false);
                binding.rdbCanNangNguoiAy4.setChecked(false);
            }
        }
    }
}