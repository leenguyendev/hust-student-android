package com.leenguyen.huststudent.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.api.RetrofitApi;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

public class JsoupUtils {
    private static JsoupUtils instance;
    private HashMap<String, String> cookies;
    private Context context;

    public static JsoupUtils getInstance() {
        if (instance == null) {
            instance = new JsoupUtils();
        }
        return instance;
    }

    public boolean login(Context context, String userName, String password, String captcha, String loginCode, HashMap<String, String> cookies, int typeLogin) {
        this.cookies = cookies;
        this.context = context;
        boolean isLoginSuccess = false;
        String keyDataLogin7, valueDataLogin17, valueDataLogin18;
        if (typeLogin == 0) {
            keyDataLogin7 = Constant.KEY_FORM_DATA_LOGIN_7;
            valueDataLogin17 = Constant.VALUE_FORM_DATA_LOGIN_17;
            valueDataLogin18 = Constant.VALUE_FORM_DATA_LOGIN_18;
        } else if (typeLogin == 1) {
            keyDataLogin7 = Constant.KEY_FORM_DATA_LOGIN_7_2;
            valueDataLogin17 = Constant.VALUE_FORM_DATA_LOGIN_17_2;
            valueDataLogin18 = Constant.VALUE_FORM_DATA_LOGIN_18_2;
        } else if (typeLogin == 2) {
            keyDataLogin7 = Constant.KEY_FORM_DATA_LOGIN_7;
            valueDataLogin17 = Constant.VALUE_FORM_DATA_LOGIN_17_2;
            valueDataLogin18 = Constant.VALUE_FORM_DATA_LOGIN_18_2;
        } else {
            keyDataLogin7 = Constant.KEY_FORM_DATA_LOGIN_7;
            valueDataLogin17 = Constant.VALUE_FORM_DATA_LOGIN_17;
            valueDataLogin18 = Constant.VALUE_FORM_DATA_LOGIN_18_2;
        }
        try {
            Connection.Response response = Jsoup.connect(Constant.URL_LOGIN)
                    .cookies(cookies)
                    .data(Constant.KEY_FORM_DATA_LOGIN_1, Constant.VALUE_FORM_DATA_LOGIN_1)
                    .data(Constant.KEY_FORM_DATA_LOGIN_2, Constant.VALUE_FORM_DATA_LOGIN_2)
                    .data(Constant.KEY_FORM_DATA_LOGIN_3, Utils.getInstance().getValueFromSharedPreferences(context,
                            Constant.SHARE_PREFERENCES_FORM_DATA_LOGIN,
                            Constant.KEY_SHARE_PREFERENCES_FORM_DATA_LOGIN_3))
                    .data(Constant.KEY_FORM_DATA_LOGIN_4, Constant.VALUE_FORM_DATA_LOGIN_4)
                    .data(Constant.KEY_FORM_DATA_LOGIN_5, Utils.getInstance().getValueFromSharedPreferences(context,
                            Constant.SHARE_PREFERENCES_FORM_DATA_LOGIN,
                            Constant.KEY_SHARE_PREFERENCES_FORM_DATA_LOGIN_5))
                    .data(Constant.KEY_FORM_DATA_LOGIN_6, Constant.VALUE_FORM_DATA_LOGIN_6)
                    .data(keyDataLogin7, Constant.VALUE_FORM_DATA_LOGIN_7)
                    .data(Constant.KEY_FORM_DATA_LOGIN_8, Constant.VALUE_FORM_DATA_LOGIN_8)
                    .data(Constant.KEY_FORM_DATA_LOGIN_9, Constant.VALUE_FORM_DATA_LOGIN_9_HEAD + userName + Constant.VALUE_FORM_DATA_LOGIN_9_END)
                    .data(Constant.KEY_FORM_DATA_LOGIN_10, userName)
                    .data(Constant.KEY_FORM_DATA_LOGIN_11, Constant.VALUE_FORM_DATA_LOGIN_11_HEAD + password + Constant.VALUE_FORM_DATA_LOGIN_11_END)
                    .data(Constant.KEY_FORM_DATA_LOGIN_12, password)
                    .data(Constant.KEY_FORM_DATA_LOGIN_13, Constant.VALUE_FORM_DATA_LOGIN_13)
                    .data(Constant.KEY_FORM_DATA_LOGIN_14, captcha)
                    .data(Constant.KEY_FORM_DATA_LOGIN_15, Constant.VALUE_FORM_DATA_LOGIN_15)
                    .data(Constant.KEY_FORM_DATA_LOGIN_16, loginCode)
                    .data(Constant.KEY_FORM_DATA_LOGIN_17, valueDataLogin17)
                    .data(Constant.KEY_FORM_DATA_LOGIN_18, valueDataLogin18)
                    .followRedirects(true)
                    .method(Connection.Method.POST)
                    .timeout(60000)
                    .userAgent(Constant.USER_AGENT)
                    .execute();
            if (response != null && response.statusCode() == 200 && !response.url().toString().equals(Constant.URL_LOGIN)) {
                isLoginSuccess = true;
                getThongTinSv();
                getBangDiemHocPhan();
                getBangDiemCaNhan();
                getThoiKhoaBieu();
                getKetQuaThiToeic();
                getThongTinLopSV();
                getKiemTraNhapDiemKiMoiNhat();
                getThongTinCongNoHocPhi();
                getThoiKhoaBieuTamThoi();
                getLichThi();
            }
        } catch (Exception e) {
            Log.e("RAKAN", "login: " + e.toString());
            e.printStackTrace();
        }
        return isLoginSuccess;
    }

    private void getThongTinSv() {
        RetrofitApi.service.getThongTinSVApi("ASP.NET_SessionId=" + cookies.get("ASP.NET_SessionId")).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                JsonUtils.getInstance().parseThongTinSV(context, Jsoup.parse(response.body()));
//                getAvatar(Jsoup.parse(response.body()));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    private void getAvatar(Document document) {
        Element element1 = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_UserImageCPanel_imgUserImage");
        String linkDownloadAvatar = element1.absUrl("src");
        try {
            URL url = new URL(linkDownloadAvatar);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();

            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            if (context != null) {
                Utils.getInstance().saveToSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR, encodedImage);
                Utils.getInstance().saveToSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR_ROTATE_ANGLE, "0");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void getThongTinCongNoHocPhi() {
        RetrofitApi.service.getThongTinCongNoHocPhi("ASP.NET_SessionId=" + cookies.get("ASP.NET_SessionId")).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                JsonUtils.getInstance().parseThongTinHocPhi(context, Jsoup.parse(response.body()));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    void getThoiKhoaBieuTamThoi() {
        RetrofitApi.service.getThoiKhoaBieuTamThoi("ASP.NET_SessionId=" + cookies.get("ASP.NET_SessionId")).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                JsonUtils.getInstance().parseTKBTamThoi(context, Jsoup.parse(response.body()));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    void getKiemTraNhapDiemKiMoiNhat() {
        RetrofitApi.service.getKiemTraNhapDiemKiMoiNhat("ASP.NET_SessionId=" + cookies.get("ASP.NET_SessionId")).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                JsonUtils.getInstance().parseThongTinNhapDiemMoiNhat(context, Jsoup.parse(response.body()));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    void getBangDiemHocPhan() {
        RetrofitApi.service.getBangDiemHocPhan("ASP.NET_SessionId=" + cookies.get("ASP.NET_SessionId")).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                JsonUtils.getInstance().parseBangDiemHocPhan(context, Jsoup.parse(response.body()));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    void getBangDiemCaNhan() {
        RetrofitApi.service.getBangDiemCaNhan("ASP.NET_SessionId=" + cookies.get("ASP.NET_SessionId")).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                JsonUtils.getInstance().parseBangDiemCaNhan(context, Jsoup.parse(response.body()));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    void getKetQuaThiToeic() {
        RetrofitApi.service.getKetQuaThiToeic("ASP.NET_SessionId=" + cookies.get("ASP.NET_SessionId")).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                JsonUtils.getInstance().parseDiemThiToeic(context, Jsoup.parse(response.body()));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    void getLichThi() {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constant.URL_GOOGLE_SHEET_SCRIP_LICH_THI, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray data = jsonObject.getJSONArray("lichThi");
                    Utils.getInstance().saveToSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LICH_THI_FULL, data.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("RAKAN", "getLichThi_1: " + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("RAKAN", "getLichThi_2: " + error.toString());
            }
        });
        requestQueue.add(stringRequest);
        //Not exists
    }

    void getThoiKhoaBieu() {
        RetrofitApi.service.getThoiKhoaBieu("ASP.NET_SessionId=" + cookies.get("ASP.NET_SessionId")).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                JsonUtils.getInstance().parseTKB(context, Jsoup.parse(response.body()));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    void getThongTinLopSV() {
        RetrofitApi.service.getThongTinLopSV("ASP.NET_SessionId=" + cookies.get("ASP.NET_SessionId")).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                JsonUtils.getInstance().parseThongTinLopSV(context, Jsoup.parse(response.body()));
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }
}
