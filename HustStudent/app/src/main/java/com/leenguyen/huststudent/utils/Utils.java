package com.leenguyen.huststudent.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.CommentReviewHocPhanModel;
import com.leenguyen.huststudent.model.HustRankingModel;
import com.leenguyen.huststudent.widget.ThoiKhoaBieuWidget;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

public class Utils {
    private static Utils instance;
    private Dialog dialogLoading, dialogNotice;

    public static Utils getInstance() {
        if (instance == null) {
            instance = new Utils();
        }
        return instance;
    }

    public void showLoadingDialog(Context context) {
        if (dialogLoading != null) {
            dialogLoading = null;
        }
        dialogLoading = new Dialog(context);
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.setContentView(R.layout.dialog_loading);
        ImageView imgLoading = dialogLoading.findViewById(R.id.imgLoading);
        Glide.with(context).load(R.drawable.ic_ami_quay).into(imgLoading);
        dialogLoading.setCancelable(false);
        dialogLoading.show();
    }

    public void hideLoadingDialog() {
        if (dialogLoading != null && dialogLoading.isShowing()) {
            dialogLoading.dismiss();
            dialogLoading = null;
        }
    }

    public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void showNoticeDialog(Context context, String title, String decs) {
        if (dialogNotice != null) {
            dialogNotice = null;
        }
        dialogNotice = new Dialog(context);
        dialogNotice.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNotice.setCancelable(false);
        dialogNotice.setContentView(R.layout.dialog_notice);
        TextView txtTitle = dialogNotice.findViewById(R.id.txtTitle);
        TextView txtDecs = dialogNotice.findViewById(R.id.txtDecs);
        Button btnOK = dialogNotice.findViewById(R.id.btnOK);
        txtTitle.setText(title);
        txtDecs.setText(decs);
        btnOK.setOnClickListener(v -> dialogNotice.dismiss());
        dialogNotice.show();
    }

    public void saveToSharedPreferences(Context context, String sharePreferences, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(sharePreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getValueFromSharedPreferences(Context context, String sharePreferences, String key) {
        SharedPreferences preferences = context.getSharedPreferences(sharePreferences, Context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public void sendIntentActivity(Context context, Class activity) {
        Intent intent = new Intent(context, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public double convertDiemChuToDiemSo(String diemChu) {
        double diemSo = 0;
        switch (diemChu) {
            case "A+":
            case "A":
                diemSo = 4;
                break;
            case "B+":
                diemSo = 3.5;
                break;
            case "B":
                diemSo = 3;
                break;
            case "C+":
                diemSo = 2.5;
                break;
            case "C":
                diemSo = 2;
                break;
            case "D+":
                diemSo = 1.5;
                break;
            case "D":
                diemSo = 1;
                break;
            case "F":
                diemSo = 0;
                break;
        }
        return diemSo;
    }

    public static ArrayList<HustRankingModel> sortHustRanking(ArrayList<HustRankingModel> arrayList) {
        Collections.sort(arrayList, new Comparator<HustRankingModel>() {
            @Override
            public int compare(HustRankingModel lhs, HustRankingModel rhs) {
                String lid = null, rid = null;
                try {
                    lid = lhs.getCpa();
                    rid = rhs.getCpa();
                    if (lid == null && rid != null) {
                        return -1;

                    }
                    if (lid != null && rid == null) {
                        return 1;
                    }

                    if (lid == null && rid == null) {
                        return 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Here you could parse string id to integer and then compare.
                return lid.compareTo(rid);
            }
        });
        return arrayList;
    }

    public static ArrayList<CommentReviewHocPhanModel> sortByLikeComment(ArrayList<CommentReviewHocPhanModel> arrayList) {
        Collections.sort(arrayList, (lhs, rhs) -> {
            int lid = 0, rid = 0;
            lid = lhs.getLike();
            rid = rhs.getLike();
            if (lid > rid) {
                return -1;
            } else {
                return 1;
            }
        });
        return arrayList;
    }

//    public static JSONArray sortIntJsonArray(JSONArray array, final String sortByKey) {
//        List<JSONObject> jsons = new ArrayList<JSONObject>();
//        for (int i = 0; i < array.length(); i++) {
//            try {
//                jsons.add(array.getJSONObject(i));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        Collections.sort(jsons, new Comparator<JSONObject>() {
//            @Override
//            public int compare(JSONObject lhs, JSONObject rhs) {
//                int lid = 0, rid = 0;
//                try {
//                    lid = lhs.getInt(sortByKey);
//                    rid = rhs.getInt(sortByKey);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                // Here you could parse string id to integer and then compare.
//                return Math.max(lid, rid);
//            }
//        });
//        return new JSONArray(jsons);
//    }

    public String getTuanHocHienTai(Context context) {
        int tuanHienTai;
        Calendar calendar = Calendar.getInstance();
        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
        try {
            int weekOfYearSaved = Integer.parseInt(Utils.getInstance().getValueFromSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_WEEK_OF_YEAR));
            int tuanHocHienTaiSaved = Integer.parseInt(Utils.getInstance().getValueFromSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_TUAN_HOC_HIEN_TAI));
            if (weekOfYear >= weekOfYearSaved) {
                tuanHienTai = tuanHocHienTaiSaved + (weekOfYear - weekOfYearSaved);
            } else {
                tuanHienTai = tuanHocHienTaiSaved + weekOfYear + (52 - weekOfYearSaved); //1 nam co 52 tuan
            }
        } catch (Exception e) {
            tuanHienTai = 0;
        }
        return String.valueOf(tuanHienTai);
    }

    public String getCurrentVersion(Context context) {
        String version = "0";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }

    public void sendBroadcastUpdateWidget(Context context) {
        Intent intent = new Intent(context, ThoiKhoaBieuWidget.class);
        intent.setAction(ThoiKhoaBieuWidget.REFRESH_ACTION);
        context.sendBroadcast(intent);
    }

    public void openFacebookPage(Context context, String linkFacebook) {
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL(context, linkFacebook);
        facebookIntent.setData(Uri.parse(facebookUrl));
        context.startActivity(facebookIntent);
    }

    private String getFacebookPageURL(Context context, String linkFacebook) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + linkFacebook;
            } else { //older versions of fb app
                return "fb://page/" + linkFacebook.substring(25).replace("/", "");
            }
        } catch (PackageManager.NameNotFoundException e) {
            return linkFacebook; //normal web url
        }
    }

    public String getKhoaHoc(Context context) {
        String khoaHoc = "";
        String dataThongTinCaNhanSV = Utils.getInstance().getValueFromSharedPreferences(context,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        try {
            JSONObject thongTinCaNhanSV = new JSONObject(dataThongTinCaNhanSV);
            khoaHoc = thongTinCaNhanSV.getString(JsonUtils.KEY_KHOA_HOC).trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return khoaHoc;
    }

    public static boolean isPhoneNumberValidate(String mobNumber, String countryCode) {
//        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
//        Phonenumber.PhoneNumber phoneNumber;
//        boolean isValid = false;
//        try {
//            String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
//            phoneNumber = phoneNumberUtil.parse(mobNumber, isoCode);
//            isValid = phoneNumberUtil.isValidNumber(phoneNumber);
//
//        } catch (NumberParseException e) {
//            e.printStackTrace();
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        } catch (NumberFormatException e) {
//            e.printStackTrace();
//        }
//        return isValid;
        return true;
    }
}
