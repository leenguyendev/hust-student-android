package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.HocPhanModel;
import com.leenguyen.huststudent.model.UserInfoLoginModel;

import java.util.ArrayList;

public class UserListAdapter extends ArrayAdapter<UserInfoLoginModel> {
    private Context context;
    private ArrayList<UserInfoLoginModel> userInfoLoginModels;
    private int layoutResource;

    public UserListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<UserInfoLoginModel> hocPhanModels) {
        super(context, resource, hocPhanModels);
        this.context = context;
        this.userInfoLoginModels = hocPhanModels;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtMaSV = row.findViewById(R.id.txtMaSV);
        txtMaSV.setText(userInfoLoginModels.get(position).getMaSV());
        return row;
    }
}
