package com.leenguyen.huststudent.model;

public class ThongTinDiemMonMoiModel {
    private String mssv, maLop, tenLop, trongSoQuaTrinh, diemQuaTrinh, thongTinDiemQuaTrinh, diemThi, thongTinDiemThi;

    public ThongTinDiemMonMoiModel(String mssv, String maLop, String tenLop, String trongSoQuaTrinh, String diemQuaTrinh, String thongTinDiemQuaTrinh, String diemThi, String thongTinDiemThi) {
        this.mssv = mssv;
        this.maLop = maLop;
        this.tenLop = tenLop;
        this.trongSoQuaTrinh = trongSoQuaTrinh;
        this.diemQuaTrinh = diemQuaTrinh;
        this.thongTinDiemQuaTrinh = thongTinDiemQuaTrinh;
        this.diemThi = diemThi;
        this.thongTinDiemThi = thongTinDiemThi;
    }

    public String getMssv() {
        return mssv;
    }

    public void setMssv(String mssv) {
        this.mssv = mssv;
    }

    public String getMaLop() {
        return maLop;
    }

    public void setMaLop(String maLop) {
        this.maLop = maLop;
    }

    public String getTenLop() {
        return tenLop;
    }

    public void setTenLop(String tenLop) {
        this.tenLop = tenLop;
    }

    public String getTrongSoQuaTrinh() {
        return trongSoQuaTrinh;
    }

    public void setTrongSoQuaTrinh(String trongSoQuaTrinh) {
        this.trongSoQuaTrinh = trongSoQuaTrinh;
    }

    public String getDiemQuaTrinh() {
        return diemQuaTrinh;
    }

    public void setDiemQuaTrinh(String diemQuaTrinh) {
        this.diemQuaTrinh = diemQuaTrinh;
    }

    public String getThongTinDiemQuaTrinh() {
        return thongTinDiemQuaTrinh;
    }

    public void setThongTinDiemQuaTrinh(String thongTinDiemQuaTrinh) {
        this.thongTinDiemQuaTrinh = thongTinDiemQuaTrinh;
    }

    public String getDiemThi() {
        return diemThi;
    }

    public void setDiemThi(String diemThi) {
        this.diemThi = diemThi;
    }

    public String getThongTinDiemThi() {
        return thongTinDiemThi;
    }

    public void setThongTinDiemThi(String thongTinDiemThi) {
        this.thongTinDiemThi = thongTinDiemThi;
    }
}
