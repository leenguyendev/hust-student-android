package com.leenguyen.huststudent.menu;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.view.View;
import android.widget.Toast;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.WebviewActivity;
import com.leenguyen.huststudent.databinding.ActivityShopBindingImpl;
import com.leenguyen.huststudent.utils.Utils;

public class ShopActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String LINK_MOC_KHOA = "https://shopee.vn/product/55014865/18726246285/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityShopBindingImpl binding = DataBindingUtil.setContentView(this, R.layout.activity_shop);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        binding.loMocKhoa.setOnClickListener(this);
        binding.loHustVip.setOnClickListener(this);
        binding.btnXemShop.setOnClickListener(this);
        binding.loRemoveAds.setOnClickListener(this);
        binding.btnBack.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loMocKhoa:
                checkNetworkAndOpenGame();
                break;
            case R.id.loHustVip:
                Utils.getInstance().sendIntentActivity(getApplicationContext(), HustVipActivity.class);
                break;
            case R.id.loRemoveAds:
                Utils.getInstance().sendIntentActivity(getApplicationContext(), XoaQuangCaoActivity.class);
                break;
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }

    private void checkNetworkAndOpenGame() {
        if (!Utils.getInstance().isOnline(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), "Cần có kết nối mạng", Toast.LENGTH_LONG).show();
        } else {
            try {
                Uri uri = Uri.parse("googlechrome://navigate?url=" + ShopActivity.LINK_MOC_KHOA);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                if (getApplicationContext() != null) {
                    if (i.resolveActivity(getApplicationContext().getPackageManager()) == null) {
                        i.setData(Uri.parse(ShopActivity.LINK_MOC_KHOA));
                    }
                    this.startActivity(i);
                }

            } catch (ActivityNotFoundException e) {
                // Chrome is not installed
                Intent intent = new Intent(getApplicationContext(), WebviewActivity.class);
                intent.putExtra("link", ShopActivity.LINK_MOC_KHOA);
                startActivity(intent);
            }
        }
    }
}