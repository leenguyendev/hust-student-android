package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.ThongTinDiemMonMoiModel;

import java.util.ArrayList;
import java.util.Random;

public class ThongTinDiemMonMoiAdapter extends ArrayAdapter<ThongTinDiemMonMoiModel> {
    private Context context;
    private ArrayList<ThongTinDiemMonMoiModel> thongTinDiemMonMoiModels;
    private int layoutResource;

    public ThongTinDiemMonMoiAdapter(@NonNull Context context, int resource, @NonNull ArrayList<ThongTinDiemMonMoiModel> thongTinDiemMonMoiModels) {
        super(context, resource, thongTinDiemMonMoiModels);
        this.context = context;
        this.thongTinDiemMonMoiModels = thongTinDiemMonMoiModels;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtTenHP = row.findViewById(R.id.txtTenHP);
        TextView txtDiemQT = row.findViewById(R.id.txtDiemQuaTrinh);
        TextView txtDiemThi = row.findViewById(R.id.txtDiemThi);
        TextView txtDiemChu = row.findViewById(R.id.txtDiemChu);
        RelativeLayout loDivider = row.findViewById(R.id.loDivider);

        txtTenHP.setText(thongTinDiemMonMoiModels.get(position).getTenLop());
        txtDiemQT.setText("Điểm QT: " + thongTinDiemMonMoiModels.get(position).getDiemQuaTrinh());
        txtDiemThi.setText("Điểm Thi: " + thongTinDiemMonMoiModels.get(position).getDiemThi());
        String trongSoQT = thongTinDiemMonMoiModels.get(position).getTrongSoQuaTrinh();
        double diemTongKet;
        if (trongSoQT != null && !trongSoQT.equals(" ") &&
                thongTinDiemMonMoiModels.get(position).getDiemQuaTrinh() != null &&
                !thongTinDiemMonMoiModels.get(position).getDiemQuaTrinh().equals(" ") &&
                thongTinDiemMonMoiModels.get(position).getDiemThi() != null &&
                !thongTinDiemMonMoiModels.get(position).getDiemThi().equals(" ")) {
            diemTongKet = (Double.parseDouble(thongTinDiemMonMoiModels.get(position).getDiemQuaTrinh()) * Double.parseDouble(trongSoQT)) +
                    Double.parseDouble(thongTinDiemMonMoiModels.get(position).getDiemThi()) * (1 - Double.parseDouble(trongSoQT));
        } else if (trongSoQT != null && trongSoQT.equals("0") &&
                thongTinDiemMonMoiModels.get(position).getDiemThi() != null &&
                !thongTinDiemMonMoiModels.get(position).getDiemThi().equals(" ")) {
            diemTongKet = Double.parseDouble(thongTinDiemMonMoiModels.get(position).getDiemThi());
        } else {
            diemTongKet = -1;
        }
        //Lam tron diem tong ket
        diemTongKet = (double) Math.round(diemTongKet * 10) / 10;

        if (diemTongKet >= 9.5) {
            txtDiemChu.setText("A+");
        } else if (diemTongKet >= 8.5) {
            txtDiemChu.setText("A");
        } else if (diemTongKet >= 8) {
            txtDiemChu.setText("B+");
        } else if (diemTongKet >= 7) {
            txtDiemChu.setText("B");
        } else if (diemTongKet >= 6.5) {
            txtDiemChu.setText("C+");
        } else if (diemTongKet >= 5.5) {
            txtDiemChu.setText("C");
        } else if (diemTongKet >= 5) {
            txtDiemChu.setText("D+");
        } else if (diemTongKet >= 4) {
            txtDiemChu.setText("D");
        } else if (diemTongKet == -1) {
            txtDiemChu.setText("");
        } else {
            txtDiemChu.setText("F");
        }

        Random random = new Random();
        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        loDivider.setBackgroundColor(color);
        return row;
    }
}
