package com.leenguyen.huststudent.menu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.DanhSachKhoaHocAdapter;
import com.leenguyen.huststudent.databinding.ActivityDanhSachKhoaHocBinding;
import com.leenguyen.huststudent.model.HustRankingModel;
import com.leenguyen.huststudent.model.QuangCaoModel;
import com.leenguyen.huststudent.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class DanhSachKhoaHocActivity extends AppCompatActivity {
    private ActivityDanhSachKhoaHocBinding binding;
    private DatabaseReference courseDatabase;
    private ArrayList<String> arrayList;
    private DanhSachKhoaHocAdapter danhSachKhoaHocAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_danh_sach_khoa_hoc);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        courseDatabase = FirebaseDatabase.getInstance("https://hust-student-course.firebaseio.com/").getReference();
        initLayout();
    }

    private void initLayout() {
        binding.btnBack.setOnClickListener(v -> onBackPressed());
        if (Utils.getInstance().isOnline(this)) {
            showDanhSachKhoaHoc();
        } else {
            Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), getString(R.string.can_co_ket_noi_mang));
        }
        binding.edtSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (binding.edtSearch.getText() != null) {
                    searchKhoaHoc(binding.edtSearch.getText().toString());
                }
                hideKeyboard(DanhSachKhoaHocActivity.this);
                return true;
            }
            return false;
        });
    }

    private void searchKhoaHoc(String searchString) {
        try {
            ArrayList<Integer> ketQuaTimKiemIndex = new ArrayList<>();
            if (arrayList != null && arrayList.size() > 0) {
                for (int i = 0; i < arrayList.size(); i++) {
                    String tenKhoaHoc = arrayList.get(i);
                    if (tenKhoaHoc.toLowerCase().contains(searchString.toLowerCase())) {
                        ketQuaTimKiemIndex.add(i);
                    }
                }
                if (ketQuaTimKiemIndex != null && ketQuaTimKiemIndex.size() > 0) {
                    danhSachKhoaHocAdapter.setHighLightPosition(ketQuaTimKiemIndex);
                    danhSachKhoaHocAdapter.notifyDataSetChanged();
                    binding.lvDanhSachKhoaHoc.smoothScrollToPosition(ketQuaTimKiemIndex.get(0));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void showDanhSachKhoaHoc() {
        courseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                arrayList = new ArrayList<>();
                for (DataSnapshot keyNote : snapshot.getChildren()) {
                    arrayList.add(keyNote.getValue().toString());
                }
                danhSachKhoaHocAdapter = new DanhSachKhoaHocAdapter(getApplicationContext(), R.layout.item_danh_sach_khoa_hoc, arrayList);
                binding.lvDanhSachKhoaHoc.setAdapter(danhSachKhoaHocAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}