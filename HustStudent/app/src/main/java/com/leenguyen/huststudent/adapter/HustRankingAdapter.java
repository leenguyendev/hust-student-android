package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.HustRankingModel;

import java.util.ArrayList;

public class HustRankingAdapter extends ArrayAdapter<HustRankingModel> {
    private Context context;
    private ArrayList<HustRankingModel> hustRankingModels;
    private int layoutResource;
    private int highLightPosition = -1;

    public HustRankingAdapter(@NonNull Context context, int resource, @NonNull ArrayList<HustRankingModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.hustRankingModels = objects;
        this.layoutResource = resource;
    }

    public void setHighLightPosition(int position) {
        highLightPosition = position;
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(layoutResource, null);
        TextView txtName = row.findViewById(R.id.txtName);
        TextView txtMSSV = row.findViewById(R.id.txtMssv);
        TextView txtCPA = row.findViewById(R.id.txtCPA);
        RelativeLayout imgRank = row.findViewById(R.id.imgRank);
        RelativeLayout imgStar = row.findViewById(R.id.imgStar);
        TextView txtRank = row.findViewById(R.id.txtRank);
        LinearLayout loItemRanking = row.findViewById(R.id.loItemRanking);

        if (position == 0) {
            imgRank.setBackgroundResource(R.drawable.rank_1);
            txtRank.setVisibility(View.GONE);
        } else if (position == 1) {
            imgRank.setBackgroundResource(R.drawable.rank_2);
            txtRank.setVisibility(View.GONE);
        } else if (position == 2) {
            imgRank.setBackgroundResource(R.drawable.rank_3);
            txtRank.setVisibility(View.GONE);
        } else {
            txtRank.setText(String.valueOf(position + 1));
        }
        String cpaString = hustRankingModels.get(position).getCpa();
        if (cpaString == null || cpaString.equals(" ")) { //fix crash if cpa = " "
            row = inflater.inflate(R.layout.item_null, null);
            return row;
        }
        if (cpaString.length() <= 5) { //check to fix crash issue when data type is not number
            double cpa = Double.parseDouble(hustRankingModels.get(position).getCpa());
            if (cpa >= 3.6) {
                imgStar.setBackgroundResource(R.drawable.ic_rating_5);
            } else if (cpa >= 3.2) {
                imgStar.setBackgroundResource(R.drawable.ic_rating_4);
            } else if (cpa >= 2.5) {
                imgStar.setBackgroundResource(R.drawable.ic_rating_3);
            } else if (cpa >= 2) {
                imgStar.setBackgroundResource(R.drawable.ic_rating_2);
            } else if (cpa == 0) {
                imgStar.setBackgroundResource(R.drawable.ic_rating_0);
            } else {
                imgStar.setBackgroundResource(R.drawable.ic_rating_1);
            }
        }
        txtName.setText(hustRankingModels.get(position).getName().trim());
        txtMSSV.setText(hustRankingModels.get(position).getMssv().trim());
        txtCPA.setText(hustRankingModels.get(position).getCpa().trim());

        if (highLightPosition != -1 && position == highLightPosition) {
            loItemRanking.setBackgroundColor(Color.GRAY);
            txtName.setTextColor(Color.WHITE);
            txtMSSV.setTextColor(Color.WHITE);
        }
        return row;
    }
}
