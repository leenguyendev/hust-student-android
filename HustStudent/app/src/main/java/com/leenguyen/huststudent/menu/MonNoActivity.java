package com.leenguyen.huststudent.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.BangDiemAdapter;
import com.leenguyen.huststudent.databinding.ActivityMonNoBinding;
import com.leenguyen.huststudent.model.HocPhanModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MonNoActivity extends AppCompatActivity {
    private ActivityMonNoBinding binding;
    private BangDiemAdapter adapter;
    private ArrayList<HocPhanModel> hocPhanModels;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_mon_no);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        boolean isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVipMember && !isNoAds) {
            initAdsBanner();
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            binding.loAds.setLayoutParams(layoutParams);
        }
        initLayout();
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    private void initLayout() {
        showDanhSachMonNo();
        binding.btnBack.setOnClickListener(view -> onBackPressed());

        binding.lvMonNo.setOnItemClickListener((adapterView, view, i, l) -> showDialogDetail(hocPhanModels.get(i)));
    }

    @SuppressLint("SetTextI18n")
    private void showDialogDetail(HocPhanModel hocPhanModel) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_mon_no);
        ImageView btnClose = dialog.findViewById(R.id.btnClose);
        TextView txtTenHP = dialog.findViewById(R.id.txtTenHP);
        TextView txtMaHP = dialog.findViewById(R.id.txtMaHP);
        TextView txtHocKy = dialog.findViewById(R.id.txtHocKy);
        TextView txtSoTC = dialog.findViewById(R.id.txtSoTC);
        TextView txtLopHoc = dialog.findViewById(R.id.txtLopHoc);
        TextView txtDiemQT = dialog.findViewById(R.id.txtDiemQT);
        TextView txtDiemThi = dialog.findViewById(R.id.txtDiemThi);
        TextView txtDiemTongKet = dialog.findViewById(R.id.txtDiemTongKet);

        btnClose.setOnClickListener(view -> dialog.dismiss());

        String hocKy = "", lopHoc = "", diemQuaTrinh = "", diemThi = "";
        txtTenHP.setText(hocPhanModel.getTenHP());
        txtSoTC.setText(getString(R.string.so_tin_chi) + " " + hocPhanModel.getSoTinChi());
        txtDiemTongKet.setText(getString(R.string.diem_tong_ket) + " " + hocPhanModel.getDiemTongKet());
        txtMaHP.setText(getString(R.string.ma_hoc_phan) + " " + hocPhanModel.getMaHocPhan());
        if (getApplicationContext() != null && !hocPhanModel.isHPNew() && !hocPhanModel.isHPCaiThien()) {
            String bangDiemCaNhan = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_CA_NHAN);
            try {
                JSONArray bangDiemCaNhanJsonA = new JSONArray(bangDiemCaNhan);
                if (bangDiemCaNhanJsonA.length() > 0) {
                    JSONObject hocPhanJsonO;
                    for (int i = bangDiemCaNhanJsonA.length() - 1; i >= 0; i--) {
                        hocPhanJsonO = bangDiemCaNhanJsonA.getJSONObject(i);
                        String maHP = hocPhanJsonO.getString(JsonUtils.KEY_MA_HP);
                        String diemTongKet = hocPhanJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        if (maHP.equals(hocPhanModel.getMaHocPhan()) && diemTongKet.equals(hocPhanModel.getDiemTongKet())) {
                            lopHoc = hocPhanJsonO.getString(JsonUtils.KEY_LOP_HOC);
                            diemQuaTrinh = hocPhanJsonO.getString(JsonUtils.KEY_DIEM_QUA_TRINH);
                            diemThi = hocPhanJsonO.getString(JsonUtils.KEY_DIEM_THI);
                            hocKy = hocPhanJsonO.getString(JsonUtils.KEY_HOC_KY);
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        txtHocKy.setText(getString(R.string.hoc_ky) + " " + hocKy);
        txtLopHoc.setText(getString(R.string.lop_hoc) + " " + lopHoc);
        txtDiemQT.setText(getString(R.string.diem_qua_trinh) + " " + diemQuaTrinh);
        txtDiemThi.setText(getString(R.string.diem_thi) + " " + diemThi);

        dialog.show();
    }

    private void showDanhSachMonNo() {
        int tongSoTinChi = 0;
        hocPhanModels = new ArrayList<>();
        String dataBangDiemHP = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN);
        try {
            JSONArray bangDiemHPJsonA = new JSONArray(dataBangDiemHP);
            if (bangDiemHPJsonA.length() > 0) {
                for (int i = 0; i < bangDiemHPJsonA.length(); i++) {
                    JSONObject diemHPJsonO = bangDiemHPJsonA.getJSONObject(i);
                    String diem = diemHPJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                    if (diem.equals("F")) {
                        String maHP = diemHPJsonO.getString(JsonUtils.KEY_MA_HP);
                        String tenHP = diemHPJsonO.getString(JsonUtils.KEY_TEN_HP);
                        String soTC = diemHPJsonO.getString(JsonUtils.KEY_TIN_CHI_HP);
                        String hocKy = diemHPJsonO.getString(JsonUtils.KEY_HOC_KY);
                        HocPhanModel hocPhanModel = new HocPhanModel(maHP, tenHP, hocKy, null, null, null, soTC, diem, null, false, false);
                        hocPhanModels.add(hocPhanModel);
                        tongSoTinChi += Integer.parseInt(soTC);
                    }
                }
            }

            if (hocPhanModels.size() > 0) {
                binding.loKhongCoDuLieu.setVisibility(View.GONE);
                adapter = new BangDiemAdapter(this, R.layout.item_bang_diem_hoc_phan, hocPhanModels);
                binding.lvMonNo.setAdapter(adapter);
                binding.loHeader.setVisibility(View.VISIBLE);
                Glide.with(this).load(R.drawable.ic_ami_tin_tuong).into(binding.imgStickerHeader);
                binding.txtSoTinChi.setText(tongSoTinChi + " tín chỉ");
            } else {
                binding.loHeader.setVisibility(View.GONE);
                binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
                Glide.with(this).load(R.drawable.ic_ami_tha_tim).into(binding.imgSticker);
            }

        } catch (Exception e) {
            Log.e("RAKAN", "showDanhSachMonNo: " + e.toString());
        }
    }
}
