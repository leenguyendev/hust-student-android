package com.leenguyen.huststudent;

import java.sql.Array;

public class Constant {
    public static final String URL_LOGIN = "https://ctt-sis.hust.edu.vn/Account/Login.aspx";
    public static final String URL_GOOGLE_SHEET_SCRIP_LICH_THI = "https://script.google.com/macros/s/AKfycbzoUOc5KyuZnhiOI0mZjEIjFl2t0yfQxuLQiMIkcla4qZOoaRPQ/exec";
    public static final String URL_CTT_DT = "https://ctt-daotao.hust.edu.vn/";
    public static final String URL_CTSV = "http://ctsv.hust.edu.vn/#/";
    public static final String URL_HUST = "https://www.hust.edu.vn/";

    public static final String ID_CAPTCHA = "ctl00_ctl00_contentPane_MainPanel_MainContent_ASPxCaptcha1_IMG";
    public static final int STATUS_CODE_OK = 200;
    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";

    public static final String KEY_USERNAME = "user_name";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_CAPTCHA = "captcha";
    public static final String KEY_MA_HOA_MD5 = "ma_hoa_md5";
    public static final String KEY_MA_DANG_NHAP = "ma_dang_nhap";
    public static final String KEY_LOGIN_FORM = "login_form";


    //SHARE REFERENCES----------------------------------------------------------------------------------
    public static final String SHARE_PREFERENCES_DATA = "share_preferences_data";
    //Use app
    public static final String KEY_SHARE_PREFERENCES_IS_FIRST_TIME = "key_share_preferences_is_first_time";
    public static final String KEY_SHARE_PREFERENCES_IS_FIRST_TIME_OPEN_BANG_DIEM = "key_share_preferences_is_first_time_open_bang_diem";
    public static final String VALUE_SHARE_PREFERENCES_IS_FIRST_TIME_TRUE = "1";
    public static final String VALUE_SHARE_PREFERENCES_IS_FIRST_TIME_FALSE = "0";
    public static final String KEY_SHARE_PREFERENCES_LAST_TIME_UPDATE_DATA = "key_share_preferences_last_time_update_data";
    public static final String KEY_SHARE_PREFERENCES_HO_SO_TIM_NGUOI_YEU = "key_share_preferences_ho_so_tim_nguoi_yeu";
    public static final String KEY_SHARE_PREFERENCES_LICH_THI_FULL = "key_share_preferences_lich_thi_full";
    public static final String KEY_SHARE_PREFERENCES_LICH_THI_TU_THEM = "key_share_preferences_lich_thi_tu_them";
    public static final String KEY_SHARE_PREFERENCES_LICH_THI_SAVED = "key_share_preferences_lich_thi_save";
    public static final String KEY_SHARE_PREFERENCES_IS_VIP_MEMBER = "key_share_preferences_is_vip_member";
    public static final String KEY_SHARE_PREFERENCES_IS_NO_ADS = "key_share_preferences_is_no_ads";
    public static final String KEY_SHARE_PREFERENCES_ADS_OPEN_ID = "key_share_preferences_ads_open_id";
    public static final String KEY_SHARE_PREFERENCES_ADS_NATIVE_ID = "key_share_preferences_ads_native_id";
    public static final String KEY_SHARE_PREFERENCES_ADS_BANNER_ID = "key_share_preferences_ads_banner_id";
    public static final String KEY_SHARE_PREFERENCES_MAX_ADS = "key_share_preferences_max_ads";
    public static final String KEY_SHARE_PREFERENCES_ADS_COUNT = "key_share_preferences_ads_count";
    public static final String KEY_SHARE_PREFERENCES_PRE_DATE = "key_share_preferences_fre_date";
    //User
    public static final String SHARE_PREFERENCES_DATA_USER = "key_share_preferences_data_user";
    public static final String SHARE_PREFERENCES_DATA_ALREADY_USER_LOGIN = "key_share_preferences_data_already_user_login";
    //Image bitmap avatar
    public static final String KEY_SHARE_PREFERENCES_DATA_AVATAR = "key_share_preferences_data_avatar";
    public static final String KEY_SHARE_PREFERENCES_DATA_AVATAR_ROTATE_ANGLE = "key_share_preferences_data_avatar_rotate_angle";
    //Data hoc tap
    public static final String KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN = "key_share_preferences_data_bang_diem_hoc_phan";
    public static final String KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_CA_NHAN = "key_share_preferences_data_bang_diem_ca_nhan";
    public static final String KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_TONG_KET = "key_share_preferences_data_bang_diem_tong_ket";
    public static final String KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN = "key_share_preferences_data_thong_tin_sinh_vien";
    public static final String KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN = "key_share_preferences_data_hoc_phan_cai_thien";
    public static final String KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI = "key_share_preferences_data_hoc_phan_moi";
    public static final String KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_KHONG_TINH_DIEM = "key_share_preferences_data_hoc_phan_khong_tinh_diem";
    public static final String KEY_SHARE_PREFERENCES_DATA_DANH_SACH_LOP_SV = "key_share_preferences_data_danh_sach_lop_sv";
    public static final String KEY_SHARE_PREFERENCES_DATA_DIEM_THI_TOEIC = "key_share_preferences_data_diem_thi_toeic";
    public static final String KEY_SHARE_PREFERENCES_DATA_TKB = "key_share_preferences_data_tkb";
    public static final String KEY_SHARE_PREFERENCES_DATA_KIEM_TRA_NHAP_DIEM_KI_MOI = "key_share_preferences_data_kiem_tra_nhap_diem_ki_moi";
    public static final String KEY_SHARE_PREFERENCES_DATA_THONG_TIN_HOC_PHI = "key_share_preferences_data_thong_tin_hoc_phi";
    public static final String KEY_SHARE_PREFERENCES_DATA_TKB_TAM_THOI = "key_share_preferences_data_thoi_khoa_bieu_tam_thoi";

    //Tuan hoc hien tai
    public static final String KEY_SHARE_PREFERENCES_DATA_WEEK_OF_YEAR = "key_share_preferenceds_data_week_of_year";
    public static final String KEY_SHARE_PREFERENCES_DATA_TUAN_HOC_HIEN_TAI = "key_share_preferenceds_data_tuan_hoc_hien_tai";

    //Data login is changed
    public static final String SHARE_PREFERENCES_FORM_DATA_LOGIN = "share_preferences_form_data_login";
    public static final String KEY_SHARE_PREFERENCES_FORM_DATA_LOGIN_3 = "key_share_preferences_form_data_login_3";
    public static final String KEY_SHARE_PREFERENCES_FORM_DATA_LOGIN_5 = "key_share_preferences_form_data_login_5";

    //Data Login form
    public static final String KEY_FORM_DATA_LOGIN_1 = "__EVENTTARGET";
    public static final String VALUE_FORM_DATA_LOGIN_1 = "";

    public static final String KEY_FORM_DATA_LOGIN_2 = "__EVENTARGUMENT";
    public static final String VALUE_FORM_DATA_LOGIN_2 = "";
    //Change
    public static final String KEY_FORM_DATA_LOGIN_3 = "__VIEWSTATE";

    public static final String KEY_FORM_DATA_LOGIN_4 = "__VIEWSTATEGENERATOR";
    public static final String VALUE_FORM_DATA_LOGIN_4 = "CD85D8D2";
    //Change
    public static final String KEY_FORM_DATA_LOGIN_5 = "__EVENTVALIDATION";

    public static final String KEY_FORM_DATA_LOGIN_6 = "ctl00$ctl00$TopMenuPane$menuTop";
    public static final String VALUE_FORM_DATA_LOGIN_6 = "{&quot;selectedItemIndexPath&quot;:&quot;&quot;,&quot;checkedState&quot;:&quot;&quot;}";

    public static final String KEY_FORM_DATA_LOGIN_7 = "ctl00$ctl00$TopMenuPane$ctl10$menuTop";
    public static final String KEY_FORM_DATA_LOGIN_7_2 = "ctl00$ctl00$TopMenuPane$ctl09$menuTop";
    public static final String VALUE_FORM_DATA_LOGIN_7 = "{&quot;selectedItemIndexPath&quot;:&quot;&quot;,&quot;checkedState&quot;:&quot;&quot;}";

    public static final String KEY_FORM_DATA_LOGIN_8 = "ctl00$ctl00$contentPane$MainPanel$MainContent$chbParents";
    public static final String VALUE_FORM_DATA_LOGIN_8 = "I";
    //Change
    public static final String KEY_FORM_DATA_LOGIN_9 = "ctl00$ctl00$contentPane$MainPanel$MainContent$tbUserName$State";
    public static final String VALUE_FORM_DATA_LOGIN_9_HEAD = "{&quot;rawValue&quot;:&quot;";
    public static final String VALUE_FORM_DATA_LOGIN_9_END = "&quot;,&quot;validationState&quot;:&quot;&quot;}";
    //Change
    public static final String KEY_FORM_DATA_LOGIN_10 = "ctl00$ctl00$contentPane$MainPanel$MainContent$tbUserName";
    //Change
    public static final String KEY_FORM_DATA_LOGIN_11 = "ctl00$ctl00$contentPane$MainPanel$MainContent$tbPassword$State";
    public static final String VALUE_FORM_DATA_LOGIN_11_HEAD = "{&quot;rawValue&quot;:&quot;";
    public static final String VALUE_FORM_DATA_LOGIN_11_END = "&quot;,&quot;validationState&quot;:&quot;&quot;}";
    //Change
    public static final String KEY_FORM_DATA_LOGIN_12 = "ctl00$ctl00$contentPane$MainPanel$MainContent$tbPassword";

    public static final String KEY_FORM_DATA_LOGIN_13 = "ctl00$ctl00$contentPane$MainPanel$MainContent$ASPxCaptcha1$TB$State";
    public static final String VALUE_FORM_DATA_LOGIN_13 = "{&quot;validationState&quot;:&quot;&quot;}";
    //Change
    public static final String KEY_FORM_DATA_LOGIN_14 = "ctl00$ctl00$contentPane$MainPanel$MainContent$ASPxCaptcha1$TB";

    public static final String KEY_FORM_DATA_LOGIN_15 = "ctl00$ctl00$contentPane$MainPanel$MainContent$btLogin";
    public static final String VALUE_FORM_DATA_LOGIN_15 = "[\"0\":\"Đăng+nhập\",\"1\":\"\"]";
    //Change
    public static final String KEY_FORM_DATA_LOGIN_16 = "ctl00$ctl00$contentPane$MainPanel$MainContent$hfInput";

    public static final String KEY_FORM_DATA_LOGIN_17 = "DXScript";
    public static final String VALUE_FORM_DATA_LOGIN_17 = "1_10,1_11,1_22,1_63,1_13,1_14,1_29,1_48,1_16,1_23,1_33,1_180,1_185,1_186,1_181,1_200,1_179,1_32";
    public static final String VALUE_FORM_DATA_LOGIN_17_2 = "1_11,1_12,1_23,1_63,1_14,1_15,1_48,1_17,1_24,1_33,1_180,1_185,1_186,1_181,1_200,1_179,1_32";

    public static final String KEY_FORM_DATA_LOGIN_18 = "DXCss";
    public static final String VALUE_FORM_DATA_LOGIN_18 = "0_2870,1_67,1_68,1_69,0_2875,0_2,0_2786,1_206,0_2791,1_207,1_204,1_203,https://fonts.googleapis.com/css?family=Arimo,../Content/bootstrap/css/bootstrap.min.css,../Content/bootstrap/css/bootstrap-combobox.css,../Content/docs.min.css,../Content/Site.css";
    public static final String VALUE_FORM_DATA_LOGIN_18_2 = "0_2944,1_67,1_68,1_69,0_2949,0_2,0_2860,1_206,0_2865,1_207,1_204,1_203,https://fonts.googleapis.com/css?family=Arimo,../Content/bootstrap/css/bootstrap.min.css,../Content/bootstrap/css/bootstrap-combobox.css,../Content/docs.min.css,../Content/Site.css";
}
