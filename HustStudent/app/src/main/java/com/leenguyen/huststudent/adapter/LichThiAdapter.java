package com.leenguyen.huststudent.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.engine.Resource;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.LichThiModel;

import java.util.ArrayList;
import java.util.Random;

public class LichThiAdapter extends ArrayAdapter<LichThiModel> {
    private Context context;
    private ArrayList<LichThiModel> lichThiModelList;
    private int layoutResource;
    private int highLightPosition = -1;

    public LichThiAdapter(@NonNull Context context, int resource, @NonNull ArrayList<LichThiModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.lichThiModelList = objects;
        this.layoutResource = resource;
    }

    public void setHighLightPosition(int position) {
        highLightPosition = position;
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(layoutResource, null);
        TextView txtTenHocPhan = row.findViewById(R.id.txtTenHP);
        TextView txtThoiGian = row.findViewById(R.id.txtThoiGian);
        TextView txtMaLop = row.findViewById(R.id.txtMaLop);
        TextView txtNhom = row.findViewById(R.id.txtNhom);
        TextView txtKipThi = row.findViewById(R.id.txtKipThi);
        TextView txtPhongThi = row.findViewById(R.id.txtPhongThi);
        RelativeLayout loDivider = row.findViewById(R.id.loDivider);
        LinearLayout loItemLichThi = row.findViewById(R.id.loItemLichThi);

        Random random = new Random();
        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        loDivider.setBackgroundColor(color);
        txtTenHocPhan.setText(lichThiModelList.get(position).getTenHocPhan());
        txtThoiGian.setText(lichThiModelList.get(position).getThu() + " " + lichThiModelList.get(position).getNgayThi());
        txtMaLop.setText("Mã lớp: " + lichThiModelList.get(position).getMaLop());
        txtNhom.setText("Nhóm: " + lichThiModelList.get(position).getNhom());
        String gioThi = "";
        switch (lichThiModelList.get(position).getKipThi()) {
            case "Kíp 1":
                gioThi = "7h00";
                break;
            case "Kíp 2":
                gioThi = "9h30";
                break;
            case "Kíp 3":
                gioThi = "12h30";
                break;
            case "Kíp 4":
                gioThi = "15h00";
                break;
        }
        if (gioThi.equals("")) {
            txtKipThi.setText(lichThiModelList.get(position).getKipThi());
        } else {
            txtKipThi.setText(lichThiModelList.get(position).getKipThi() + " (" + gioThi + ")");
        }
        txtPhongThi.setText(lichThiModelList.get(position).getPhongThi());
        if (highLightPosition != -1 && position == highLightPosition) {
            loItemLichThi.setBackgroundColor(Color.RED);
        }
        return row;
    }
}
