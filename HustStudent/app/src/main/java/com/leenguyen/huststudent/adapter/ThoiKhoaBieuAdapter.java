package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.SinhVienInfoModel;
import com.leenguyen.huststudent.model.ThoiKhoaBieuModel;
import com.leenguyen.huststudent.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ThoiKhoaBieuAdapter extends ArrayAdapter<ThoiKhoaBieuModel> {
    private Context context;
    private ArrayList<ThoiKhoaBieuModel> thoiKhoaBieuModels;
    private int layoutResource;

    public ThoiKhoaBieuAdapter(@NonNull Context context, int resource, @NonNull ArrayList<ThoiKhoaBieuModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.thoiKhoaBieuModels = objects;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtThu = row.findViewById(R.id.txtThu);
        TextView txtStartTime = row.findViewById(R.id.txtStartTime);
        TextView txtEndTime = row.findViewById(R.id.txtEndTime);
        TextView txtTenHP = row.findViewById(R.id.txtTenHP);
        TextView txtTuanHoc = row.findViewById(R.id.txtTuanHoc);
        TextView txtPhongHoc = row.findViewById(R.id.txtPhongHoc);
        RelativeLayout loDivider = row.findViewById(R.id.loDivider);
        RelativeLayout loHeader = row.findViewById(R.id.loHeader);
        ImageView imgReminderMonHoc = row.findViewById(R.id.imgReminderMonHoc);
        ImageView imgReminderMonThiNghiem = row.findViewById(R.id.imgReminderMonThiNghiem);
        TextView txtLoaiLop = row.findViewById(R.id.txtLoaiLop);
        LinearLayout loItemTKB = row.findViewById(R.id.loItemTKB);

        Random random = new Random();
        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        loDivider.setBackgroundColor(color);
        loHeader.setBackgroundColor(color);
        txtTenHP.setText(thoiKhoaBieuModels.get(position).getTenLop());
        txtPhongHoc.setText(thoiKhoaBieuModels.get(position).getPhongHoc());
        String thoiGian = thoiKhoaBieuModels.get(position).getThoiGian();
        txtTuanHoc.setText(thoiKhoaBieuModels.get(position).getTuanHoc());
        txtLoaiLop.setText("(" + thoiKhoaBieuModels.get(position).getLoaiLop() + ")");

        if (thoiGian.equals("") || thoiGian.length() == 1 || !thoiGian.contains(",") || !thoiGian.contains("-")) {
            txtStartTime.setText("");
            txtEndTime.setText("");
            loHeader.setVisibility(View.GONE);
        } else {
            int viTriDauPhay = thoiGian.indexOf(",");
            int viTriDauGach = thoiGian.indexOf("-");
            txtStartTime.setText(thoiGian.substring(viTriDauPhay + 1, viTriDauGach - 1));
            txtEndTime.setText(thoiGian.substring(viTriDauGach + 2));

            String thuCurrentPos = thoiKhoaBieuModels.get(position).getThoiGian().substring(0, viTriDauPhay);
            String thuPreviousPos;
            if (position >= 1 && thoiKhoaBieuModels.get(position - 1).getThoiGian().length() > 1) {
                thuPreviousPos = thoiKhoaBieuModels.get(position - 1).getThoiGian().substring(0, viTriDauPhay);
            } else {
                thuPreviousPos = "";
            }
            if (thuCurrentPos.equals(thuPreviousPos)) {
                loHeader.setVisibility(View.GONE);
            } else {
                txtThu.setText(thuCurrentPos);
            }
        }

        //Đánh dấu môn học tuan hien tai
        String tuanHienTai = Utils.getInstance().getTuanHocHienTai(context);
        String tuanHoc = thoiKhoaBieuModels.get(position).getTuanHoc();
        ArrayList<String> listTuanHoc = new ArrayList<>();
        //Case 1,2,3,12 or 1-2,3-6 or 1,2,4-9
        StringBuilder builder = new StringBuilder();
        String startTime = null;
        String endTime = null;
        boolean isRangeWeek = false;
        for (int i = 0; i < tuanHoc.length(); i++) {
            if ((String.valueOf(tuanHoc.charAt(i)).equals(",") || String.valueOf(tuanHoc.charAt(i)).equals(".")) && !isRangeWeek) {
                listTuanHoc.add(String.valueOf(builder).trim());
                builder = new StringBuilder();
            } else if (String.valueOf(tuanHoc.charAt(i)).equals("-")) {
                startTime = builder.toString().trim();
                isRangeWeek = true;
                builder = new StringBuilder();
            } else if ((String.valueOf(tuanHoc.charAt(i)).equals(",") || String.valueOf(tuanHoc.charAt(i)).equals(".")) && isRangeWeek) {
                endTime = builder.toString().trim();
                for (int j = Integer.parseInt(startTime); j <= Integer.parseInt(endTime); j++) {
                    listTuanHoc.add(String.valueOf(j));
                }
                startTime = null;
                endTime = null;
                isRangeWeek = false;
                builder = new StringBuilder();
            } else if (i == tuanHoc.length() - 1) {
                builder.append(tuanHoc.charAt(i));
                if (!isRangeWeek) {
                    listTuanHoc.add(String.valueOf(builder));
                } else {
                    endTime = builder.toString().trim();
                    for (int j = Integer.parseInt(startTime); j <= Integer.parseInt(endTime); j++) {
                        listTuanHoc.add(String.valueOf(j));
                    }
                }
            } else {
                builder.append(tuanHoc.charAt(i));
            }
        }

        if (listTuanHoc.size() > 0) {
            for (int i = 0; i < listTuanHoc.size(); i++) {
                if (tuanHienTai.equals(listTuanHoc.get(i))) {
                    loItemTKB.setVisibility(View.VISIBLE);

                    if (thoiKhoaBieuModels.get(position).getLoaiLop().equals("TN")) {
                        imgReminderMonThiNghiem.setVisibility(View.VISIBLE);
                        imgReminderMonHoc.setVisibility(View.GONE);
                    } else {
                        imgReminderMonThiNghiem.setVisibility(View.GONE);
                        imgReminderMonHoc.setVisibility(View.VISIBLE);
                    }
                    break;
                } else {
                    if (tuanHienTai.equals("0")) { //Chưa cài dặt tuần học
                        loItemTKB.setVisibility(View.VISIBLE);
                        break;
                    }

                    loItemTKB.setVisibility(View.VISIBLE);
                }
            }
        }

        if (loItemTKB.getVisibility() == View.GONE) {
            loHeader.setOnClickListener(v -> {
                //do nothing
            });
        }
        return row;
    }
}
