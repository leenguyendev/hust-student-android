package com.leenguyen.huststudent.model;

public class HocPhiModel {
    private String maHP, tenHP, soTienMotTCHocPhi, soTinChiHocPhi, heSoHocPhi, tongSoTienHocPhan, trangThaiDangKy, loaiDangKy, ghiChu;

    public HocPhiModel(String maHP, String tenHP, String soTienMotHocPhi, String soTinChiHocPhi, String heSoHocPhi, String tongSoTienHocPhan, String trangThaiDangKy, String loaiDangKy, String ghiChu) {
        this.maHP = maHP;
        this.tenHP = tenHP;
        this.soTienMotTCHocPhi = soTienMotHocPhi;
        this.soTinChiHocPhi = soTinChiHocPhi;
        this.heSoHocPhi = heSoHocPhi;
        this.tongSoTienHocPhan = tongSoTienHocPhan;
        this.trangThaiDangKy = trangThaiDangKy;
        this.loaiDangKy = loaiDangKy;
        this.ghiChu = ghiChu;
    }

    public String getHeSoHocPhi() {
        return heSoHocPhi;
    }

    public void setHeSoHocPhi(String heSoHocPhi) {
        this.heSoHocPhi = heSoHocPhi;
    }

    public String getMaHP() {
        return maHP;
    }

    public void setMaHP(String maHP) {
        this.maHP = maHP;
    }

    public String getTenHP() {
        return tenHP;
    }

    public void setTenHP(String tenHP) {
        this.tenHP = tenHP;
    }

    public String getSoTienMotTCHocPhi() {
        return soTienMotTCHocPhi;
    }

    public void setSoTienMotTCHocPhi(String soTienMotHocPhi) {
        this.soTienMotTCHocPhi = soTienMotHocPhi;
    }

    public String getSoTinChiHocPhi() {
        return soTinChiHocPhi;
    }

    public void setSoTinChiHocPhi(String soTinChiHocPhi) {
        this.soTinChiHocPhi = soTinChiHocPhi;
    }

    public String getTongSoTienHocPhan() {
        return tongSoTienHocPhan;
    }

    public void setTongSoTienHocPhan(String tongSoTienHocPhan) {
        this.tongSoTienHocPhan = tongSoTienHocPhan;
    }

    public String getTrangThaiDangKy() {
        return trangThaiDangKy;
    }

    public void setTrangThaiDangKy(String trangThaiDangKy) {
        this.trangThaiDangKy = trangThaiDangKy;
    }

    public String getLoaiDangKy() {
        return loaiDangKy;
    }

    public void setLoaiDangKy(String loaiDangKy) {
        this.loaiDangKy = loaiDangKy;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
}
