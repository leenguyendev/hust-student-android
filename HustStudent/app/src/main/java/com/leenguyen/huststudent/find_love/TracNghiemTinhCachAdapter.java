package com.leenguyen.huststudent.find_love;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;

import java.util.ArrayList;
import java.util.function.ObjIntConsumer;

public class TracNghiemTinhCachAdapter extends ArrayAdapter<CauHoiTracNghiemTinhCachModel> {
    private ArrayList<CauHoiTracNghiemTinhCachModel> listCauHoiModels;
    private Context context;
    private int resource;

    public TracNghiemTinhCachAdapter(@NonNull Context context, int resource, @NonNull ArrayList<CauHoiTracNghiemTinhCachModel> listCauHoiModels) {
        super(context, resource, listCauHoiModels);
        this.context = context;
        this.listCauHoiModels = listCauHoiModels;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(resource, null);
        TextView txtCauHoi = row.findViewById(R.id.txtCauHoi);
        RadioGroup radioGroup = row.findViewById(R.id.rdbgCauTraLoi);
        RadioButton radioButton_1 = row.findViewById(R.id.rdbCauTraLoi_1);
        RadioButton radioButton_2 = row.findViewById(R.id.rdbCauTraLoi_2);

        txtCauHoi.setText(listCauHoiModels.get(position).getCauHoi());
        radioButton_1.setText(listCauHoiModels.get(position).getCauTraLoi_1());
        radioButton_2.setText(listCauHoiModels.get(position).getCauTraLoi_2());
        if (listCauHoiModels.get(position).isDapAn_1() && !listCauHoiModels.get(position).isDapAn_2()) {
            radioButton_1.setChecked(true);
        } else if (listCauHoiModels.get(position).isDapAn_2() && !listCauHoiModels.get(position).isDapAn_1()) {
            radioButton_2.setChecked(true);
        } else {
            radioButton_1.setChecked(false);
            radioButton_2.setChecked(false);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdbCauTraLoi_1:
                        listCauHoiModels.get(position).setDapAn_1(true);
                        listCauHoiModels.get(position).setDapAn_2(false);
                        break;
                    case R.id.rdbCauTraLoi_2:
                        listCauHoiModels.get(position).setDapAn_2(true);
                        listCauHoiModels.get(position).setDapAn_1(false);
                        break;
                }
            }
        });

        return row;
    }

    public ArrayList<CauHoiTracNghiemTinhCachModel> getListCauHoiTracNghiemModels() {
        return listCauHoiModels;
    }
}
