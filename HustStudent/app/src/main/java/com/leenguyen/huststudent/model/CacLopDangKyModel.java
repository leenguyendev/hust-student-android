package com.leenguyen.huststudent.model;

public class CacLopDangKyModel {
    private String maLop, maLopKem, tenLop, maHP, loaiLop, thongTinLop, yeuCau, trangThaiDK, loaiDK, tinCHi;

    public CacLopDangKyModel(String maLop, String maLopKem, String tenLop, String maHP, String loaiLop, String thongTinLop, String yeuCau, String trangThaiDK, String loaiDK, String tinCHi) {
        this.maLop = maLop;
        this.maLopKem = maLopKem;
        this.tenLop = tenLop;
        this.maHP = maHP;
        this.loaiLop = loaiLop;
        this.thongTinLop = thongTinLop;
        this.yeuCau = yeuCau;
        this.trangThaiDK = trangThaiDK;
        this.loaiDK = loaiDK;
        this.tinCHi = tinCHi;
    }

    public String getMaLop() {
        return maLop;
    }

    public void setMaLop(String maLop) {
        this.maLop = maLop;
    }

    public String getMaLopKem() {
        return maLopKem;
    }

    public void setMaLopKem(String maLopKem) {
        this.maLopKem = maLopKem;
    }

    public String getTenLop() {
        return tenLop;
    }

    public void setTenLop(String tenLop) {
        this.tenLop = tenLop;
    }

    public String getMaHP() {
        return maHP;
    }

    public void setMaHP(String maHP) {
        this.maHP = maHP;
    }

    public String getLoaiLop() {
        return loaiLop;
    }

    public void setLoaiLop(String loaiLop) {
        this.loaiLop = loaiLop;
    }

    public String getThongTinLop() {
        return thongTinLop;
    }

    public void setThongTinLop(String thongTinLop) {
        this.thongTinLop = thongTinLop;
    }

    public String getYeuCau() {
        return yeuCau;
    }

    public void setYeuCau(String yeuCau) {
        this.yeuCau = yeuCau;
    }

    public String getTrangThaiDK() {
        return trangThaiDK;
    }

    public void setTrangThaiDK(String trangThaiDK) {
        this.trangThaiDK = trangThaiDK;
    }

    public String getLoaiDK() {
        return loaiDK;
    }

    public void setLoaiDK(String loaiDK) {
        this.loaiDK = loaiDK;
    }

    public String getTinCHi() {
        return tinCHi;
    }

    public void setTinCHi(String tinCHi) {
        this.tinCHi = tinCHi;
    }
}
