package com.leenguyen.huststudent.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.ThongTinDiemMonMoiAdapter;
import com.leenguyen.huststudent.databinding.ActivityKiemTraNhapDiemKiMoiBinding;
import com.leenguyen.huststudent.databinding.DialogDetailDiemMonMoiBinding;
import com.leenguyen.huststudent.model.ThongTinDiemMonMoiModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class KiemTraNhapDiemKiMoiActivity extends AppCompatActivity {

    private ActivityKiemTraNhapDiemKiMoiBinding binding;
    private ThongTinDiemMonMoiAdapter adapter;
    private ArrayList<ThongTinDiemMonMoiModel> arrayList;
    private String[] listDiemChu = {"F", "D", "D+", "C", "C+", "B", "B+", "A", "A+"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_kiem_tra_nhap_diem_ki_moi);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        boolean isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVipMember && !isNoAds) {
            initAdsBanner();
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            binding.loAds.setLayoutParams(layoutParams);
        }
        initLayout();
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    private void initLayout() {
        showListDiemMonMoi();

        showGPA();

        binding.btnBack.setOnClickListener(v -> onBackPressed());

        binding.lvDiemMonMoi.setOnItemClickListener((parent, view, position, id) -> showDetailDialog(arrayList.get(position)));
    }

    private void showDetailDialog(ThongTinDiemMonMoiModel thongTinDiemMonMoiModel) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final DialogDetailDiemMonMoiBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_detail_diem_mon_moi, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        bindingDialog.txtTenHP.setSelected(true);
        bindingDialog.txtTenHP.setText(thongTinDiemMonMoiModel.getTenLop());
        bindingDialog.txtMaSV.setText("MSSV: " + thongTinDiemMonMoiModel.getMssv());
        bindingDialog.txtMaLop.setText("Mã lớp: " + thongTinDiemMonMoiModel.getMaLop());
        bindingDialog.txtTrongSoDiemQuaTrinh.setText("Trọng số QT: " + thongTinDiemMonMoiModel.getTrongSoQuaTrinh());
        bindingDialog.txtDiemQuaTrinh.setText("Điểm QT: " + thongTinDiemMonMoiModel.getDiemQuaTrinh());
        bindingDialog.txtTTDiemQuaTrinh.setText("TT điểm QT: " + thongTinDiemMonMoiModel.getThongTinDiemQuaTrinh());
        bindingDialog.txtDiemThi.setText("Điểm thi: " + thongTinDiemMonMoiModel.getDiemThi());
        bindingDialog.txtTTDiemThi.setText("TT điểm thi: " + thongTinDiemMonMoiModel.getThongTinDiemThi());

        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void showGPA() {
        String dataListDiemMonMoi = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_KIEM_TRA_NHAP_DIEM_KI_MOI);
        String dataListBangDiemCaNhan = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_CA_NHAN);

        if (dataListDiemMonMoi == null || dataListDiemMonMoi.equals("") || dataListDiemMonMoi.equals("[]") ||
                dataListBangDiemCaNhan == null || dataListBangDiemCaNhan.equals("") || dataListBangDiemCaNhan.equals("[]")) {
            return;
        }

        try {
            double diemTong = 0;
            double tongTC = 0;
            JSONArray diemMonMoiListJsonA = new JSONArray(dataListDiemMonMoi);
            JSONArray bangDiemCaNhanListJsonA = new JSONArray(dataListBangDiemCaNhan);
            for (int i = 0; i < diemMonMoiListJsonA.length(); i++) {
                JSONObject diemMonMoiJsonO = diemMonMoiListJsonA.getJSONObject(i);
                String maLop = diemMonMoiJsonO.getString(JsonUtils.MA_LOP);
                for (int j = 0; j < bangDiemCaNhanListJsonA.length(); j++) {
                    JSONObject diemHocPhanJsonO = bangDiemCaNhanListJsonA.getJSONObject(j);
                    String maLop2 = diemHocPhanJsonO.getString(JsonUtils.KEY_LOP_HOC);
                    if (maLop.equals(maLop2)) {
                        String diemChu = diemHocPhanJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        String soTC = diemHocPhanJsonO.getString(JsonUtils.KEY_TIN_CHI_HP);
                        for (String s : listDiemChu) {
                            if (s.equals(diemChu)) {
                                diemTong += Utils.getInstance().convertDiemChuToDiemSo(diemChu) * Double.parseDouble(soTC);
                                tongTC += Double.parseDouble(soTC);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            double gpa = diemTong / tongTC;
            binding.txtGPA.setText(String.valueOf((double) Math.round(gpa * 100) / 100));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RAKAN", "showGPA: " + e.toString());
        }
    }

    private void showListDiemMonMoi() {
        String dataListDiemMonMoi = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_KIEM_TRA_NHAP_DIEM_KI_MOI);

        if (dataListDiemMonMoi == null || dataListDiemMonMoi.equals("") || dataListDiemMonMoi.equals("[]")) {
            binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
            Glide.with(this).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
            binding.loHeader.setVisibility(View.GONE);
            return;
        }
        arrayList = new ArrayList<>();
        try {
            binding.loKhongCoDuLieu.setVisibility(View.GONE);
            binding.loHeader.setVisibility(View.VISIBLE);
            Glide.with(this).load(R.drawable.ic_ami_nghi_ngo).into(binding.imgStickerHeader);
            JSONArray diemMonMoiListJsonA = new JSONArray(dataListDiemMonMoi);
            for (int i = 0; i < diemMonMoiListJsonA.length(); i++) {
                JSONObject diemMonMoiJsonO = diemMonMoiListJsonA.getJSONObject(i);
                ThongTinDiemMonMoiModel thongTinDiemMonMoiModel = new ThongTinDiemMonMoiModel(
                        diemMonMoiJsonO.getString(JsonUtils.KEY_MA_SO_SV),
                        diemMonMoiJsonO.getString(JsonUtils.MA_LOP),
                        diemMonMoiJsonO.getString(JsonUtils.TEN_LOP),
                        diemMonMoiJsonO.getString(JsonUtils.KEY_TRONG_SO_QUA_TRINH),
                        diemMonMoiJsonO.getString(JsonUtils.KEY_DIEM_QUA_TRINH),
                        diemMonMoiJsonO.getString(JsonUtils.KEY_TT_DIEM_QUA_TRINH),
                        diemMonMoiJsonO.getString(JsonUtils.KEY_DIEM_THI),
                        diemMonMoiJsonO.getString(JsonUtils.KEY_TT_DIEM_THI));
                arrayList.add(thongTinDiemMonMoiModel);
            }
            adapter = new ThongTinDiemMonMoiAdapter(this, R.layout.item_thong_tin_diem_mon_moi, arrayList);
            binding.lvDiemMonMoi.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RAKAN", "showListDiemMonMoi: " + e.toString());
        }
    }
}