package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;

import java.util.ArrayList;

public class DanhSachKhoaHocAdapter extends ArrayAdapter<String> {
    private Context context;
    private ArrayList<String> listKhoaHoc;
    private int layoutResource;
    private ArrayList<Integer> listPosHighLight;

    public DanhSachKhoaHocAdapter(@NonNull Context context, int resource, @NonNull ArrayList<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.listKhoaHoc = objects;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtSTT = row.findViewById(R.id.txtSTT);
        TextView txtTenKhoaHoc = row.findViewById(R.id.txtTenKhoaHoc);
        RelativeLayout loItemTenKhoaHoc = row.findViewById(R.id.loItemTenKhoaHoc);

        txtSTT.setText(String.valueOf(position + 1));
        txtTenKhoaHoc.setText(listKhoaHoc.get(position));

        if (listPosHighLight != null && listPosHighLight.size() > 0 && listPosHighLight.contains(position)) {
            loItemTenKhoaHoc.setBackgroundColor(Color.GRAY);
            txtTenKhoaHoc.setTextColor(Color.WHITE);
        }

        return row;
    }

    public void setHighLightPosition(ArrayList<Integer> listPosition) {
        listPosHighLight = listPosition;
    }
}
