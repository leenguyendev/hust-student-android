package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.CommentReviewHocPhanModel;

import java.util.ArrayList;

public class ReviewHocPhanAdapter extends ArrayAdapter<CommentReviewHocPhanModel> {
    private Context context;
    private ArrayList<CommentReviewHocPhanModel> commentReviewHocPhanModels;
    private int layoutResource;

    public ReviewHocPhanAdapter(@NonNull Context context, int resource, @NonNull ArrayList<CommentReviewHocPhanModel> commentReviewHocPhanModels) {
        super(context, resource, commentReviewHocPhanModels);
        this.context = context;
        this.commentReviewHocPhanModels = commentReviewHocPhanModels;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtName = row.findViewById(R.id.txtName);
        TextView txtContent = row.findViewById(R.id.txtContent);
        TextView txtLikeNumber = row.findViewById(R.id.txtLikeNumber);
        RelativeLayout imgRank = row.findViewById(R.id.imgRank);
        RelativeLayout loLike = row.findViewById(R.id.loLike);
        TextView txtRank = row.findViewById(R.id.txtRank);

        txtName.setText(commentReviewHocPhanModels.get(position).getName());
        txtContent.setText(commentReviewHocPhanModels.get(position).getContent());
        if (commentReviewHocPhanModels.get(position).getLike() == 0) {
            loLike.setVisibility(View.GONE);
        } else {
            loLike.setVisibility(View.VISIBLE);
            txtLikeNumber.setText(String.valueOf(commentReviewHocPhanModels.get(position).getLike()));
        }

        if (position == 0) {
            imgRank.setBackgroundResource(R.drawable.rank_1);
            txtRank.setVisibility(View.GONE);
        } else if (position == 1) {
            imgRank.setBackgroundResource(R.drawable.rank_2);
            txtRank.setVisibility(View.GONE);
        } else if (position == 2) {
            imgRank.setBackgroundResource(R.drawable.rank_3);
            txtRank.setVisibility(View.GONE);
        } else {
            txtRank.setText(String.valueOf(position + 1));
        }

        return row;
    }
}
