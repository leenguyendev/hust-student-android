package com.leenguyen.huststudent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.databinding.ActivityMainBinding;
import com.leenguyen.huststudent.databinding.DialogNoticeBinding;
import com.leenguyen.huststudent.databinding.DialogRegisterVipBinding;
import com.leenguyen.huststudent.home.HomeFragment;
import com.leenguyen.huststudent.menu.HustVipActivity;
import com.leenguyen.huststudent.menu.MenuFragment;
import com.leenguyen.huststudent.utils.Utils;
import com.leenguyen.huststudent.widget.ThoiKhoaBieuWidget;

import java.util.Calendar;
import java.util.Objects;
import java.util.Timer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String ACTION_OPEN_MENU = "com.leenguyen.huststudent.action_open_menu";
    private ActivityMainBinding binding;
    private static final int PROFILE_POS = 0;
    private static final int BANG_DIEM_POS = 1;
    private static final int TKB_POS = 2;
    private static final int NOTIFICATION_POS = 3;
    private static final int MORE_POS = 4;
    private int fragmentOpenPos = -1;
    private Fragment[] fragments;
    private DatabaseReference versionDatabase;
    private boolean alreadyLogin;
    private String action;
    private Timer timer;
    private boolean isLogin = false;
    private boolean isVipMember = false, isNoAds = false;
    private AdView adView;
    private int maxAds = 0, adsCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        Intent intent = getIntent();
        action = intent.getAction();
        versionDatabase = FirebaseDatabase.getInstance("https://hust-student-version.firebaseio.com/").getReference();
        isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        alreadyLogin = Utils.getInstance().getValueFromSharedPreferences(
                this, Constant.SHARE_PREFERENCES_DATA,
                Constant.SHARE_PREFERENCES_DATA_ALREADY_USER_LOGIN).equals("1");
        String maxAdsString = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_MAX_ADS);
        if (maxAdsString != null && !maxAdsString.equals(""))
            maxAds = Integer.parseInt(maxAdsString);
        Calendar calendar = Calendar.getInstance();
        int currDay = calendar.get(Calendar.DAY_OF_YEAR);
        String preDay = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_PRE_DATE);
        String adsCountString = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_ADS_COUNT);
        if (adsCountString != null && !adsCountString.equals(""))
            adsCount = Integer.parseInt(adsCountString);
        if (preDay.equals("") || currDay != Integer.parseInt(preDay)
                && adsCount < maxAds
                && !isVipMember && alreadyLogin && !isNoAds) {
            MobileAds.initialize(getApplicationContext(), initializationStatus -> {

            });
            initAdsBanner();
            Utils.getInstance().saveToSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_ADS_COUNT, String.valueOf(adsCount + 1));
        } else if (adsCount == maxAds) {
            Utils.getInstance().saveToSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_PRE_DATE, String.valueOf(currDay));
            Utils.getInstance().saveToSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_ADS_COUNT, "0");
            binding.loAds.setVisibility(View.GONE);
        } else {
            binding.loAds.setVisibility(View.GONE);
        }

        initLayout();
        checkAlreadyLogin();
        binding.loProfile.setOnClickListener(this);
        binding.loBangDiem.setOnClickListener(this);
        binding.loTKB.setOnClickListener(this);
        binding.loMore.setOnClickListener(this);
        binding.loNotification.setOnClickListener(this);
    }

    @SuppressLint("MissingPermission")
    private void initAdsBanner() {
        binding.loAds.setVisibility(View.VISIBLE);
        String adsBannerId = Utils.getInstance().getValueFromSharedPreferences(
                getApplicationContext(),
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_ADS_BANNER_ID);
//        String adsBannerId = "ca-app-pub-3940256099942544/6300978111";
        adView = new AdView(this);
        adView.setAdUnitId(adsBannerId);
        binding.containAdsView.addView(adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        AdSize adSize = getAdSize();
        // Step 4 - Set the adaptive ad size on the ad view.
        adView.setAdSize(adSize);
        // Step 5 - Start loading the ad in the background.
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkVersion();
        if (adView != null)
            adView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (adView != null)
            adView.pause();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adView != null)
            adView.destroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (fragmentOpenPos == PROFILE_POS) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        } else {
            if (isLogin) {
                openFragment(PROFILE_POS);
            } else {
                Utils.getInstance().sendIntentActivity(this, LoginActivity.class);
            }
        }

    }

    private void initLayout() {
        fragments = new Fragment[]{new HomeFragment(), new BangDiemFragment(), new TKBFragment(), new NotificationFragment(), new MenuFragment()};
        if (action != null && action.equals(ThoiKhoaBieuWidget.OPEN_TKB_ACTION)) {
            openFragment(TKB_POS);
        } else if (action != null && action.equals(ACTION_OPEN_MENU)) {
            openFragment(MORE_POS);
        } else {
            openFragment(PROFILE_POS);
        }
    }

    private void showDangKyVipDialog() {
        Dialog dialog = new Dialog(this);
        DialogRegisterVipBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_register_vip, null, false);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(bindingDialog.getRoot());
        bindingDialog.btnRegister.setOnClickListener(v -> Utils.getInstance().sendIntentActivity(this, HustVipActivity.class));
        dialog.show();
    }

    public void openFragment(int pos) {
        if (fragmentOpenPos == pos) {
            return;
        }
        fragmentOpenPos = pos;
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.frMain, fragments[pos], "openFragment")
                .addToBackStack(null)
                .commit();
        setSelectedPos(pos);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loProfile:
                openFragment(PROFILE_POS);
                break;
            case R.id.loBangDiem:
                openFragment(BANG_DIEM_POS);
                break;
            case R.id.loTKB:
                openFragment(TKB_POS);
                break;
            case R.id.loNotification:
                openFragment(NOTIFICATION_POS);
                break;
            case R.id.loMore:
                openFragment(MORE_POS);
                break;
        }
    }

    private void checkAlreadyLogin() {
        if (!alreadyLogin) {
            binding.loProfile.setEnabled(false);
            binding.loBangDiem.setEnabled(false);
            binding.loTKB.setEnabled(false);
        } else {
            isLogin = true;
        }
    }

    private void setSelectedPos(int pos) {
        switch (pos) {
            case PROFILE_POS:
                binding.imgProfile.setSelected(true);
                binding.dividerProfile.setVisibility(View.VISIBLE);

                binding.imgList.setSelected(false);
                binding.dividerList.setVisibility(View.GONE);
                binding.imgTKB.setSelected(false);
                binding.dividerTKB.setVisibility(View.GONE);
                binding.imgMore.setSelected(false);
                binding.dividerMore.setVisibility(View.GONE);
                binding.imgNotification.setSelected(false);
                binding.dividerNotification.setVisibility(View.GONE);
                break;
            case BANG_DIEM_POS:
                binding.imgList.setSelected(true);
                binding.dividerList.setVisibility(View.VISIBLE);

                binding.imgProfile.setSelected(false);
                binding.dividerProfile.setVisibility(View.GONE);
                binding.imgTKB.setSelected(false);
                binding.dividerTKB.setVisibility(View.GONE);
                binding.imgMore.setSelected(false);
                binding.dividerMore.setVisibility(View.GONE);
                binding.imgNotification.setSelected(false);
                binding.dividerNotification.setVisibility(View.GONE);
                break;
            case TKB_POS:
                binding.imgTKB.setSelected(true);
                binding.dividerTKB.setVisibility(View.VISIBLE);

                binding.imgProfile.setSelected(false);
                binding.dividerProfile.setVisibility(View.GONE);
                binding.imgList.setSelected(false);
                binding.dividerList.setVisibility(View.GONE);
                binding.imgMore.setSelected(false);
                binding.dividerMore.setVisibility(View.GONE);
                binding.imgNotification.setSelected(false);
                binding.dividerNotification.setVisibility(View.GONE);
                break;
            case NOTIFICATION_POS:
                binding.imgNotification.setSelected(true);
                binding.dividerNotification.setVisibility(View.VISIBLE);

                binding.imgProfile.setSelected(false);
                binding.dividerProfile.setVisibility(View.GONE);
                binding.imgList.setSelected(false);
                binding.dividerList.setVisibility(View.GONE);
                binding.imgMore.setSelected(false);
                binding.dividerMore.setVisibility(View.GONE);
                binding.imgTKB.setSelected(false);
                binding.dividerTKB.setVisibility(View.GONE);
                break;
            case MORE_POS:
                binding.imgMore.setSelected(true);
                binding.dividerMore.setVisibility(View.VISIBLE);

                binding.imgProfile.setSelected(false);
                binding.dividerProfile.setVisibility(View.GONE);
                binding.imgList.setSelected(false);
                binding.dividerList.setVisibility(View.GONE);
                binding.imgTKB.setSelected(false);
                binding.dividerTKB.setVisibility(View.GONE);
                binding.imgNotification.setSelected(false);
                binding.dividerNotification.setVisibility(View.GONE);
                break;
        }
    }

    private void checkVersion() {
        versionDatabase.child("version").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String currentVersion = Utils.getInstance().getCurrentVersion(MainActivity.this);
                String latestVersion = Objects.requireNonNull(snapshot.getValue()).toString();
                if (Integer.parseInt(latestVersion) > Integer.parseInt(currentVersion)) {
                    showUpdateAppDialog(latestVersion);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void showUpdateAppDialog(String version) {
        Dialog dialog = new Dialog(this);
        DialogNoticeBinding dialogNoticeBinding = DataBindingUtil.inflate(LayoutInflater.from(MainActivity.this), R.layout.dialog_notice, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogNoticeBinding.getRoot());
        dialog.setCancelable(true);
        dialogNoticeBinding.btnOK.setText("Cập nhật ngay");
        dialogNoticeBinding.txtTitle.setText("Cập nhật " + version);
        dialogNoticeBinding.txtDecs.setText("Ứng dụng đã có phiên bản mới, cập nhật ngay để sử dụng ứng dụng tốt hơn nhé. Nếu Google Play không hiển thị bản cập nhật do đang hiển thị cache, bạn hãy back ra vào lại hoặc cài đặt lại app nhé.");
        dialogNoticeBinding.btnOK.setOnClickListener(v -> {
            final String appPackageName = getPackageName();
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
            dialog.dismiss();
        });
        dialog.show();
    }

    public boolean isVip() {
        return isVipMember;
    }

    public boolean isNoAds() {
        return isNoAds;
    }
}
