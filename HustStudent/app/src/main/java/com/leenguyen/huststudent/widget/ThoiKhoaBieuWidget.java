package com.leenguyen.huststudent.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.MainActivity;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.ThoiKhoaBieuModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

/**
 * Implementation of App Widget functionality.
 */
public class ThoiKhoaBieuWidget extends AppWidgetProvider {
    public static String REFRESH_ACTION = "android.appwidget.action.APPWIDGET_UPDATE";
    public static String OPEN_TKB_ACTION = "android.appwidget.action.OPEN_TKB_ACTION";
    private boolean isHasClassToday = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction() != null && intent.getAction().equals(REFRESH_ACTION)) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, ThoiKhoaBieuWidget.class));
            onUpdate(context, appWidgetManager, appWidgetIds);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {
            RemoteViews remoteViews = updateWidgetListView(context, appWidgetIds[i]);
            appWidgetManager.updateAppWidget(appWidgetIds[i], remoteViews);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.lvLichHoc);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    private RemoteViews updateWidgetListView(Context context, int appWidgetId) {
        //which layout to show on widget
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.thoi_khoa_bieu_widget);
        String tuanHienTai = Utils.getInstance().getTuanHocHienTai(context);
        Calendar calendar = Calendar.getInstance();
        String thu = calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ? "CN" : "Thứ " + calendar.get(Calendar.DAY_OF_WEEK);
        if (Utils.getInstance().getValueFromSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.SHARE_PREFERENCES_DATA_ALREADY_USER_LOGIN).equals("1")) {
            remoteViews.setViewVisibility(R.id.btnOpen, View.VISIBLE);
        } else {
            remoteViews.setViewVisibility(R.id.btnOpen, View.GONE);
            remoteViews.setTextViewText(R.id.txtKhongCoDuLieu, "Cần đăng nhập vào app để xem thời khóa biểu");
            remoteViews.setViewVisibility(R.id.loKhongCoDuLieu, View.VISIBLE);
            remoteViews.setImageViewResource(R.id.imgSticker, R.drawable.ic_ami_quay);
        }
        if (tuanHienTai.equals("0")) {
            remoteViews.setTextViewText(R.id.txtHomNay, "Lịch học hôm nay (" + thu + ", Tuần ..." + ")");
            remoteViews.setViewVisibility(R.id.loKhongCoDuLieu, View.VISIBLE);
            remoteViews.setImageViewResource(R.id.imgSticker, R.drawable.ic_ami_ok);
        } else {
            remoteViews.setTextViewText(R.id.txtHomNay, "Lịch học hôm nay (" + thu + ", Tuần " + tuanHienTai + ")");
            String tkbData = Utils.getInstance().getValueFromSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_TKB);
            try {
                JSONArray tkbJsonA = new JSONArray(tkbData);
                if (tkbJsonA.length() > 0) {
                    for (int i = 0; i < tkbJsonA.length(); i++) {
                        JSONObject object = tkbJsonA.getJSONObject(i);
                        String thoiGian = object.getString(JsonUtils.THOI_GIAN);
                        if (thoiGian.length() >= 4) {
                            String ngayHoc = String.valueOf(thoiGian.charAt(4));
                            if (ngayHoc.equals(String.valueOf(calendar.get(Calendar.DAY_OF_WEEK)))) {
                                isHasClassToday = true;
                                break;
                            }
                        }
                    }
                }

                if (isHasClassToday) {
                    remoteViews.setViewVisibility(R.id.loKhongCoDuLieu, View.GONE);
                    remoteViews.setViewVisibility(R.id.lvLichHoc, View.VISIBLE);
                    //RemoteViews Service needed to provide adapter for ListView
                    Intent svcIntent = new Intent(context, TKBWidgetService.class);
                    //passing app widget id to that RemoteViews Service
                    svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId + new Random().nextInt());
                    //setting a unique Uri to the intent
                    //don't know its purpose to me right now
                    svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));
                    //setting adapter to listview of the widget
                    remoteViews.setRemoteAdapter(R.id.lvLichHoc, svcIntent);
                } else {
                    remoteViews.setViewVisibility(R.id.loKhongCoDuLieu, View.VISIBLE);
                    remoteViews.setViewVisibility(R.id.lvLichHoc, View.GONE);
                    remoteViews.setTextViewText(R.id.txtKhongCoDuLieu, "Hôm nay không phải đi học, quẩy lên nào!!!");
                    remoteViews.setImageViewResource(R.id.imgSticker, R.drawable.ic_ami_quay);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("RAKAN", "populateListItem: " + e.toString());
            }
        }
        remoteViews.setOnClickPendingIntent(R.id.btnOpen, getPenIntent(context));
        return remoteViews;
    }

    static private PendingIntent getPenIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setAction(OPEN_TKB_ACTION);
        return PendingIntent.getActivity(context, 0, intent, 0);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

