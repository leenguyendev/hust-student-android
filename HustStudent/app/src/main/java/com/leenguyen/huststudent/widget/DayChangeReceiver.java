package com.leenguyen.huststudent.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.leenguyen.huststudent.utils.Utils;

public class DayChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_DATE_CHANGED)) {
            Utils.getInstance().sendBroadcastUpdateWidget(context);
        }
    }
}
