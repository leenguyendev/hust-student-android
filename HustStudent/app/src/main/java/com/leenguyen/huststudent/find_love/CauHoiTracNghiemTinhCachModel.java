package com.leenguyen.huststudent.find_love;

public class CauHoiTracNghiemTinhCachModel {
    private String cauHoi, cauTraLoi_1, cauTraLoi_2;
    private boolean isDapAn_1, isDapAn_2;

    public CauHoiTracNghiemTinhCachModel(String cauHoi, String cauTraLoi_1, String cauTraLoi_2, boolean isDapAn_1, boolean isDapAn_2) {
        this.cauHoi = cauHoi;
        this.cauTraLoi_1 = cauTraLoi_1;
        this.cauTraLoi_2 = cauTraLoi_2;
        this.isDapAn_1 = isDapAn_1;
        this.isDapAn_2 = isDapAn_2;
    }

    public CauHoiTracNghiemTinhCachModel() {
    }

    public String getCauHoi() {
        return cauHoi;
    }

    public void setCauHoi(String cauHoi) {
        this.cauHoi = cauHoi;
    }

    public String getCauTraLoi_1() {
        return cauTraLoi_1;
    }

    public void setCauTraLoi_1(String cauTraLoi_1) {
        this.cauTraLoi_1 = cauTraLoi_1;
    }

    public String getCauTraLoi_2() {
        return cauTraLoi_2;
    }

    public void setCauTraLoi_2(String cauTraLoi_2) {
        this.cauTraLoi_2 = cauTraLoi_2;
    }

    public boolean isDapAn_1() {
        return isDapAn_1;
    }

    public void setDapAn_1(boolean dapAn_1) {
        isDapAn_1 = dapAn_1;
    }

    public boolean isDapAn_2() {
        return isDapAn_2;
    }

    public void setDapAn_2(boolean dapAn_2) {
        isDapAn_2 = dapAn_2;
    }
}
