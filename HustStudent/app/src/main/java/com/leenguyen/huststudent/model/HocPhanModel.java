package com.leenguyen.huststudent.model;

public class HocPhanModel {
    private String maHocPhan, tenHP, hocKy, lop, diemQT, diemThi;
    private String soTinChi;
    private String diemTongKet;
    private boolean isHPCaiThien;
    private boolean isHPNew;
    private String diemCaiThien;

    public HocPhanModel(String maHocPhan, String tenHP, String hocKy, String lop, String diemQT, String diemThi, String soTinChi, String diemTongKet, String diemCaiThien, boolean isHPCaiThien, boolean isHPNew) {
        this.maHocPhan = maHocPhan;
        this.tenHP = tenHP;
        this.hocKy = hocKy;
        this.lop = lop;
        this.diemQT = diemQT;
        this.diemThi = diemThi;
        this.soTinChi = soTinChi;
        this.diemTongKet = diemTongKet;
        this.isHPCaiThien = isHPCaiThien;
        this.isHPNew = isHPNew;
        this.diemCaiThien = diemCaiThien;
    }

    public String getDiemCaiThien() {
        return diemCaiThien;
    }

    public void setDiemCaiThien(String diemCaiThien) {
        this.diemCaiThien = diemCaiThien;
    }

    public boolean isHPCaiThien() {
        return isHPCaiThien;
    }

    public void setHPCaiThien(boolean HPCaiThien) {
        isHPCaiThien = HPCaiThien;
    }

    public boolean isHPNew() {
        return isHPNew;
    }

    public void setHPNew(boolean HPNew) {
        isHPNew = HPNew;
    }

    public String getMaHocPhan() {
        return maHocPhan;
    }

    public void setMaHocPhan(String maHocPhan) {
        this.maHocPhan = maHocPhan;
    }

    public String getHocKy() {
        return hocKy;
    }

    public void setHocKy(String hocKy) {
        this.hocKy = hocKy;
    }

    public String getLop() {
        return lop;
    }

    public void setLop(String lop) {
        this.lop = lop;
    }

    public String getDiemQT() {
        return diemQT;
    }

    public void setDiemQT(String diemQT) {
        this.diemQT = diemQT;
    }

    public String getDiemThi() {
        return diemThi;
    }

    public void setDiemThi(String diemThi) {
        this.diemThi = diemThi;
    }

    public String getDiemTongKet() {
        return diemTongKet;
    }

    public void setDiemTongKet(String diemTongKet) {
        this.diemTongKet = diemTongKet;
    }

    public String getTenHP() {
        return tenHP;
    }

    public void setTenHP(String tenHP) {
        this.tenHP = tenHP;
    }

    public String getSoTinChi() {
        return soTinChi;
    }

    public void setSoTinChi(String soTinChi) {
        this.soTinChi = soTinChi;
    }
}
