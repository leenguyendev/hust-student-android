package com.leenguyen.huststudent.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.HocPhiAdapter;
import com.leenguyen.huststudent.databinding.ActivityHocPhiBinding;
import com.leenguyen.huststudent.databinding.DialogThongTinHocPhiHocPhanBinding;
import com.leenguyen.huststudent.model.HocPhiModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HocPhiActivity extends AppCompatActivity {
    private ActivityHocPhiBinding binding;
    private HocPhiAdapter adapter;
    private ArrayList<HocPhiModel> hocPhiModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hoc_phi);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        boolean isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVipMember && !isNoAds) {
            initAdsBanner();
        } else {
            binding.adView.setVisibility(View.GONE);
        }
        initLayout();
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    private void initLayout() {
        binding.btnBack.setOnClickListener(view -> onBackPressed());
        showThongTinHocPhi();
        binding.lvHocPhiHocPhan.setOnItemClickListener((parent, view, position, id) -> showDialogThongTinHocPhiHocPhanChiTiet(hocPhiModels.get(position)));
    }

    private void showDialogThongTinHocPhiHocPhanChiTiet(HocPhiModel hocPhiModel) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final DialogThongTinHocPhiHocPhanBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_thong_tin_hoc_phi_hoc_phan, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        bindingDialog.txtMaHP.setText("Mã HP: " + hocPhiModel.getMaHP());
        bindingDialog.txtTenHP.setText("Tên HP: " + hocPhiModel.getTenHP());
        bindingDialog.txtSoTienMotTinChi.setText("Học phí/TC: " + hocPhiModel.getSoTienMotTCHocPhi());
        bindingDialog.txtSoTinChiHocPhi.setText("Số TC học phí: " + hocPhiModel.getSoTinChiHocPhi());
        bindingDialog.txtHeSoHocPhi.setText("Hệ số học phí: " + hocPhiModel.getHeSoHocPhi());
        bindingDialog.txtTongHocPhiHocPhan.setText("Tổng học phí HP: " + hocPhiModel.getTongSoTienHocPhan());
        bindingDialog.txtTrangThaiDangKy.setText("Trạng thái đăng ký: " + hocPhiModel.getTrangThaiDangKy());
        bindingDialog.txtLoaiDangKy.setText("Loại đăng ký: " + hocPhiModel.getLoaiDangKy());
        bindingDialog.txtGhiChu.setText("Ghi chú: " + hocPhiModel.getGhiChu());

        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void showThongTinHocPhi() {
        String dataThongTinHocPhi = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_HOC_PHI);
        if (dataThongTinHocPhi != null && !dataThongTinHocPhi.equals("")) {
            hocPhiModels = new ArrayList<>();
            try {
                String tongSoTienPhaiDong = new JSONObject(dataThongTinHocPhi).getString(JsonUtils.KEY_TONG_SO_TIEN_PHAI_DONG);
                String ghiChu = new JSONObject(dataThongTinHocPhi).getString(JsonUtils.KEY_GHI_CHU);
                binding.txtTongSoTienPhaiDong.setText(tongSoTienPhaiDong.equals(" ") ? "0 đ" : tongSoTienPhaiDong + " đ");
                binding.txtGhiChu.setText(ghiChu);
                JSONArray bangHocPhiHocPhanJsonA = new JSONObject(dataThongTinHocPhi).getJSONArray(JsonUtils.KEY_BANG_HOC_PHI_HOC_PHAN);
                if (bangHocPhiHocPhanJsonA.length() > 0) {
                    for (int i = 0; i < bangHocPhiHocPhanJsonA.length(); i++) {
                        JSONObject hocPhiHocPhanJsonO = bangHocPhiHocPhanJsonA.getJSONObject(i);
                        hocPhiModels.add(new HocPhiModel(hocPhiHocPhanJsonO.getString(JsonUtils.KEY_MA_HP),
                                hocPhiHocPhanJsonO.getString(JsonUtils.KEY_TEN_HP),
                                hocPhiHocPhanJsonO.getString(JsonUtils.KEY_SO_TIN_MOT_TIN_CHI),
                                hocPhiHocPhanJsonO.getString(JsonUtils.KEY_SO_TIN_CHI_HOC_PHI),
                                hocPhiHocPhanJsonO.getString(JsonUtils.KEY_HE_SO_HOC_PHI_LOP),
                                hocPhiHocPhanJsonO.getString(JsonUtils.KEY_TONG_SO_TIEN_HOC_PHAN),
                                hocPhiHocPhanJsonO.getString(JsonUtils.KEY_TRANG_THAI_DANG_KY),
                                hocPhiHocPhanJsonO.getString(JsonUtils.KEY_LOAI_DANG_KY),
                                hocPhiHocPhanJsonO.getString(JsonUtils.KEY_GHI_CHU)));
                    }
                    if (hocPhiModels.size() > 0) {
                        adapter = new HocPhiAdapter(this, R.layout.item_hoc_phi_hoc_phan, hocPhiModels);
                        binding.lvHocPhiHocPhan.setAdapter(adapter);
                    }
                }

            } catch (Exception e) {
                Log.e("RAKAN", "showThongTinHocPhi: " + e.toString());
            }
            binding.loKhongCoDuLieu.setVisibility(View.GONE);
            binding.loHeader.setVisibility(View.VISIBLE);
            Glide.with(this).load(R.drawable.ic_ami_boi_roi).into(binding.imgStickerHeader);
        } else {
            binding.loHeader.setVisibility(View.GONE);
            binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
            Glide.with(this).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
            binding.txtGhiChu.setVisibility(View.GONE);
            binding.txtChuThich.setVisibility(View.GONE);
            binding.footer.setVisibility(View.GONE);
        }
    }
}
