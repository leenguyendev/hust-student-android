package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.DiemThiToeicModel;

import java.util.ArrayList;
import java.util.Random;

public class DiemThiToeicAdapter extends ArrayAdapter<DiemThiToeicModel> {
    private Context context;
    private ArrayList<DiemThiToeicModel> diemThiToeicModels;
    private int layoutResource;

    public DiemThiToeicAdapter(@NonNull Context context, int resource, @NonNull ArrayList<DiemThiToeicModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.diemThiToeicModels = objects;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtHocKy = row.findViewById(R.id.txtHocKy);
        TextView txtDiemNghe = row.findViewById(R.id.txtDiemNghe);
        TextView txtDiemDoc = row.findViewById(R.id.txtDiemDoc);
        TextView txtDiemTong = row.findViewById(R.id.txtDiemTong);
        RelativeLayout loDivider = row.findViewById(R.id.loDivider);

        txtHocKy.setText(diemThiToeicModels.get(position).getHocKy());
        txtDiemNghe.setText("Điểm nghe: " + diemThiToeicModels.get(position).getDiemNghe());
        txtDiemDoc.setText("Điểm đọc: " + diemThiToeicModels.get(position).getDiemDoc());
        txtDiemTong.setText(diemThiToeicModels.get(position).getDiemTong());
        Random random = new Random();
        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        loDivider.setBackgroundColor(color);

        return row;
    }
}
