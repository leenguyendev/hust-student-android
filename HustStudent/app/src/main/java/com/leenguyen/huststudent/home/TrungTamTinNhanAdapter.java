package com.leenguyen.huststudent.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.WebviewActivity;
import com.leenguyen.huststudent.model.QuangCaoModel;

import java.util.ArrayList;
import java.util.Random;

public class TrungTamTinNhanAdapter extends ArrayAdapter<QuangCaoModel> {
    private Context context;
    private ArrayList<QuangCaoModel> quangCaoModels;
    private int layoutResource;

    public TrungTamTinNhanAdapter(@NonNull Context context, int resource, @NonNull ArrayList<QuangCaoModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.quangCaoModels = objects;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView name = row.findViewById(R.id.txtName);
        TextView content = row.findViewById(R.id.txtContent);
        TextView txtXemChiTiet = row.findViewById(R.id.txtXemTrang);
        RelativeLayout loDivider = row.findViewById(R.id.loDivider);

        name.setText(quangCaoModels.get(position).getName());
        content.setText(quangCaoModels.get(position).getContent());
        content.setSelected(true);
        if (quangCaoModels.get(position).getLink().equals("")) {
            txtXemChiTiet.setVisibility(View.GONE);
        } else {
            txtXemChiTiet.setVisibility(View.VISIBLE);
        }
        Random random = new Random();
        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        loDivider.setBackgroundColor(color);

        txtXemChiTiet.setOnClickListener(v -> {
            Intent intent = new Intent(context, WebviewActivity.class);
            intent.putExtra("link", quangCaoModels.get(position).getLink());
            intent.putExtra("title", quangCaoModels.get(position).getName());
            context.startActivity(intent);
        });
        return row;
    }
}