package com.leenguyen.huststudent.menu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.ReviewHocPhanAdapter;
import com.leenguyen.huststudent.databinding.ActivityReviewHocPhanBinding;
import com.leenguyen.huststudent.databinding.DialogThemBinhLuanBinding;
import com.leenguyen.huststudent.databinding.DialogThemHocPhanBinding;
import com.leenguyen.huststudent.model.CommentReviewHocPhanModel;
import com.leenguyen.huststudent.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ReviewHocPhanActivity extends AppCompatActivity {
    private ActivityReviewHocPhanBinding binding;
    private DatabaseReference reviewHPDatabase;
    private ReviewHocPhanAdapter reviewHocPhanAdapter;
    private ArrayList<CommentReviewHocPhanModel> commentReviewHocPhanModels;
    private boolean isFABOpen = false;
    private String maHocPhanReview = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_review_hoc_phan);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        reviewHPDatabase = FirebaseDatabase.getInstance("https://hust-student-review-hoc-phan.firebaseio.com/").getReference();
        boolean isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVipMember && !isNoAds) {
            binding.adView.setVisibility(View.VISIBLE);
            initAdsBanner();
        } else {
            binding.adView.setVisibility(View.GONE);
        }
        initLayout();
        checkInternet();
        binding.btnBack.setOnClickListener(v -> onBackPressed());
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    private void checkInternet() {
        if (!Utils.getInstance().isOnline(this)) {
            Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), getString(R.string.can_co_ket_noi_mang));
        }
    }

    private void initLayout() {
        binding.loThongTinHP.setVisibility(View.GONE);
        binding.loDivider.setVisibility(View.GONE);
        binding.edtSearch.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard(ReviewHocPhanActivity.this);
                searchHocPhanReview(binding.edtSearch.getText().toString().toUpperCase().trim());
                return true;
            }
            return false;
        });

        binding.loFabComment.setOnClickListener(v -> {
            if (!maHocPhanReview.equals("")) {
                showDialogThemBinhLuan();
                closeFABMenu();
            } else {
                Utils.getInstance().showNoticeDialog(ReviewHocPhanActivity.this, getString(R.string.chu_y), getString(R.string.khong_co_hoc_phan_dang_duoc_review));
            }
        });

        binding.loFabThemHocPhan.setOnClickListener(v -> {
            showDialogThemHocPhan();
            closeFABMenu();
        });

        binding.fabButton.setOnClickListener(v -> {
            if (!isFABOpen) {
                showFabMenu();
            } else {
                closeFABMenu();
            }
        });

        binding.lvComment.setOnItemLongClickListener((parent, view, position, id) -> {
            int curentLikeNumber = commentReviewHocPhanModels.get(position).getLike();
            reviewHPDatabase.child(maHocPhanReview).child("comment").child(commentReviewHocPhanModels.get(position).getTimeStamp()).child("like").setValue(curentLikeNumber + 1);
            return false;
        });
    }

    private void searchHocPhanReview(String maHocPhan) {
        maHocPhanReview = maHocPhan;
        //showTenHocPhan
        reviewHPDatabase.child(maHocPhan).child("tenHocPhan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getValue() != null) {
                    binding.txtTenHP.setText(snapshot.getValue().toString());
                    binding.loThongTinHP.setVisibility(View.VISIBLE);
                    binding.loDivider.setVisibility(View.VISIBLE);
                    binding.loLoiNoiDau.setVisibility(View.GONE);
                } else {
                    Utils.getInstance().showNoticeDialog(ReviewHocPhanActivity.this, "Tiếc quá", "Học phần này chưa được thêm và review");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        //Show ma HP
        reviewHPDatabase.child(maHocPhan).child("maHocPhan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getValue() != null) {
                    binding.txtMaHP.setText(snapshot.getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        //show comment
        reviewHPDatabase.child(maHocPhan).child("comment").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                commentReviewHocPhanModels = new ArrayList<>();
                List<String> keys = new ArrayList<>();
                for (DataSnapshot keyNote : snapshot.getChildren()) {
                    keys.add(keyNote.getKey());
                    CommentReviewHocPhanModel commentReviewHocPhanModel = keyNote.getValue(CommentReviewHocPhanModel.class);
                    commentReviewHocPhanModels.add(commentReviewHocPhanModel);
                }
                commentReviewHocPhanModels = Utils.sortByLikeComment(commentReviewHocPhanModels);
                if (commentReviewHocPhanModels.size() == 0) {
                    if (binding.loThongTinHP.getVisibility() != View.GONE) {
                        Toast.makeText(ReviewHocPhanActivity.this, "Hiện tại học phần này chưa có nhận xét nào", Toast.LENGTH_LONG).show();
                    }
                }
                showDataToListView();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void showDialogThemHocPhan() {
        final Dialog dialog = new Dialog(ReviewHocPhanActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final DialogThemHocPhanBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_them_hoc_phan, null, false);
        dialog.setContentView(bindingDialog.getRoot());

        bindingDialog.btnGui.setOnClickListener(v -> {
            if (bindingDialog.edtMaHocPhan.getText().toString().trim().equals("") || bindingDialog.edtTenHocPhan.getText().toString().trim().equals("")) {
                bindingDialog.txtNotice.setVisibility(View.VISIBLE);
            } else {
                bindingDialog.txtNotice.setVisibility(View.GONE);
                final boolean[] hocPhanDaTonTai = {false};
                String maHocPhan = bindingDialog.edtMaHocPhan.getText().toString().toUpperCase().trim();
                String tenHocPhan = bindingDialog.edtTenHocPhan.getText().toString().trim();
                reviewHPDatabase.child(maHocPhan).child("maHocPhan").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.getValue() != null) {
                            hocPhanDaTonTai[0] = true;
                        }
                        if (!hocPhanDaTonTai[0]) {
                            reviewHPDatabase.child(maHocPhan).child("maHocPhan").setValue(maHocPhan);
                            reviewHPDatabase.child(maHocPhan).child("tenHocPhan").setValue(tenHocPhan);
                            reviewHPDatabase.child("0Check").child(maHocPhan).setValue(tenHocPhan); //to check only
                            searchHocPhanReview(maHocPhan);
                        } else {
                            Utils.getInstance().showNoticeDialog(ReviewHocPhanActivity.this, "Thông báo", "Học phần này đã tồn tại, vui lòng nhập mã học phần trong ô tìm kiếm bên trên để xem review.");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
                dialog.dismiss();
            }
        });
        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void showDialogThemBinhLuan() {
        final Dialog dialog = new Dialog(ReviewHocPhanActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final DialogThemBinhLuanBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_them_binh_luan, null, false);
        dialog.setContentView(bindingDialog.getRoot());

        bindingDialog.btnGui.setOnClickListener(v -> {
            if (bindingDialog.edtNickName.getText().toString().trim().equals("") || bindingDialog.edtContent.getText().toString().trim().equals("")) {
                bindingDialog.txtNotice.setVisibility(View.VISIBLE);
            } else {
                bindingDialog.txtNotice.setVisibility(View.GONE);
                String timeStamp = String.valueOf(System.currentTimeMillis() / 1000);
                String nickName = bindingDialog.edtNickName.getText().toString().trim();
                String content = bindingDialog.edtContent.getText().toString().trim();
                CommentReviewHocPhanModel commentReviewHocPhanModel = new CommentReviewHocPhanModel(nickName, content, 0, timeStamp);
                reviewHPDatabase.child(maHocPhanReview).child("comment").child(timeStamp).setValue(commentReviewHocPhanModel);
                dialog.dismiss();
            }
        });

        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void showDataToListView() {
        reviewHocPhanAdapter = new ReviewHocPhanAdapter(ReviewHocPhanActivity.this, R.layout.item_comment_review_hoc_phan, commentReviewHocPhanModels);
        binding.lvComment.setAdapter(reviewHocPhanAdapter);
        reviewHocPhanAdapter.notifyDataSetChanged();
    }

    private static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void showFabMenu() {
        isFABOpen = true;
//        binding.fabBGLayout.setVisibility(View.VISIBLE);
        binding.fabButton.animate().rotationBy(45);
        binding.loFabThemHocPhan.setVisibility(View.VISIBLE);
        binding.loFabComment.setVisibility(View.VISIBLE);
        binding.loFabThemHocPhan.animate().translationY(-getResources().getDimension(R.dimen.standard_60));
        binding.loFabComment.animate().translationY(-getResources().getDimension(R.dimen.standard_120));
    }

    private void closeFABMenu() {
        isFABOpen = false;
//        binding.fabBGLayout.setVisibility(View.GONE);
        binding.loFabComment.animate().translationY(0);
        binding.loFabThemHocPhan.animate().translationY(0);
        binding.fabButton.animate().rotation(0);
        binding.loFabThemHocPhan.animate().translationY(0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (!isFABOpen) {
                    binding.loFabComment.setVisibility(View.GONE);
                    binding.loFabThemHocPhan.setVisibility(View.GONE);
                }
            }
        });

    }
}