package com.leenguyen.huststudent.find_love;

public class ThongTinGioiThieuModel {
    private String hoTen, mssv, khoaVien, email, soDienThoai, ngaySinh, gioiTinh, queQuan, facebook, dacDiemCaNhanVaNguoiAy;
    private boolean isDaCoGau;

    public ThongTinGioiThieuModel() {
    }

    public ThongTinGioiThieuModel(String hoTen, String mssv, String khoaVien, String email,
                                  String soDienThoai, String ngaySinh, String gioiTinh,
                                  String queQuan, String facebook, boolean isDaCoGau,
                                  String dacDiemCaNhanVaNguoiAy) {
        this.hoTen = hoTen;
        this.mssv = mssv;
        this.khoaVien = khoaVien;
        this.email = email;
        this.soDienThoai = soDienThoai;
        this.ngaySinh = ngaySinh;
        this.gioiTinh = gioiTinh;
        this.queQuan = queQuan;
        this.facebook = facebook;
        this.isDaCoGau = isDaCoGau;
        this.dacDiemCaNhanVaNguoiAy = dacDiemCaNhanVaNguoiAy;
    }

    public String getDacDiemCaNhanVaNguoiAy() {
        return dacDiemCaNhanVaNguoiAy;
    }

    public void setDacDiemCaNhanVaNguoiAy(String dacDiemCaNhanVaNguoiAy) {
        this.dacDiemCaNhanVaNguoiAy = dacDiemCaNhanVaNguoiAy;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public String getMssv() {
        return mssv;
    }

    public void setMssv(String mssv) {
        this.mssv = mssv;
    }

    public String getKhoaVien() {
        return khoaVien;
    }

    public void setKhoaVien(String khoaVien) {
        this.khoaVien = khoaVien;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) {
        this.soDienThoai = soDienThoai;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getQueQuan() {
        return queQuan;
    }

    public void setQueQuan(String queQuan) {
        this.queQuan = queQuan;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public boolean isDaCoGau() {
        return isDaCoGau;
    }

    public void setDaCoGau(boolean daCoGau) {
        isDaCoGau = daCoGau;
    }
}
