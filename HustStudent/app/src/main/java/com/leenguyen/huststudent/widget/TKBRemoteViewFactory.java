package com.leenguyen.huststudent.widget;

import android.app.LauncherActivity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.ThoiKhoaBieuAdapter;
import com.leenguyen.huststudent.model.ThoiKhoaBieuModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

public class TKBRemoteViewFactory implements RemoteViewsService.RemoteViewsFactory {
    private ArrayList<ThoiKhoaBieuModel> listItemList;
    private Context context;
    private int appWidgetId;

    public TKBRemoteViewFactory(Context context, Intent intent) {
        this.context = context;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        populateListItem();
    }

    private void populateListItem() {
        Calendar calendar = Calendar.getInstance();
        String thu = String.valueOf(calendar.get(Calendar.DAY_OF_WEEK));
        String tkbData = Utils.getInstance().getValueFromSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_TKB);
        try {
            JSONArray tkbJsonA = new JSONArray(tkbData);
            if (tkbJsonA.length() > 0) {
                listItemList = new ArrayList<>();
                listItemList.clear();
                for (int i = 0; i < tkbJsonA.length(); i++) {
                    JSONObject object = tkbJsonA.getJSONObject(i);
                    String thoiGian = object.getString(JsonUtils.THOI_GIAN);
                    String tuanHoc = object.getString(JsonUtils.TUAN_HOC);
                    String phongHoc = object.getString(JsonUtils.PHONG_HOC);
                    String maLop = object.getString(JsonUtils.MA_LOP);
                    String loaiLop = object.getString(JsonUtils.LOAI_LOP);
                    String nhom = object.getString(JsonUtils.NHOM);
                    String maHP = object.getString(JsonUtils.MA_HP);
                    String tenLop = object.getString(JsonUtils.TEN_LOP);
                    String ghiChu = object.getString(JsonUtils.GHI_CHU);
                    String hinhThucDay = object.getString(JsonUtils.HINH_THUC_DAY);
                    String giangVien = object.getString(JsonUtils.GIANG_VIEN);
                    String linkOnline = object.getString(JsonUtils.LINK_ONLINE);
                    String maCode = object.getString(JsonUtils.MA_CODE);
                    if (thoiGian.length() >= 4) {
                        String ngayHoc = String.valueOf(thoiGian.charAt(4));
                        if (thu.equals(ngayHoc)) {
                            ThoiKhoaBieuModel thoiKhoaBieuModel = new ThoiKhoaBieuModel(thoiGian, tuanHoc, phongHoc, maLop, loaiLop, nhom, maHP, tenLop, ghiChu, hinhThucDay, giangVien, linkOnline, maCode);
                            listItemList.add(thoiKhoaBieuModel);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RAKAN", "populateListItem: " + e.toString());
        }
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return listItemList != null ? listItemList.size() : 0;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        if (position >= getCount()) {
            return getLoadingView();
        }
        RemoteViews remoteView = new RemoteViews(
                context.getPackageName(), R.layout.item_tkb_widget);
        Random random = new Random();
        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        remoteView.setInt(R.id.loDivider, "setBackgroundColor", color);

        ThoiKhoaBieuModel thoiKhoaBieuModel = listItemList.get(position);
        remoteView.setTextViewText(R.id.txtTenHP, thoiKhoaBieuModel.getTenLop());
        remoteView.setTextViewText(R.id.txtLoaiLop, "(" + thoiKhoaBieuModel.getLoaiLop() + ")");
        remoteView.setTextViewText(R.id.txtPhongHoc, thoiKhoaBieuModel.getPhongHoc());
        remoteView.setTextViewText(R.id.txtTuanHoc, thoiKhoaBieuModel.getTuanHoc());
        String thoiGian = thoiKhoaBieuModel.getThoiGian();
        if (thoiGian.equals("") || thoiGian.length() == 1 || !thoiGian.contains(",") || !thoiGian.contains("-")) {
            remoteView.setTextViewText(R.id.txtStartTime, "");
            remoteView.setTextViewText(R.id.txtEndTime, "");
        } else {
            int viTriDauPhay = thoiGian.indexOf(",");
            int viTriDauGach = thoiGian.indexOf("-");
            remoteView.setTextViewText(R.id.txtStartTime, thoiGian.substring(viTriDauPhay + 1, viTriDauGach - 1));
            remoteView.setTextViewText(R.id.txtEndTime, thoiGian.substring(viTriDauGach + 2));
        }

        //Đánh dấu môn học tuan hien tai
        String tuanHienTai = Utils.getInstance().getTuanHocHienTai(context);
        String tuanHoc = thoiKhoaBieuModel.getTuanHoc();
        ArrayList<String> listTuanHoc = new ArrayList<>();
        //Case 1,2,3,12 or 1-2,3-6 or 1,2,4-9
        StringBuilder builder = new StringBuilder();
        String startTime = null;
        String endTime = null;
        boolean isRangeWeek = false;
        for (int i = 0; i < tuanHoc.length(); i++) {
            if ((String.valueOf(tuanHoc.charAt(i)).equals(",") || String.valueOf(tuanHoc.charAt(i)).equals(".")) && !isRangeWeek) {
                listTuanHoc.add(String.valueOf(builder).trim());
                builder = new StringBuilder();
            } else if (String.valueOf(tuanHoc.charAt(i)).equals("-")) {
                startTime = builder.toString().trim();
                isRangeWeek = true;
                builder = new StringBuilder();
            } else if ((String.valueOf(tuanHoc.charAt(i)).equals(",") || String.valueOf(tuanHoc.charAt(i)).equals(".")) && isRangeWeek) {
                endTime = builder.toString().trim();
                for (int j = Integer.parseInt(startTime); j <= Integer.parseInt(endTime); j++) {
                    listTuanHoc.add(String.valueOf(j));
                }
                startTime = null;
                endTime = null;
                isRangeWeek = false;
                builder = new StringBuilder();
            } else if (i == tuanHoc.length() - 1) {
                builder.append(tuanHoc.charAt(i));
                if (!isRangeWeek) {
                    listTuanHoc.add(String.valueOf(builder));
                } else {
                    endTime = builder.toString().trim();
                    for (int j = Integer.parseInt(startTime); j <= Integer.parseInt(endTime); j++) {
                        listTuanHoc.add(String.valueOf(j));
                    }
                }
            } else {
                builder.append(tuanHoc.charAt(i));
            }
        }
        if (listTuanHoc.size() > 0) {
            for (int i = 0; i < listTuanHoc.size(); i++) {
                if (tuanHienTai.equals(listTuanHoc.get(i))) {
                    if (thoiKhoaBieuModel.getLoaiLop().equals("TN")) {
                        remoteView.setViewVisibility(R.id.imgReminderMonThiNghiem, View.VISIBLE);
                        remoteView.setViewVisibility(R.id.imgReminderMonHoc, View.GONE);
                    } else {
                        remoteView.setViewVisibility(R.id.imgReminderMonThiNghiem, View.GONE);
                        remoteView.setViewVisibility(R.id.imgReminderMonHoc, View.VISIBLE);
                    }

                    remoteView.setViewVisibility(R.id.loItemTKB, View.VISIBLE);
                    break;
                } else {
                    remoteView.setViewVisibility(R.id.loItemTKB, View.VISIBLE);
                }
            }
        }
        return remoteView;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 2441;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
