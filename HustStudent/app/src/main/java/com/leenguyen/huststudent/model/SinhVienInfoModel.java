package com.leenguyen.huststudent.model;

public class SinhVienInfoModel {
    private String maSV, hoSV, demSV, tenSV, ngaySinhSV, lopSV, chuongTrinhDaoTaoSV, trangThaiHocTapSV;

    public SinhVienInfoModel(String maSV, String hoSV, String demSV, String tenSV, String ngaySinhSV, String lopSV, String chuongTrinhDaoTaoSV, String trangThaiHocTapSV) {
        this.maSV = maSV;
        this.hoSV = hoSV;
        this.demSV = demSV;
        this.tenSV = tenSV;
        this.ngaySinhSV = ngaySinhSV;
        this.lopSV = lopSV;
        this.chuongTrinhDaoTaoSV = chuongTrinhDaoTaoSV;
        this.trangThaiHocTapSV = trangThaiHocTapSV;
    }

    public String getMaSV() {
        return maSV;
    }

    public void setMaSV(String maSV) {
        this.maSV = maSV;
    }

    public String getHoSV() {
        return hoSV;
    }

    public void setHoSV(String hoSV) {
        this.hoSV = hoSV;
    }

    public String getDemSV() {
        return demSV;
    }

    public void setDemSV(String demSV) {
        this.demSV = demSV;
    }

    public String getTenSV() {
        return tenSV;
    }

    public void setTenSV(String tenSV) {
        this.tenSV = tenSV;
    }

    public String getNgaySinhSV() {
        return ngaySinhSV;
    }

    public void setNgaySinhSV(String ngaySinhSV) {
        this.ngaySinhSV = ngaySinhSV;
    }

    public String getLopSV() {
        return lopSV;
    }

    public void setLopSV(String lopSV) {
        this.lopSV = lopSV;
    }

    public String getChuongTrinhDaoTaoSV() {
        return chuongTrinhDaoTaoSV;
    }

    public void setChuongTrinhDaoTaoSV(String chuongTrinhDaoTaoSV) {
        this.chuongTrinhDaoTaoSV = chuongTrinhDaoTaoSV;
    }

    public String getTrangThaiHocTapSV() {
        return trangThaiHocTapSV;
    }

    public void setTrangThaiHocTapSV(String trangThaiHocTapSV) {
        this.trangThaiHocTapSV = trangThaiHocTapSV;
    }
}
