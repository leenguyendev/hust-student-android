package com.leenguyen.huststudent.menu;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.LoginActivity;
import com.leenguyen.huststudent.MainActivity;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.databinding.DialogUpdateDataBinding;
import com.leenguyen.huststudent.databinding.FragmentMenuBinding;
import com.leenguyen.huststudent.find_love.ThongTinCaNhanTimNguoiYeuActivity;
import com.leenguyen.huststudent.game.CaroActivity;
import com.leenguyen.huststudent.model.HustRankingModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.JsoupUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public class MenuFragment extends Fragment implements View.OnClickListener {
    private FragmentMenuBinding binding;
    private Bitmap bitmap;
    private HashMap<String, String> cookiesLogin;
    private Dialog dialog;
    private DialogUpdateDataBinding bindingDialog;
    private DatabaseReference rankingDatabase;
    private boolean isPrivate = false;
    private String startDayTurnOn;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false);
        rankingDatabase = FirebaseDatabase.getInstance("https://hust-student-ranking.firebaseio.com/").getReference();
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        initLayout();
        String alreadyLogin = Utils.getInstance().getValueFromSharedPreferences(requireContext(), Constant.SHARE_PREFERENCES_DATA, Constant.SHARE_PREFERENCES_DATA_ALREADY_USER_LOGIN);
        boolean isUserLogin = alreadyLogin.equals("1");
        if (!isUserLogin) {
            disableFeatures();
        } else {
            showThongTinSV();
        }
    }

    @SuppressLint("SetTextI18n")
    private void initLayout() {
        binding.txtLastUpdateTime.setText("Cập nhật lần cuối: " +
                Utils.getInstance().getValueFromSharedPreferences(requireContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_LAST_TIME_UPDATE_DATA));
        Glide.with(requireContext()).load(R.drawable.ic_ami_love_2).into(binding.imgTimNguoiYeu);
        binding.loStudentInfo.setOnClickListener(this);
        binding.loDanhSachSV.setOnClickListener(this);
        binding.loDiemThiToeic.setOnClickListener(this);
        binding.loTKBTamThoi.setOnClickListener(this);
        binding.loLichThi.setOnClickListener(this);
        binding.loBanDo.setOnClickListener(this);
        binding.loMucTieuTotNghiep.setOnClickListener(this);
        binding.loHocPhi.setOnClickListener(this);
        binding.loMonNo.setOnClickListener(this);
        binding.loHustRanking.setOnClickListener(this);
        binding.loTraCuuNhapDiemKyMoi.setOnClickListener(this);
        binding.loGiaiTri.setOnClickListener(this);
        binding.loShop.setOnClickListener(this);
        binding.btnUpdate.setOnClickListener(this);
        binding.btnSettings.setOnClickListener(this);
        binding.loTimNguoiYeu.setOnClickListener(this);
        binding.loReviewHocPhan.setOnClickListener(this);
        binding.loHustVip.setOnClickListener(this);
    }

    private void showThongTinSV() {
        if (getContext() == null) {
            return;
        }
        String studentInfoData = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        try {
            JSONObject studentInfoJsonO = new JSONObject(studentInfoData);
            binding.txtHoTenSV.setText(studentInfoJsonO.getString(JsonUtils.KEY_HO_TEN_SV));
            String encodedImage = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR);
            String rotateAngle = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR_ROTATE_ANGLE);
            if (encodedImage != null && !encodedImage.equals("")) {
                byte[] imageAsBytes = Base64.decode(encodedImage.getBytes(), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
                bitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * 0.5), (int) (bitmap.getHeight() * 0.5), true);
                binding.imgAvatar.setImageBitmap(bitmap);
                binding.imgAvatar.setRotation(Float.parseFloat(rotateAngle));
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RAKAN", "showThongTinSV: " + e);
        }

    }

    private void disableFeatures() {
        binding.loStudentInfo.setEnabled(false);
        binding.loDanhSachSV.setEnabled(false);
        binding.loDiemThiToeic.setEnabled(false);
        binding.loTKBTamThoi.setEnabled(false);
        binding.loLichThi.setEnabled(false);
        binding.loMucTieuTotNghiep.setEnabled(false);
        binding.loHocPhi.setEnabled(false);
        binding.loMonNo.setEnabled(false);
        binding.loHustRanking.setEnabled(false);
        binding.loTraCuuNhapDiemKyMoi.setEnabled(false);
        binding.btnSettings.setEnabled(false);
        binding.loTimNguoiYeu.setEnabled(false);
        binding.loHustVip.setEnabled(false);
        binding.loReviewHocPhan.setEnabled(false);
        binding.txtHoTenSV.setText(getString(R.string.app_name_detail));
        binding.txtLastUpdateTime.setVisibility(View.GONE);
        binding.txtChuY.setVisibility(View.GONE);
        binding.btnUpdate.setText(getString(R.string.thoat));
        Utils.getInstance().showNoticeDialog(getContext(), getString(R.string.chu_y), "Bạn sẽ chỉ sử dụng được tính năng xem Bản Đồ, Cửa Hàng, Giải Trí và xem Web khi ở chế độ không đăng nhập.");
        binding.btnUpdate.setOnClickListener(v -> Utils.getInstance().sendIntentActivity(getContext(), LoginActivity.class));
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loStudentInfo:
                Utils.getInstance().sendIntentActivity(getContext(), ThongTinCaNhanSVActivity.class);
                break;
            case R.id.loDanhSachSV:
                Utils.getInstance().sendIntentActivity(getContext(), DanhSachLopSVActivity.class);
                break;
            case R.id.loDiemThiToeic:
                Utils.getInstance().sendIntentActivity(getContext(), DiemThiToeicActivity.class);
                break;
            case R.id.loTKBTamThoi:
                Utils.getInstance().sendIntentActivity(getContext(), TKBTamThoiActivity.class);
                break;
            case R.id.loLichThi:
                Utils.getInstance().sendIntentActivity(getContext(), LichThiActivity.class);
                break;
            case R.id.loBanDo:
                Utils.getInstance().sendIntentActivity(getContext(), BanDoActivity.class);
                break;
            case R.id.loMucTieuTotNghiep:
                Utils.getInstance().sendIntentActivity(getContext(), MucTieuTotNghiepActivity.class);
                break;
            case R.id.loHocPhi:
                Utils.getInstance().sendIntentActivity(getContext(), HocPhiActivity.class);
                break;
            case R.id.loMonNo:
                Utils.getInstance().sendIntentActivity(getContext(), MonNoActivity.class);
                break;
            case R.id.loHustRanking:
                Utils.getInstance().sendIntentActivity(getContext(), HustRankingActivity.class);
                break;
            case R.id.loTraCuuNhapDiemKyMoi:
                Utils.getInstance().sendIntentActivity(getContext(), KiemTraNhapDiemKiMoiActivity.class);
                break;
            case R.id.loGiaiTri:
                Utils.getInstance().sendIntentActivity(requireContext(), CaroActivity.class);
                break;
            case R.id.loShop:
                Utils.getInstance().sendIntentActivity(getContext(), ShopActivity.class);
                break;
            case R.id.btnUpdate:
                if (Utils.getInstance().isOnline(requireContext())) {
                    showDialogUpdate();
                    binding.btnUpdate.setClickable(false);
                    new AccessToHost().execute();
                } else {
                    Toast.makeText(getContext(), getString(R.string.khong_co_internet), Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnSettings:
                Utils.getInstance().sendIntentActivity(getContext(), SettingsActivity.class);
                break;
            case R.id.loTimNguoiYeu:
                if (Utils.getInstance().isOnline(requireContext())) {
                    Utils.getInstance().sendIntentActivity(getContext(), ThongTinCaNhanTimNguoiYeuActivity.class);
                } else {
                    Toast.makeText(getContext(), R.string.khong_co_internet, Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.loReviewHocPhan:
                Utils.getInstance().sendIntentActivity(getContext(), ReviewHocPhanActivity.class);
                break;
            case R.id.loHustVip:
                Utils.getInstance().sendIntentActivity(getContext(), HustVipActivity.class);
                break;
        }
    }

    private void showDialogUpdate() {
        if (getContext() == null) {
            return;
        }
        dialog = new Dialog(getContext());
        bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_update_data, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(bindingDialog.getRoot());
        bindingDialog.imgCaptcha.setImageBitmap(bitmap);
        bindingDialog.btnCancel.setOnClickListener(v -> dialog.dismiss());

        bindingDialog.imgRefresh.setOnClickListener(v -> {
            if (Utils.getInstance().isOnline(getContext())) {
                new AccessToHost().execute();
            } else {
                Toast.makeText(getContext(), getString(R.string.khong_co_internet), Toast.LENGTH_LONG).show();
            }
        });
        bindingDialog.btnContinues.setOnClickListener(v -> {
            if (bindingDialog.edtCaptcha.getText().toString().length() <= 0) {
                Toast.makeText(getContext(), "Điền mã captcha đi thím", Toast.LENGTH_LONG).show();
                return;
            }
            try {
                if (getContext() != null) {
                    String accountData = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.SHARE_PREFERENCES_DATA_USER);
                    if (accountData != null && !accountData.equals("")) {
                        JSONObject accountJsonO = new JSONObject(accountData);
                        String mssv = accountJsonO.getString(JsonUtils.KEY_MA_SV);
                        String password = accountJsonO.getString(JsonUtils.KEY_PASSWORD);
                        String maDangNhap = accountJsonO.getString(JsonUtils.KEY_MA_DANG_NHAP);
                        String loginForm = accountJsonO.getString(JsonUtils.KEY_LOGIN_FORM);
                        //Login
                        HashMap<String, String> accountAndCaptcha = new HashMap<>();
                        accountAndCaptcha.put(Constant.KEY_USERNAME, mssv);
                        accountAndCaptcha.put(Constant.KEY_PASSWORD, password);
                        accountAndCaptcha.put(Constant.KEY_MA_DANG_NHAP, maDangNhap);
                        accountAndCaptcha.put(Constant.KEY_LOGIN_FORM, loginForm);
                        accountAndCaptcha.put(Constant.KEY_CAPTCHA, bindingDialog.edtCaptcha.getText().toString());
                        dialog.dismiss();
                        new Login().execute(accountAndCaptcha);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                dialog.dismiss();
                Utils.getInstance().showNoticeDialog(getContext(), "Lỗi", "Đăng xuất rồi đăng nhập lại để sửa lỗi này nha thím -_-");
                Log.e("RAKAN", "showDialogUpdate: " + e);
            }
        });
        dialog.show();
    }

    private void checkNetworkAndOpenGame() {
        if (Utils.getInstance().isOnline(requireContext())) {
            Toast.makeText(getContext(), "Cần ngắt kết nối mạng để bắt đầu chơi", Toast.LENGTH_LONG).show();
        } else {
            Random random = new Random();
            String url = "https://www.google.com.vn/" + random.nextFloat();
            try {
                Uri uri = Uri.parse("googlechrome://navigate?url=" + url);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                if (getActivity() != null) {
                    if (i.resolveActivity(getActivity().getPackageManager()) == null) {
                        i.setData(Uri.parse(url));
                    }
                    getActivity().startActivity(i);
                }

            } catch (ActivityNotFoundException e) {
                // Chrome is not installed
                Toast.makeText(getContext(), "Cần cài đặt trình duyệt Chrome để bắt đầu chơi", Toast.LENGTH_LONG).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class AccessToHost extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            Connection.Response connection;
            try {
                connection = Jsoup.connect(Constant.URL_LOGIN)
                        .method(Connection.Method.GET)
                        .timeout(15000)
                        .execute();
                if (connection.statusCode() == Constant.STATUS_CODE_OK) {
                    //Save cookies
                    cookiesLogin = new HashMap<>();
                    cookiesLogin.putAll(connection.cookies());
                    Document document = connection.parse();
                    //Get Captcha
                    Element captcha = document.getElementById(Constant.ID_CAPTCHA);
                    InputStream inputStream = new java.net.URL(captcha.absUrl("src")).openStream();
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    //Get form data login
                    String valueFormDataLogin3 = document.select("input[id=__VIEWSTATE]").first().attr("value");
                    String valueFormDataLogin5 = document.select("input[id=__EVENTVALIDATION]").first().attr("value");
                    if (getContext() != null) {
                        Utils.getInstance().saveToSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_FORM_DATA_LOGIN, Constant.KEY_SHARE_PREFERENCES_FORM_DATA_LOGIN_3, valueFormDataLogin3);
                        Utils.getInstance().saveToSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_FORM_DATA_LOGIN, Constant.KEY_SHARE_PREFERENCES_FORM_DATA_LOGIN_5, valueFormDataLogin5);
                    }
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean isConnectOK) {
            super.onPostExecute(isConnectOK);
            binding.btnUpdate.setClickable(true);
            if (isConnectOK) {
                bindingDialog.imgCaptcha.setImageBitmap(bitmap);
            } else {
                Toast.makeText(getContext(), getString(R.string.notice_login_no_internet), Toast.LENGTH_LONG).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class Login extends AsyncTask<HashMap, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getInstance().showLoadingDialog(getContext());
        }

        @Override
        protected Boolean doInBackground(HashMap... accountAndCaptcha) {
            String userName, password, captcha, maDangNhap, loginForm;
            boolean isLoginSuccess = false;
            try {
                JSONObject accountJsonO = new JSONObject(accountAndCaptcha[0]);
                userName = accountJsonO.getString(Constant.KEY_USERNAME);
                password = accountJsonO.getString(Constant.KEY_PASSWORD);
                captcha = accountJsonO.getString(Constant.KEY_CAPTCHA);
                maDangNhap = accountJsonO.getString(Constant.KEY_MA_DANG_NHAP);
                loginForm = accountJsonO.getString(Constant.KEY_LOGIN_FORM);

                if (maDangNhap.length() > 0) {
                    switch (loginForm) {
                        case "0":
                            isLoginSuccess = JsoupUtils.getInstance().login(getContext(), userName, password, captcha, maDangNhap, cookiesLogin, 0);
                            break;
                        case "1":
                            isLoginSuccess = JsoupUtils.getInstance().login(getContext(), userName, password, captcha, maDangNhap, cookiesLogin, 1);
                            break;
                        case "2":
                            isLoginSuccess = JsoupUtils.getInstance().login(getContext(), userName, password, captcha, maDangNhap, cookiesLogin, 2);
                            if (!isLoginSuccess) {
                                isLoginSuccess = JsoupUtils.getInstance().login(getContext(), userName, password, captcha, maDangNhap, cookiesLogin, 2);
                            }
                            break;
                        case "3":
                            isLoginSuccess = JsoupUtils.getInstance().login(getContext(), userName, password, captcha, maDangNhap, cookiesLogin, 3);
                            break;
                    }
                }
                if (isLoginSuccess) {
                    Utils.getInstance().sendBroadcastUpdateWidget(getContext());
                    sendDataToTopStudent();
                    Utils.getInstance().saveToSharedPreferences(requireContext(),
                            Constant.SHARE_PREFERENCES_DATA,
                            Constant.KEY_SHARE_PREFERENCES_LAST_TIME_UPDATE_DATA,
                            java.text.DateFormat.getDateTimeInstance().format(new Date()));
                }
            } catch (Exception e) {
                Log.e("RAKAN", "Login: " + e);
                e.printStackTrace();
            }
            return isLoginSuccess;
        }

        @Override
        protected void onPostExecute(Boolean isLoginSuccess) {
            super.onPostExecute(isLoginSuccess);
            if (isLoginSuccess) {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    Utils.getInstance().hideLoadingDialog();
                    Toast.makeText(getContext(), "Cập nhật thành công", Toast.LENGTH_LONG).show();
                    Utils.getInstance().sendIntentActivity(getContext(), MainActivity.class);
                }, 6969);
            } else {
                Toast.makeText(getContext(), "Cập nhật thất bại", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void sendDataToTopStudent() {
        final String mssv, name, cpa;
        String thongTinSV = Utils.getInstance().getValueFromSharedPreferences(requireContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        String bangDiemTongKet = Utils.getInstance().getValueFromSharedPreferences(requireContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_TONG_KET);
        try {
            JSONObject thongTinSVJsonO = new JSONObject(thongTinSV);
            mssv = thongTinSVJsonO.getString(JsonUtils.KEY_MA_SO_SV).trim();
            name = thongTinSVJsonO.getString(JsonUtils.KEY_HO_TEN_SV).trim();
            JSONArray bangDiemTongKetJsonA = new JSONArray(bangDiemTongKet);
            if (bangDiemTongKetJsonA.length() > 0) {
                JSONObject diemTongKetLastJsonO = bangDiemTongKetJsonA.getJSONObject(0);
                if (diemTongKetLastJsonO != null) {
                    cpa = diemTongKetLastJsonO.getString(JsonUtils.KEY_CPA);
                } else {
                    cpa = "0";
                }
            } else {
                cpa = "0";
            }

            String khoaHoc = Utils.getInstance().getKhoaHoc(getContext());

            rankingDatabase.child("k" + khoaHoc).child(mssv).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    HustRankingModel hustRankingModel = snapshot.getValue(HustRankingModel.class);
                    isPrivate = hustRankingModel.isPrivate();
                    startDayTurnOn = hustRankingModel.getStartDayTurnOn();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
            rankingDatabase.child("k" + khoaHoc).child(mssv).setValue(new HustRankingModel(mssv, name, cpa, isPrivate, startDayTurnOn));

        } catch (Exception e) {
            Log.e("RAKAN", "sendDataToTopStudent: " + e);
            e.printStackTrace();
        }
    }
}
