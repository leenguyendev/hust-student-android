package com.leenguyen.huststudent.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.leenguyen.huststudent.Constant;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class JsonUtils {
    private static JsonUtils instance;

    //HP info
    public static final String KEY_MA_HP = "maHP";
    public static final String KEY_HOC_KY = "hocKy";
    public static final String KEY_LOP_HOC = "lopHoc";
    public static final String KEY_TEN_HP = "tenHP";
    public static final String KEY_TIN_CHI_HP = "tinChiHP";
    public static final String KEY_DIEM_QUA_TRINH = "diemQT";
    public static final String KEY_DIEM_THI = "diemThi";
    public static final String KEY_DIEM_TONG_KET = "diemTongKet";
    public static final String KEY_DIEM_CAI_THIEN = "diemCaiThien";
    public static final String KEY_CPA = "cpa";
    public static final String KEY_GPA = "gpa";
    public static final String KEY_TC_QUA = "tinChiQua";
    public static final String KEY_TC_TICH_LUY = "tinChiTichLuy";
    public static final String KEY_TC_NO_DANG_KY = "tinChiNoDangKy";
    public static final String KEY_TC_DANG_KY = "tinChiDangKy";
    public static final String KEY_TRINH_DO = "trinhDo";
    public static final String KEY_MUC_CANH_CAO = "mucCanhCao";
    public static final String KEY_THIEU_DIEM = "thieuDiem";
    public static final String KEY_KHONG_TINH = "khongTinh";
    public static final String KEY_CHUONG_TRINH_DAO_TAO = "chuongTrinhDaoTao";
    public static final String KEY_DU_KIEN_XU_LY_HOC_TAP = "duKiemXuLyHocTap";
    public static final String KEY_XU_LY_CHINH_THUC = "xuLyChinhThuc";

    //User info
    public static final String KEY_HO_TEN_SV = "hoTenSv";
    public static final String KEY_MA_SO_SV = "maSoSV";
    public static final String KEY_NAM_VAO_TRUONG = "namVaoTruong";
    public static final String KEY_BAC_DAO_TAO = "bacDaoTao";
    public static final String KEY_CHUONG_TRINH = "chuongTrinh";
    public static final String KEY_KHOA_VIEN = "khoaVien";
    public static final String KEY_TINH_TRANG_HOC_TAP = "tinhTrangHocTap";
    public static final String KEY_GIOI_TINH = "gioiTinh";
    public static final String KEY_LOP = "lop";
    public static final String KEY_KHOA_HOC = "khoaHoc";
    public static final String KEY_EMAIL_SV = "emailSv";
    public static final String KEY_DAN_TOC = "danToc";
    public static final String KEY_NAM_TOT_NGHIEP_C3 = "namTotNghiepCap3";
    public static final String KEY_DIA_CHI = "diaChi";
    public static final String KEY_SO_CMTND = "soCMTND";
    public static final String KEY_NOI_CAP = "noiCap";
    public static final String KEY_HO_TEN_BO = "hoTenBo";
    public static final String KEY_NAM_SINH_BO = "namSinhBo";
    public static final String KEY_NGHE_NGHIEP_BO = "ngheNhiepBo";
    public static final String KEY_DIEN_THOAI_BO = "dienThoaiBo";
    public static final String KEY_EMAIL_BO = "emailBo";
    public static final String KEY_TON_GIAO = "tonGiao";
    public static final String KEY_TRUONG_THPT = "truongTHPT";
    public static final String KEY_HO_KHAU = "hoKhau";
    public static final String KEY_SO_DIEN_THOAI_SV = "soDienThoaiSV";
    public static final String KEY_HO_TEN_ME = "hoTenMe";
    public static final String KEY_NAM_SINH_ME = "namSinhMe";
    public static final String KEY_NGHE_NGHIEP_ME = "ngheNghiepMe";
    public static final String KEY_DIEN_THOAI_ME = "dienThoaiMe";
    public static final String KEY_EMAIL_ME = "emailMe";

    public static final String KEY_PASSWORD = "password";
    public static final String KEY_MA_DANG_NHAP = "ma_dang_nhap";
    public static final String KEY_LOGIN_FORM = "login_form";

    //SV Info in danh sach lop
    public static final String KEY_MA_SV = "maSV";
    public static final String KEY_HO_SV = "hoSV";
    public static final String KEY_DEM_SV = "demSV";
    public static final String KEY_TEN_SV = "tenSV";
    public static final String KEY_NGAY_SINH_SV = "ngaySinhSV";
    public static final String KEY_LOP_SV = "lopSV";
    public static final String KEY_CHUONG_TRINH_DAO_TAO_SV = "chuongTrinhDaoTaoSV";
    public static final String KEY_TRANG_THAI_HOC_TAP_SV = "trangThaiHocTapSV";

    //Diem thi Toeic info
    public static final String KEY_GHI_CHU = "ghiChu";
    public static final String KEY_NGAY_THI = "ngayThi";
    public static final String KEY_DIEM_NGHE = "diemNghe";
    public static final String KEY_DIEM_DOC = "diemDoc";
    public static final String KEY_DIEM_TONG = "diemTong";

    //Thoi khoa bieu
    public static final String THOI_GIAN = "thoiGian";
    public static final String TUAN_HOC = "tuanHoc";
    public static final String PHONG_HOC = "phongHoc";
    public static final String MA_LOP = "maLop";
    public static final String LOAI_LOP = "loaiLop";
    public static final String NHOM = "nhom";
    public static final String MA_HP = "maHP";
    public static final String TEN_LOP = "tenLop";
    public static final String GHI_CHU = "ghiChu";
    public static final String HINH_THUC_DAY = "hinhThucDay";
    public static final String GIANG_VIEN = "giangVien";
    public static final String LINK_ONLINE = "linkOnline";
    public static final String MA_CODE = "maCode";

    //Thong tin nhap diem ki moi
    public static final String KEY_TRONG_SO_QUA_TRINH = "trongSoQuaTrinh";
    public static final String KEY_TT_DIEM_QUA_TRINH = "thongTinDiemQuaTrinh";
    public static final String KEY_TT_DIEM_THI = "thongTinDiemThi";

    //Thong tin hoc phi
    public static final String KEY_SO_TIN_MOT_TIN_CHI = "soTienMotTinChi";
    public static final String KEY_SO_TIN_CHI_HOC_PHI = "soTinChiHocPhi";
    public static final String KEY_HE_SO_HOC_PHI_LOP = "heSoTinChiHocPhi";
    public static final String KEY_TONG_SO_TIEN_HOC_PHAN = "tongSoTienHocPhan";
    public static final String KEY_TRANG_THAI_DANG_KY = "trangThaiDangKy";
    public static final String KEY_LOAI_DANG_KY = "loaiDangKy";
    public static final String KEY_BANG_HOC_PHI_HOC_PHAN = "bangHocPhiHocPhan";
    public static final String KEY_TONG_SO_TIEN_PHAI_DONG = "tongSoTienPhaiDong";

    //TKB tam thoi
    public static final String KEY_CAC_LOP_DANG_KY = "cacLopDangKy";
    public static final String KEY_TKB_CAC_LOP_DANG_KY = "thoiKhoaBieuCacLopDangKy";

    public static final String KEY_MA_LOP = "maLop";
    public static final String KEY_MA_LOP_KEM = "maLopKem";
    public static final String KEY_TEN_LOP = "tenLop";
    public static final String KEY_LOAI_LOP = "loaiLop";
    public static final String KEY_THONG_TIN_LOP = "thongTinLop";
    public static final String KEY_YEU_CAU = "yeuCau";
    public static final String KEY_TIN_CHI = "tinChi";

    public static final String KEY_THU = "thu";
    public static final String KEY_THOI_GIAN = "thoiGian";
    public static final String KEY_TUAN_HOC = "tuanHoc";
    public static final String KEY_PHONG_HOC = "phongHoc";

    //Lich Thi
    public static final String KEY_VIEN = "vien";
    public static final String KEY_MA_HOC_PHAN = "maHocPhan";
    public static final String KEY_TEN_HOC_PHAN = "tenHocPhan";
    public static final String KEY_NHOM = "nhom";
    public static final String KEY_DOT_MO = "dotMo";
    public static final String KEY_TUAN = "tuan";
    public static final String KEY_KIP_THI = "kipThi";
    public static final String KEY_SO_LUONG_DANG_KY = "soLuongDangKy";
    public static final String KEY_PHONG_THI = "phongThi";


    static JsonUtils getInstance() {
        if (instance == null) {
            instance = new JsonUtils();
        }
        return instance;
    }

    void parseThongTinSV(Context context, Document document) {
        try {
            Elements elements = document.select("div[class=row]");

            Element element1 = elements.get(1);

            Element maSVElement = element1.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_lbMSSV");
            String maSoSV = maSVElement.text().substring(5);

            Element thongTinSVRow1 = element1.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_lbTextInfo1");
            String[] contentThongTinSVRow1 = thongTinSVRow1.text().split(":");
            String hoTenSv = contentThongTinSVRow1[1].substring(0, contentThongTinSVRow1[1].length() - 16);
            String namVaoTruong = contentThongTinSVRow1[2].substring(0, contentThongTinSVRow1[2].length() - 13);
            String bacDaoTao = contentThongTinSVRow1[3].substring(0, contentThongTinSVRow1[3].length() - 14);
            String chuongTrinh = contentThongTinSVRow1[4].substring(0, contentThongTinSVRow1[4].length() - 19);
            String khoaVien = contentThongTinSVRow1[5].substring(0, contentThongTinSVRow1[5].length() - 20);
            String tinhTrangHocTap = contentThongTinSVRow1[6].substring(1);

            Element thongTinSVRow2 = element1.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_lbTextInfo2");
            String[] contentThongTinSVRow2 = thongTinSVRow2.text().split(":");
            String gioiTinh = contentThongTinSVRow2[1].substring(0, contentThongTinSVRow2[1].length() - 5);
            String lop = contentThongTinSVRow2[2].substring(0, contentThongTinSVRow2[2].length() - 10);
            String khoaHoc = contentThongTinSVRow2[3].substring(0, contentThongTinSVRow2[3].length() - 7);
            String email = contentThongTinSVRow2[4];

            Element element5 = elements.get(5);
            Element thongTinSVRow3 = element5.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_lbTextInfo5");
            String[] contentThongTinSVRow3 = thongTinSVRow3.text().split(":");
            String danToc = contentThongTinSVRow3[1].substring(0, contentThongTinSVRow3[1].length() - 14);
            String namTotNghiepCap3 = contentThongTinSVRow3[2].substring(0, contentThongTinSVRow3[2].length() - 9);
            String diaChi = contentThongTinSVRow3[3].substring(0, contentThongTinSVRow3[3].length() - 10);
            String soCMTND = contentThongTinSVRow3[4].substring(0, contentThongTinSVRow3[4].length() - 9);
            String noiCap = contentThongTinSVRow3[5].substring(0, contentThongTinSVRow3[5].length() - 11);
            String hoTenBo = contentThongTinSVRow3[6].substring(0, contentThongTinSVRow3[6].length() - 10);
            String namSinhBo = contentThongTinSVRow3[7].substring(0, contentThongTinSVRow3[7].length() - 13);
            String ngheNhiepBo = contentThongTinSVRow3[8].substring(0, contentThongTinSVRow3[8].length() - 12);
            String dienThoaiBo = contentThongTinSVRow3[9].substring(0, contentThongTinSVRow3[9].length() - 7);
            String emailBo = contentThongTinSVRow3[10];

            Element thongTinSVRow4 = element5.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_lbTextInfo6");
            String[] contentThongTinSVRow4 = thongTinSVRow4.text().split(":");
            String tonGiao = contentThongTinSVRow4[1].substring(0, contentThongTinSVRow4[1].length() - 13);
            String truongTHPT = contentThongTinSVRow4[2].substring(0, contentThongTinSVRow4[2].length() - 9);
            String hoKhau = contentThongTinSVRow4[3].substring(0, contentThongTinSVRow4[3].length() - 15);
            String soDienThoaiSV = contentThongTinSVRow4[4].substring(0, contentThongTinSVRow4[4].length() - 11);
            String hoTenMe = contentThongTinSVRow4[5].substring(0, contentThongTinSVRow4[5].length() - 10);
            String namSinhMe = contentThongTinSVRow4[6].substring(0, contentThongTinSVRow4[6].length() - 13);
            String ngheNghiepMe = contentThongTinSVRow4[7].substring(0, contentThongTinSVRow4[7].length() - 12);
            String dienThoaiMe = contentThongTinSVRow4[8].substring(0, contentThongTinSVRow4[8].length() - 7);
            String emailMe = contentThongTinSVRow4[9];

            JSONObject thongTinSVJsonO = new JSONObject();
            thongTinSVJsonO.put(KEY_HO_TEN_SV, hoTenSv);
            thongTinSVJsonO.put(KEY_MA_SO_SV, maSoSV);
            thongTinSVJsonO.put(KEY_NAM_VAO_TRUONG, namVaoTruong);
            thongTinSVJsonO.put(KEY_BAC_DAO_TAO, bacDaoTao);
            thongTinSVJsonO.put(KEY_CHUONG_TRINH, chuongTrinh);
            thongTinSVJsonO.put(KEY_KHOA_VIEN, khoaVien);
            thongTinSVJsonO.put(KEY_TINH_TRANG_HOC_TAP, tinhTrangHocTap);
            thongTinSVJsonO.put(KEY_GIOI_TINH, gioiTinh);
            thongTinSVJsonO.put(KEY_LOP, lop);
            thongTinSVJsonO.put(KEY_KHOA_HOC, khoaHoc);
            thongTinSVJsonO.put(KEY_EMAIL_SV, email);
            thongTinSVJsonO.put(KEY_DAN_TOC, danToc);
            thongTinSVJsonO.put(KEY_NAM_TOT_NGHIEP_C3, namTotNghiepCap3);
            thongTinSVJsonO.put(KEY_DIA_CHI, diaChi);
            thongTinSVJsonO.put(KEY_SO_CMTND, soCMTND);
            thongTinSVJsonO.put(KEY_NOI_CAP, noiCap);
            thongTinSVJsonO.put(KEY_HO_TEN_BO, hoTenBo);
            thongTinSVJsonO.put(KEY_NAM_SINH_BO, namSinhBo);
            thongTinSVJsonO.put(KEY_NGHE_NGHIEP_BO, ngheNhiepBo);
            thongTinSVJsonO.put(KEY_DIEN_THOAI_BO, dienThoaiBo);
            thongTinSVJsonO.put(KEY_EMAIL_BO, emailBo);
            thongTinSVJsonO.put(KEY_TON_GIAO, tonGiao);
            thongTinSVJsonO.put(KEY_TRUONG_THPT, truongTHPT);
            thongTinSVJsonO.put(KEY_HO_KHAU, hoKhau);
            thongTinSVJsonO.put(KEY_SO_DIEN_THOAI_SV, soDienThoaiSV);
            thongTinSVJsonO.put(KEY_HO_TEN_ME, hoTenMe);
            thongTinSVJsonO.put(KEY_NAM_SINH_ME, namSinhMe);
            thongTinSVJsonO.put(KEY_NGHE_NGHIEP_ME, ngheNghiepMe);
            thongTinSVJsonO.put(KEY_DIEN_THOAI_ME, dienThoaiMe);
            thongTinSVJsonO.put(KEY_EMAIL_ME, emailMe);
            Utils.getInstance().saveToSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN, thongTinSVJsonO.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RAKAN", "parseThongTinSV: " + e.toString());
            Toast.makeText(context, "Không tải được thông tin sv", Toast.LENGTH_SHORT).show();
        }
    }

    void parseTKBTamThoi(Context context, Document document) {
        try {
            JSONObject dataTKBTamThoiJsonO = new JSONObject();
            JSONArray dataCacLopDangKyJsonA = new JSONArray();
            JSONArray dataTKBCacLopDangKyJsonA = new JSONArray();
            Element tableCacLopDangKy = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_gvRegisteredList_DXMainTable");
            Element tableTKBCacLopDangKy = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_gvTimeTable_DXMainTable");

            //Parse thong tin cac lop dang ky
            Elements lopDangKyList = tableCacLopDangKy.getElementsByClass("dxgvDataRow_Mulberry");
            for (Element element : lopDangKyList) {
                String maLop = element.select("td.dxgv").first().text();
                String maLopKem = element.select("td.dxgv").get(1).text();
                String tenLop = element.select("td.dxgv").get(2).text();
                String maHP = element.select("td.dxgv").get(3).text();
                String loaiLop = element.select("td.dxgv").get(4).text();
                String thongTinLop = element.select("td.dxgv").get(5).text();
                String yeuCau = element.select("td.dxgv").get(6).text();
                String trangThaiDangKy = element.select("td.dxgv").get(7).text();
                String loaiDangKy = element.select("td.dxgv").get(8).text();
                String tinChi = element.select("td.dxgv").get(9).text();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(KEY_MA_LOP, maLop);
                jsonObject.put(KEY_MA_LOP_KEM, maLopKem);
                jsonObject.put(KEY_TEN_LOP, tenLop);
                jsonObject.put(KEY_MA_HP, maHP);
                jsonObject.put(KEY_LOAI_LOP, loaiLop);
                jsonObject.put(KEY_THONG_TIN_LOP, thongTinLop);
                jsonObject.put(KEY_YEU_CAU, yeuCau);
                jsonObject.put(KEY_TRANG_THAI_DANG_KY, trangThaiDangKy);
                jsonObject.put(KEY_LOAI_DANG_KY, loaiDangKy);
                jsonObject.put(KEY_TIN_CHI, tinChi);
                dataCacLopDangKyJsonA.put(jsonObject);
            }

            //Parse thong tin thoi khoa bieu cac lop dang ky
            Elements tkbLopDangKyList = tableTKBCacLopDangKy.getElementsByClass("dxgvDataRow_Mulberry");
            for (Element element : tkbLopDangKyList) {
                String thu = element.select("td.dxgv").first().text();
                String thoiGian = element.select("td.dxgv").get(1).text();
                String tuanHoc = element.select("td.dxgv").get(2).text();
                String phongHoc = element.select("td.dxgv").get(3).text();
                String lop = element.select("td.dxgv").get(4).text();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(KEY_THU, thu);
                jsonObject.put(KEY_THOI_GIAN, thoiGian);
                jsonObject.put(KEY_TUAN_HOC, tuanHoc);
                jsonObject.put(KEY_PHONG_HOC, phongHoc);
                jsonObject.put(KEY_LOP_HOC, lop);
                dataTKBCacLopDangKyJsonA.put(jsonObject);
            }
            dataTKBTamThoiJsonO.put(KEY_CAC_LOP_DANG_KY, dataCacLopDangKyJsonA);
            dataTKBTamThoiJsonO.put(KEY_TKB_CAC_LOP_DANG_KY, dataTKBCacLopDangKyJsonA);

            Utils.getInstance().saveToSharedPreferences(context,
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_TKB_TAM_THOI,
                    dataTKBTamThoiJsonO.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RAKAN", "parseTKBTamThoi: " + e.toString());
            Toast.makeText(context, "Không tải được thông tin tkb tạm thời", Toast.LENGTH_SHORT).show();
        }
    }

    void parseThongTinHocPhi(Context context, Document document) {
        try {
            JSONObject dataThongTinHocPhiJson0 = new JSONObject();
            Element tableBangHocPhiHocPhan = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_rpEditTables_ASPxCallbackPanel1_gvCourseRegister_DXMainTable");
            Element thongTinHocPhi = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_rpEditTables_ASPxCallbackPanel1_TuitionInfo");
            Element rowSoTienConPhaiDong = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_rpEditTables_ASPxCallbackPanel1_gvTuitionTerm_DXFooterRow");
            //Parse bang hoc phi hoc phan
            JSONArray bangHocPhiHocPhanJsonA = new JSONArray();
            Elements bangHocPhi = tableBangHocPhiHocPhan.getElementsByClass("dxgvDataRow_Mulberry");
            for (Element element : bangHocPhi) {
                String maHocPhan = element.select("td.dxgv").first().text();
                String tenHocPhan = element.select("td.dxgv").get(1).text();
                String soTienMotTinChi = element.select("td.dxgv").get(2).text();
                String soTinChiHocPhi = element.select("td.dxgv").get(3).text();
                String heSoHocPhiLop = element.select("td.dxgv").get(4).text();
                String tongSoTienHocPhan = element.select("td.dxgv").get(5).text();
                String trangThaiDangKy = element.select("td.dxgv").get(6).text();
                String loaiDangKy = element.select("td.dxgv").get(7).text();
                String ghiChu = element.select("td.dxgv").get(8).text();
                JSONObject hocPhiHocPhanJsonO = new JSONObject();
                hocPhiHocPhanJsonO.put(KEY_MA_HP, maHocPhan);
                hocPhiHocPhanJsonO.put(KEY_TEN_HP, tenHocPhan);
                hocPhiHocPhanJsonO.put(KEY_SO_TIN_MOT_TIN_CHI, soTienMotTinChi);
                hocPhiHocPhanJsonO.put(KEY_SO_TIN_CHI_HOC_PHI, soTinChiHocPhi);
                hocPhiHocPhanJsonO.put(KEY_HE_SO_HOC_PHI_LOP, heSoHocPhiLop);
                hocPhiHocPhanJsonO.put(KEY_TONG_SO_TIEN_HOC_PHAN, tongSoTienHocPhan);
                hocPhiHocPhanJsonO.put(KEY_TRANG_THAI_DANG_KY, trangThaiDangKy);
                hocPhiHocPhanJsonO.put(KEY_LOAI_DANG_KY, loaiDangKy);
                hocPhiHocPhanJsonO.put(KEY_GHI_CHU, ghiChu);
                bangHocPhiHocPhanJsonA.put(hocPhiHocPhanJsonO);
            }
            dataThongTinHocPhiJson0.put(KEY_BANG_HOC_PHI_HOC_PHAN, bangHocPhiHocPhanJsonA);

            //Parse thong tin hoc phi
//            String tongSoTienPhaiDong = thongTinHocPhi.select("li").first().select("span").text().substring(23).replace("đ.", "");
            String soTienConPhaiDong = rowSoTienConPhaiDong.select("td.dxgv").get(3).text().substring(15).replace(" đ", "");
            String ghiChu = thongTinHocPhi.select("li").select("span").get(1).text().replace("tại đây", "tại trang web https://ctt-sis.hust.edu.vn");
            dataThongTinHocPhiJson0.put(KEY_TONG_SO_TIEN_PHAI_DONG, soTienConPhaiDong);
            dataThongTinHocPhiJson0.put(KEY_GHI_CHU, ghiChu);

            Utils.getInstance().saveToSharedPreferences(context,
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_HOC_PHI,
                    dataThongTinHocPhiJson0.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RAKAN", "parseThongTinHocPhi: " + e.toString());
            Toast.makeText(context, "Không tải được thông tin học phí", Toast.LENGTH_SHORT).show();
        }
    }

    void parseTKB(Context context, Document document) {
        try {
            JSONArray dataTkbJsonA = new JSONArray();
            Element table = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_gvStudentRegister_DXMainTable");
            Elements listTkb = table.getElementsByClass("dxgvDataRow_Mulberry");
            for (Element element : listTkb) {
                String thoiGian = element.select("td.dxgv").first().text();
                String tuanHoc = element.select("td.dxgv").get(1).text();
                String phongHoc = element.select("td.dxgv").get(2).text();
                String maLop = element.select("td.dxgv").get(3).text();
                String loaiLop = element.select("td.dxgv").get(4).text();
                String nhom = element.select("td.dxgv").get(5).text();
                String maHP = element.select("td.dxgv").get(6).text();
                String tenLop = element.select("td.dxgv").get(7).text();
                String ghiChu = element.select("td.dxgv").get(8).text();
                String hinhThucDay = element.select("td.dxgv").get(9).text();
                String giangVien = element.select("td.dxgv").get(10).text();
                String linkOnline = element.select("td.dxgv").get(11).text();
                String maCode = element.select("td.dxgv").get(12).text();
                JSONObject tkbJsonO = new JSONObject();
                tkbJsonO.put(THOI_GIAN, thoiGian);
                tkbJsonO.put(TUAN_HOC, tuanHoc);
                tkbJsonO.put(PHONG_HOC, phongHoc);
                tkbJsonO.put(MA_LOP, maLop);
                tkbJsonO.put(LOAI_LOP, loaiLop);
                tkbJsonO.put(NHOM, nhom);
                tkbJsonO.put(MA_HP, maHP);
                tkbJsonO.put(TEN_LOP, tenLop);
                tkbJsonO.put(GHI_CHU, ghiChu);
                tkbJsonO.put(HINH_THUC_DAY, hinhThucDay);
                tkbJsonO.put(GIANG_VIEN, giangVien);
                tkbJsonO.put(LINK_ONLINE, linkOnline);
                tkbJsonO.put(MA_CODE, maCode);
                dataTkbJsonA.put(tkbJsonO);
            }
            Utils.getInstance().saveToSharedPreferences(context,
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_TKB,
                    dataTkbJsonA.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RAKAN", "parseTKB: " + e.toString());
            Toast.makeText(context, "Không tải được thông tin TKB", Toast.LENGTH_SHORT).show();
        }
    }

    void parseDiemThiToeic(Context context, Document document) {
        try {
            JSONArray dataDiemThiToeicJsonA = new JSONArray();
            Element table = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_gvStudents");
            Elements danhSachDiemThiToeic = table.getElementsByClass("dxgvDataRow");
            for (Element element : danhSachDiemThiToeic) {
                String maSV = element.select("td.dxgv").first().text();
                String hoTen = element.select("td.dxgv").get(1).text();
                String ngaySinh = element.select("td.dxgv").get(2).text();
                String hocKy = element.select("td.dxgv").get(3).text();
                String ghiChu = element.select("td.dxgv").get(4).text();
                String ngayThi = element.select("td.dxgv").get(5).text();
                String diemNghe = element.select("td.dxgv").get(6).text();
                String diemDoc = element.select("td.dxgv").get(7).text();
                String diemTong = element.select("td.dxgv").get(8).text();
                JSONObject object = new JSONObject();
                object.put(KEY_MA_SV, maSV);
                object.put(KEY_HO_TEN_SV, hoTen);
                object.put(KEY_NGAY_SINH_SV, ngaySinh);
                object.put(KEY_HOC_KY, hocKy);
                object.put(KEY_GHI_CHU, ghiChu);
                object.put(KEY_NGAY_THI, ngayThi);
                object.put(KEY_DIEM_NGHE, diemNghe);
                object.put(KEY_DIEM_DOC, diemDoc);
                object.put(KEY_DIEM_TONG, diemTong);
                dataDiemThiToeicJsonA.put(object);
            }
            Utils.getInstance().saveToSharedPreferences(context,
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_DIEM_THI_TOEIC,
                    dataDiemThiToeicJsonA.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Không tải được thông tin điểm thi toeic", Toast.LENGTH_SHORT).show();
            Log.e("RAKAN", "parseDiemThiToeic: " + e.toString());
        }
    }

    void parseThongTinLopSV(Context context, Document document) {
        try {
            JSONArray thongTinLopSvJsonA = new JSONArray();
            Element table = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_gvStudents_DXMainTable");
            Elements danhSachSV = table.getElementsByClass("dxgvDataRow");
            for (Element element : danhSachSV) {
                String maSV = element.select("td.dxgv").first().text();
                String hoSV = element.select("td.dxgv").get(1).text();
                String demSV = element.select("td.dxgv").get(2).text();
                String tenSV = element.select("td.dxgv").get(3).text();
                String ngaySinhSV = element.select("td.dxgv").get(4).text();
                String lopSV = element.select("td.dxgv").get(5).text();
                String chuongTrinhDaoTaoSV = element.select("td.dxgv").get(6).text();
                String trangThaiHocTapSV = element.select("td.dxgv").get(7).text();
                try {
                    JSONObject svJsonO = new JSONObject();
                    svJsonO.put(KEY_MA_SV, maSV);
                    svJsonO.put(KEY_HO_SV, hoSV);
                    svJsonO.put(KEY_DEM_SV, demSV);
                    svJsonO.put(KEY_TEN_SV, tenSV);
                    svJsonO.put(KEY_NGAY_SINH_SV, ngaySinhSV);
                    svJsonO.put(KEY_LOP_SV, lopSV);
                    svJsonO.put(KEY_CHUONG_TRINH_DAO_TAO_SV, chuongTrinhDaoTaoSV);
                    svJsonO.put(KEY_TRANG_THAI_HOC_TAP_SV, trangThaiHocTapSV);
                    thongTinLopSvJsonA.put(svJsonO);
                } catch (Exception e) {
                    Log.e("RAKAN", "parseThongTinLopSV1: " + e.toString());
                }
            }
            Utils.getInstance().saveToSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_DANH_SACH_LOP_SV, thongTinLopSvJsonA.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Không tải được thông tin lớp sv", Toast.LENGTH_SHORT).show();
            Log.e("RAKAN", "parseThongTinLopSV2: " + e.toString());
        }
    }

    void parseBangDiemHocPhan(Context context, Document document) {
        try {
            String bangHPKhongTinhDiem = Utils.getInstance().getValueFromSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_KHONG_TINH_DIEM);
            JSONArray bangHPKhongTinhDiemJsonA = null;
            if (bangHPKhongTinhDiem != null && !bangHPKhongTinhDiem.equals("")) {
                bangHPKhongTinhDiemJsonA = new JSONArray(bangHPKhongTinhDiem);
            }
            JSONArray bangDiemHocPhanJsonA = new JSONArray();
            Element element = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_gvCourseMarks_DXMainTable");
            Elements elements = element.getElementsByClass("dxgvDataRow");
            for (Element elementHP : elements) {
                String hocKy = elementHP.select("td.dxgv").first().text();
                String maHP = elementHP.select("td.dxgv").get(1).text();
                String tenHP = elementHP.select("td.dxgv").get(2).text();
                String tinChiHP = elementHP.select("td.dxgv").get(3).text();
                String diemHP = elementHP.select("td.dxgv").get(4).text();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject();
                    jsonObject.put(KEY_HOC_KY, hocKy);
                    jsonObject.put(KEY_MA_HP, maHP);
                    jsonObject.put(KEY_TEN_HP, tenHP);
                    jsonObject.put(KEY_TIN_CHI_HP, tinChiHP);
                    jsonObject.put(KEY_DIEM_TONG_KET, diemHP);
                } catch (Exception e) {
                    Log.e("RAKAN", "parseBangDiemHocPhan1: " + e.toString());
                    e.printStackTrace();
                }
                if (bangHPKhongTinhDiemJsonA != null && bangHPKhongTinhDiemJsonA.length() > 0) {
                    for (int j = 0; j < bangHPKhongTinhDiemJsonA.length(); j++) {
                        JSONObject hpKhongTinhDiemJsonO = bangHPKhongTinhDiemJsonA.getJSONObject(j);
                        String maHPKhongTinhDiem = hpKhongTinhDiemJsonO.getString(KEY_MA_HP);
                        if(maHPKhongTinhDiem.equals(maHP)){
                            break;
                        } else {
                            bangDiemHocPhanJsonA.put(jsonObject);
                        }
                    }
                } else {
                    bangDiemHocPhanJsonA.put(jsonObject);
                }
            }
            Utils.getInstance().saveToSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN, bangDiemHocPhanJsonA.toString());
        } catch (Exception e) {
            Log.e("RAKAN", "parseBangDiemHocPhan2: " + e.toString());
            e.printStackTrace();
            Toast.makeText(context, "Không tải được thông tin bảng điểm học phần", Toast.LENGTH_SHORT).show();
        }
    }

    void parseBangDiemCaNhan(Context context, Document document) {
        try {
            JSONArray bangDiemCaNhanJsonA = new JSONArray();
            Element element = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_gvCourseMarks_DXMainTable");
            Elements elements = element.getElementsByClass("dxgvDataRow");
            for (Element elementHP : elements) {
                String hocKy = elementHP.select("td.dxgv").first().text();
                String maHP = elementHP.select("td.dxgv").get(1).text();
                String tenHP = elementHP.select("td.dxgv").get(2).text();
                String tinChiHP = elementHP.select("td.dxgv").get(3).text();
                String lopHocHP = elementHP.select("td.dxgv").get(4).text();
                String diemQuaTrinhHP = elementHP.select("td.dxgv").get(5).text();
                String diemThiHP = elementHP.select("td.dxgv").get(6).text();
                String diemChuHP = elementHP.select("td.dxgv").get(7).text();

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(KEY_HOC_KY, hocKy);
                jsonObject.put(KEY_MA_HP, maHP);
                jsonObject.put(KEY_TEN_HP, tenHP);
                jsonObject.put(KEY_TIN_CHI_HP, tinChiHP);
                jsonObject.put(KEY_LOP_HOC, lopHocHP);
                jsonObject.put(KEY_DIEM_QUA_TRINH, diemQuaTrinhHP);
                jsonObject.put(KEY_DIEM_THI, diemThiHP);
                jsonObject.put(KEY_DIEM_TONG_KET, diemChuHP);
                bangDiemCaNhanJsonA.put(jsonObject);

            }
            Utils.getInstance().saveToSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_CA_NHAN, bangDiemCaNhanJsonA.toString());
        } catch (Exception e) {
            Log.e("RAKAN", "parseBangDiemCaNhan: " + e.toString());
        }

        try {
            JSONArray bangDiemTongKetJsonA = new JSONArray();
            Element diemTongKet = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_gvResults_DXMainTable");
            Elements diemTongKets = diemTongKet.getElementsByClass("dxgvDataRow");
            for (Element element1 : diemTongKets) {
                String hocKy = element1.select("td.dxgv").first().text();
                String gpa = element1.select("td.dxgv").get(1).text();
                String cpa = element1.select("td.dxgv").get(2).text();
                String tinChiQua = element1.select("td.dxgv").get(3).text();
                String tinChiTichLuy = element1.select("td.dxgv").get(4).text();
                String tinChiNoDangKy = element1.select("td.dxgv").get(5).text();
                String tinChiDangKy = element1.select("td.dxgv").get(6).text();
                String trinhDo = element1.select("td.dxgv").get(7).text();
                String mucCanhCao = element1.select("td.dxgv").get(8).text();
                String thieuDiem = element1.select("td.dxgv").get(9).text();
                String khongTinh = element1.select("td.dxgv").get(10).text();
                String chuongTrinhDaoTao = element1.select("td.dxgv").get(11).text();
                String duKiemXuLyHocTap = element1.select("td.dxgv").get(12).text();
                String xuLyChinhThuc = element1.select("td.dxgv").get(13).text();

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(KEY_HOC_KY, hocKy);
                jsonObject.put(KEY_GPA, gpa);
                jsonObject.put(KEY_CPA, cpa);
                jsonObject.put(KEY_TC_QUA, tinChiQua);
                jsonObject.put(KEY_TC_TICH_LUY, tinChiTichLuy);
                jsonObject.put(KEY_TC_NO_DANG_KY, tinChiNoDangKy);
                jsonObject.put(KEY_TC_DANG_KY, tinChiDangKy);
                jsonObject.put(KEY_TRINH_DO, trinhDo);
                jsonObject.put(KEY_MUC_CANH_CAO, mucCanhCao);
                jsonObject.put(KEY_THIEU_DIEM, thieuDiem);
                jsonObject.put(KEY_KHONG_TINH, khongTinh);
                jsonObject.put(KEY_CHUONG_TRINH_DAO_TAO, chuongTrinhDaoTao);
                jsonObject.put(KEY_DU_KIEN_XU_LY_HOC_TAP, duKiemXuLyHocTap);
                jsonObject.put(KEY_XU_LY_CHINH_THUC, xuLyChinhThuc);
                bangDiemTongKetJsonA.put(jsonObject);
            }
            Utils.getInstance().saveToSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_TONG_KET, bangDiemTongKetJsonA.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Không tải được thông tin bảng điểm cá nhân", Toast.LENGTH_SHORT).show();
            Log.e("RAKAN", "parseBangDiemCaNhan: " + e.toString());
        }
    }

    void parseThongTinNhapDiemMoiNhat(Context context, Document document) {
        try {
            JSONArray bangDiemMoiJsonA = new JSONArray();
            Element element = document.getElementById("ctl00_ctl00_contentPane_MainPanel_MainContent_panelGradeTable_gvClassGrade_DXMainTable");
            Elements elements = element.getElementsByClass("dxgvDataRow");
            for (Element elementHP : elements) {
                String mssv = elementHP.select("td.dxgv").first().text();
                String maLop = elementHP.select("td.dxgv").get(1).text();
                String tenLop = elementHP.select("td.dxgv").get(2).text();
                String trongSoQuaTrinh = elementHP.select("td.dxgv").get(3).text();
                String diemQuaTrinh = elementHP.select("td.dxgv").get(4).text();
                String thongTinDiemQuaTrinh = elementHP.select("td.dxgv").get(5).text();
                String diemThi = elementHP.select("td.dxgv").get(6).text();
                String thongTinDiemThi = elementHP.select("td.dxgv").get(7).text();

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(KEY_MA_SO_SV, mssv);
                jsonObject.put(MA_LOP, maLop);
                jsonObject.put(TEN_LOP, tenLop);
                jsonObject.put(KEY_TRONG_SO_QUA_TRINH, trongSoQuaTrinh);
                jsonObject.put(KEY_DIEM_QUA_TRINH, diemQuaTrinh);
                jsonObject.put(KEY_TT_DIEM_QUA_TRINH, thongTinDiemQuaTrinh);
                jsonObject.put(KEY_DIEM_THI, diemThi);
                jsonObject.put(KEY_TT_DIEM_THI, thongTinDiemThi);
                bangDiemMoiJsonA.put(jsonObject);
            }
            Utils.getInstance().saveToSharedPreferences(context, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_KIEM_TRA_NHAP_DIEM_KI_MOI, bangDiemMoiJsonA.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Không tải được thông tin nhập điểm", Toast.LENGTH_SHORT).show();
            Log.e("RAKAN", "parseThongTinNhapDiemMoiNhat: " + e.toString());
        }
    }
}
