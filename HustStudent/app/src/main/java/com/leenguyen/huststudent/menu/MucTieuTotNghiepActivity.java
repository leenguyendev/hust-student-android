package com.leenguyen.huststudent.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.BangDiemAdapter;
import com.leenguyen.huststudent.databinding.ActivityMucTieuTotNghiepBinding;
import com.leenguyen.huststudent.model.HocPhanModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MucTieuTotNghiepActivity extends AppCompatActivity {
    private ActivityMucTieuTotNghiepBinding binding;
    private ArrayList<HocPhanModel> hocPhanModels;
    private double tongDiemHienTai;
    private int soTinChiHienTai;
    private final String[] listDiemChu = {"F", "D", "D+", "C", "C+", "B", "B+", "A", "A+"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_muc_tieu_tot_nghiep);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        boolean isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVipMember && !isNoAds) {
            initAdsBanner();
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            binding.loAds.setLayoutParams(layoutParams);
        }
        initLayout();
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    private void initLayout() {
        binding.btnBack.setOnClickListener(view -> onBackPressed());
        binding.rdgMucTieu.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.cbXuatSac:
                case R.id.cbGioi:
                case R.id.cbKha:
                case R.id.cbTrungBinh:
                    startTinhToanMucTieu();
                    break;
                case R.id.cbLoveBKForever:
                    Glide.with(getApplicationContext()).load(R.drawable.ic_ami_luom).into(binding.imgSticker);
                    binding.txtNhanXet.setText("Really?");
            }
        });
        Glide.with(this).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
        binding.txtNhanXet.setText(getString(R.string.muc_tieu_tot_nghiep_guide));
        showListHPSeHocVaCaiThien();
    }

    private void showListHPSeHocVaCaiThien() {
        if (hocPhanModels != null) {
            hocPhanModels = null;
        }
        hocPhanModels = new ArrayList<>();
        try {
            String bangHPNew = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI);
            if (bangHPNew != null && !bangHPNew.equals("")) {
                JSONArray bangHPNewJsonA = new JSONArray(bangHPNew);
                if (bangHPNewJsonA.length() > 0) {
                    for (int i = bangHPNewJsonA.length() - 1; i >= 0; i--) {
                        JSONObject hPNewJsonO = bangHPNewJsonA.getJSONObject(i);
                        String maHP = hPNewJsonO.getString(JsonUtils.KEY_MA_HP);
                        String tenHP = hPNewJsonO.getString(JsonUtils.KEY_TEN_HP);
                        String soTC = hPNewJsonO.getString(JsonUtils.KEY_TIN_CHI_HP);
                        String diemTongKet = hPNewJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        HocPhanModel hocPhanModel = new HocPhanModel(maHP, tenHP, null, null, null, null, soTC, diemTongKet, null, false, true);
                        hocPhanModels.add(hocPhanModel);
                    }
                }
            }

            String bangHPCaiThien = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN);
            if (bangHPCaiThien != null && !bangHPCaiThien.equals("")) {
                JSONArray bangHPCaiThienJsonA = new JSONArray(bangHPCaiThien);
                if (bangHPCaiThienJsonA.length() > 0) {
                    for (int i = bangHPCaiThienJsonA.length() - 1; i >= 0; i--) {
                        JSONObject hPCaiThienJsonO = bangHPCaiThienJsonA.getJSONObject(i);
                        String maHP = hPCaiThienJsonO.getString(JsonUtils.KEY_MA_HP);
                        String tenHP = hPCaiThienJsonO.getString(JsonUtils.KEY_TEN_HP);
                        String soTC = hPCaiThienJsonO.getString(JsonUtils.KEY_TIN_CHI_HP);
                        String diemTongKet = hPCaiThienJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        String diemCaiThien = hPCaiThienJsonO.getString(JsonUtils.KEY_DIEM_CAI_THIEN);
                        HocPhanModel hocPhanModel = new HocPhanModel(maHP, tenHP, null, null, null, null, soTC, diemTongKet, diemCaiThien, true, false);
                        hocPhanModels.add(hocPhanModel);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (hocPhanModels.size() > 0) {
            binding.txtKhongCoHP.setVisibility(View.GONE);
            BangDiemAdapter bangDiemAdapter = new BangDiemAdapter(getApplicationContext(), R.layout.item_bang_diem_hoc_phan, hocPhanModels);
            binding.lvHP.setAdapter(bangDiemAdapter);
        } else {
            binding.txtKhongCoHP.setVisibility(View.VISIBLE);
        }
    }

    @SuppressLint("SetTextI18n")
    private void startTinhToanMucTieu() {
        setValueSoTinChiVaTongSoDiemTinhCaHocPhanSeHoc();

        int soTCMucTieu = binding.pkSoTinChi.getValue();
        double cpaMucTieu = 0;
        double tongDiemConLai = 0;
        double cpaConLai = 0;
        int soTCConLai = 0;
        if (binding.cbXuatSac.isChecked()) {
            cpaMucTieu = 3.6;
        } else if (binding.cbGioi.isChecked()) {
            cpaMucTieu = 3.2;
        } else if (binding.cbKha.isChecked()) {
            cpaMucTieu = 2.5;
        } else if (binding.cbTrungBinh.isChecked()) {
            cpaMucTieu = 2.0;
        }
        int soTCTicLuy = soTinChiHienTai;
        double cpaHienTai = tongDiemHienTai / soTinChiHienTai;
        Log.d("RAKAN", "cpaHienTai: " + cpaHienTai);

        if (soTCTicLuy >= soTCMucTieu) {
            Toast.makeText(this, "Tổng số tín chỉ cần cao hơn số tín chỉ tích lũy hiện tại", Toast.LENGTH_LONG).show();
        } else {
            binding.imgSticker.setVisibility(View.VISIBLE);
            binding.txtNhanXet.setVisibility(View.VISIBLE);
            soTCConLai = soTCMucTieu - soTCTicLuy;
            tongDiemConLai = cpaMucTieu * soTCMucTieu - cpaHienTai * soTCTicLuy;
            cpaConLai = tongDiemConLai / soTCConLai;
            Log.d("RAKAN", "cpaConLai: " + cpaConLai);
            if (cpaConLai > 4) {
                Glide.with(this).load(R.drawable.ic_ami_cry_2).into(binding.imgSticker);
                binding.txtNhanXet.setText(R.string.ket_qua_tinh_toan_muc_tieu_can_cai_thien);
            } else if (cpaConLai < 0) {
                Glide.with(this).load(R.drawable.ic_ami_quay_2).into(binding.imgSticker);
                binding.txtNhanXet.setText(R.string.ket_qua_tinh_toan_muc_tieu_chuc_mung);
            } else if (Double.isNaN(cpaConLai)) {//SV mới
                Glide.with(this).load(R.drawable.ic_ami_tin_tuong).into(binding.imgSticker);
                binding.txtNhanXet.setText(getString(R.string.ket_qua_tinh_toan_muc_tieu_cpa_con_lai) + " " + cpaMucTieu);
            } else {
                Glide.with(this).load(R.drawable.ic_ami_tin_tuong).into(binding.imgSticker);
                binding.txtNhanXet.setText(getString(R.string.ket_qua_tinh_toan_muc_tieu_cpa_con_lai) + " " + (double) Math.round(cpaConLai * 100) / 100);
            }
        }
    }

    private void setValueSoTinChiVaTongSoDiemTinhCaHocPhanSeHoc() {
        tongDiemHienTai = 0;
        soTinChiHienTai = 0;
        try {
            String bangHPCaiThien = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN);
            if (bangHPCaiThien != null && !bangHPCaiThien.equals("")) {
                JSONArray bangHPCaiThienJsonA = new JSONArray(bangHPCaiThien);
                if (bangHPCaiThienJsonA.length() > 0) {
                    for (int i = 0; i < bangHPCaiThienJsonA.length(); i++) {
                        JSONObject hPCaiThienJsonO = bangHPCaiThienJsonA.getJSONObject(i);
                        int soTC = Integer.parseInt(hPCaiThienJsonO.getString(JsonUtils.KEY_TIN_CHI_HP));
                        String diemHienTai = hPCaiThienJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        String diemCaiThien = hPCaiThienJsonO.getString(JsonUtils.KEY_DIEM_CAI_THIEN);
                        tongDiemHienTai += soTC * (Utils.getInstance().convertDiemChuToDiemSo(diemCaiThien) - Utils.getInstance().convertDiemChuToDiemSo(diemHienTai));
//                        soTinChiHienTai += soTC;
                    }
                }
            }

            String bangHPNew = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI);
            if (bangHPNew != null && !bangHPNew.equals("")) {
                JSONArray bangHPNewJsonA = new JSONArray(bangHPNew);
                if (bangHPNewJsonA.length() > 0) {
                    for (int i = 0; i < bangHPNewJsonA.length(); i++) {
                        JSONObject hPNewJsonO = bangHPNewJsonA.getJSONObject(i);
                        int soTC = Integer.parseInt(hPNewJsonO.getString(JsonUtils.KEY_TIN_CHI_HP));
                        String diem = hPNewJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        soTinChiHienTai += soTC;
                        tongDiemHienTai += soTC * Utils.getInstance().convertDiemChuToDiemSo(diem);
                    }
                }
            }

            String bangDiemHP = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN);
            if (bangDiemHP != null && !bangDiemHP.equals("")) {
                JSONArray bangDiemHPJsonA = new JSONArray(bangDiemHP);
                if (bangDiemHPJsonA.length() > 0) {
                    for (int i = 0; i < bangDiemHPJsonA.length(); i++) {
                        JSONObject diemHPJsonO = bangDiemHPJsonA.getJSONObject(i);
                        int soTC = Integer.parseInt(diemHPJsonO.getString(JsonUtils.KEY_TIN_CHI_HP));
                        String diem = diemHPJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                        //fix issue bảng điểm có điểm lạ I, X, R, W => k tính điểm
                        for (int i1 = 0; i1 < listDiemChu.length; i1++) {
                            if (listDiemChu[i1].equals(diem)) {
                                soTinChiHienTai += soTC;
                                tongDiemHienTai += soTC * Utils.getInstance().convertDiemChuToDiemSo(diem);
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.d("RAKAN", "setValueSoTinChiVaTongSoDiemTinhCaHocPhanSeHoc: " + e.toString());
            e.printStackTrace();
        }
    }
}
