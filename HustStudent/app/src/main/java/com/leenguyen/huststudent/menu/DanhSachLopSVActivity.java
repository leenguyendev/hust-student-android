package com.leenguyen.huststudent.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.DanhSachLopSVAdapter;
import com.leenguyen.huststudent.databinding.ActivityDanhSachLopSVBinding;
import com.leenguyen.huststudent.model.SinhVienInfoModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class DanhSachLopSVActivity extends AppCompatActivity {
    private ActivityDanhSachLopSVBinding binding;
    private ArrayList<SinhVienInfoModel> sinhVienInfoModels;
    private DanhSachLopSVAdapter danhSachLopSVAdapter;
    private Dialog dialogSVInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_danh_sach_lop_s_v);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        boolean isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVipMember && !isNoAds) {
            initAdsBanner();
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            binding.loAds.setLayoutParams(layoutParams);
        }
        initLayout();
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    private void initLayout() {
        showDanhSachLopSV();
        binding.btnBack.setOnClickListener(view -> onBackPressed());

        binding.lvDanhSachLopSV.setOnItemClickListener((adapterView, view, i, l) -> showDialogSinhVienInfo(sinhVienInfoModels.get(i)));
    }

    @SuppressLint("SetTextI18n")
    private void showDialogSinhVienInfo(SinhVienInfoModel sinhVienInfoModel) {
        dialogSVInfo = new Dialog(this);
        dialogSVInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSVInfo.setContentView(R.layout.dialog_sinh_vien_info);
        TextView txtHoTenSV = dialogSVInfo.findViewById(R.id.txtHoTenSV);
        TextView txtMaSV = dialogSVInfo.findViewById(R.id.txtMaSV);
        TextView txtNgaySinh = dialogSVInfo.findViewById(R.id.txtNgaySinhSV);
        TextView txtLopSV = dialogSVInfo.findViewById(R.id.txtLopSv);
        TextView txtChuongTrinhDaoTao = dialogSVInfo.findViewById(R.id.txtChuongTrinhDaoTaoSV);
        TextView txtTrangThaiHocTap = dialogSVInfo.findViewById(R.id.txtTrangThaiHocTapSV);
        ImageView btnClose = dialogSVInfo.findViewById(R.id.btnClose);

        txtHoTenSV.setText(sinhVienInfoModel.getHoSV() + " " +
                sinhVienInfoModel.getDemSV() + " " +
                sinhVienInfoModel.getTenSV());
        txtMaSV.setText(sinhVienInfoModel.getMaSV());
        txtNgaySinh.setText(sinhVienInfoModel.getNgaySinhSV());
        txtLopSV.setText(sinhVienInfoModel.getLopSV());
        txtChuongTrinhDaoTao.setText(sinhVienInfoModel.getChuongTrinhDaoTaoSV());
        txtTrangThaiHocTap.setText("Trạng thái: " + sinhVienInfoModel.getTrangThaiHocTapSV());
        btnClose.setOnClickListener(view -> dialogSVInfo.dismiss());

        dialogSVInfo.show();
    }

    private void showDanhSachLopSV() {
        sinhVienInfoModels = new ArrayList<>();
        String dataDanhSachLopSV = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_DANH_SACH_LOP_SV);
        try {
            JSONArray danhSachSVJsonA = new JSONArray(dataDanhSachLopSV);
            if (danhSachSVJsonA.length() > 0) {
                for (int i = 0; i < danhSachSVJsonA.length(); i++) {
                    JSONObject sinhVienInfo = danhSachSVJsonA.getJSONObject(i);
                    String maSV = sinhVienInfo.getString(JsonUtils.KEY_MA_SV);
                    String hoSV = sinhVienInfo.getString(JsonUtils.KEY_HO_SV);
                    String demSV = sinhVienInfo.getString(JsonUtils.KEY_DEM_SV);
                    String tenSV = sinhVienInfo.getString(JsonUtils.KEY_TEN_SV);
                    String ngaySinhSV = sinhVienInfo.getString(JsonUtils.KEY_NGAY_SINH_SV);
                    String lopSV = sinhVienInfo.getString(JsonUtils.KEY_LOP_SV);
                    String chuongTrinhDaoTaoSV = sinhVienInfo.getString(JsonUtils.KEY_CHUONG_TRINH_DAO_TAO_SV);
                    String trangThaiHocTapSV = sinhVienInfo.getString(JsonUtils.KEY_TRANG_THAI_HOC_TAP_SV);
                    SinhVienInfoModel sinhVienInfoModel = new SinhVienInfoModel(maSV, hoSV, demSV, tenSV, ngaySinhSV, lopSV, chuongTrinhDaoTaoSV, trangThaiHocTapSV);
                    sinhVienInfoModels.add(sinhVienInfoModel);
                }
            }
            danhSachLopSVAdapter = new DanhSachLopSVAdapter(this, R.layout.item_danh_sach_lop_sv, sinhVienInfoModels);
            binding.lvDanhSachLopSV.setAdapter(danhSachLopSVAdapter);
        } catch (Exception e) {
            Log.e("RAKAN", "showDanhSachLopSV: " + e.toString());
        }
    }
}
