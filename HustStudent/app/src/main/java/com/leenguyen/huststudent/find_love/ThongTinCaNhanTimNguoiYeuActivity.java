package com.leenguyen.huststudent.find_love;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.WebviewActivity;
import com.leenguyen.huststudent.databinding.ActivityThongTinCaNhanTimNguoiYeuBinding;
import com.leenguyen.huststudent.databinding.DialogBoSungThongTinGioiThieuBinding;
import com.leenguyen.huststudent.databinding.DialogChonGioiTinhBinding;
import com.leenguyen.huststudent.databinding.DialogThongTinNguoiAyBinding;
import com.leenguyen.huststudent.databinding.DialogTimNguoiYeuBinding;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class ThongTinCaNhanTimNguoiYeuActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityThongTinCaNhanTimNguoiYeuBinding binding;
    private String hoTen;
    private String mssv;
    private String khoaVien;
    private String email;
    private String soDienThoai;
    private String ngaySinh;
    private String gioiTinh;
    private String queQuan;
    private String facebook;
    private String dacDiemCaNhanVaNguoiAy = "0000000000000000000000000000000000";
    private String tinhTrang;
    private boolean isDaCoGau;
    private Dialog dialogLoadTimNguoiYeu;
    private DatabaseReference loveDatabase;
    private ArrayList<ThongTinGioiThieuModel> thongTinGioiThieuModels;
    private String rate;
    private String gioiTinhCanTim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_thong_tin_ca_nhan_tim_nguoi_yeu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        loveDatabase = FirebaseDatabase.getInstance("https://hust-student-love.firebaseio.com/").getReference();
        loveDatabase.child("rate").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                rate = snapshot.getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        boolean isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVipMember && !isNoAds) {
            initAdsBanner();
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            binding.loAds.setLayoutParams(layoutParams);
        }
        initLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utils.getInstance().isOnline(this)) {
            Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), "Tính năng cần có kết nối mạng internet để hoạt động");
        }
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    private void initLayout() {
        showThongTinGioiThieu();
        binding.btnChinhSuaThongTInCaNha.setOnClickListener(this);
        binding.btnDacDiemCaNhanVaNguoiAy.setOnClickListener(this);
        binding.btnTimNguoiAy.setOnClickListener(this);
        binding.txtHuongDanLayLinkFace.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    private void showThongTinGioiThieu() {
        String dataThongTinCaNhanSV = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        try {
            JSONObject thongTinCaNhanSV = new JSONObject(dataThongTinCaNhanSV);
            hoTen = thongTinCaNhanSV.getString(JsonUtils.KEY_HO_TEN_SV).trim();
            mssv = thongTinCaNhanSV.getString(JsonUtils.KEY_MA_SO_SV).trim();
            khoaVien = thongTinCaNhanSV.getString(JsonUtils.KEY_KHOA_VIEN).trim() + " K" + thongTinCaNhanSV.getString(JsonUtils.KEY_KHOA_HOC).trim();
            email = thongTinCaNhanSV.getString(JsonUtils.KEY_EMAIL_SV).trim();
            gioiTinh = thongTinCaNhanSV.getString(JsonUtils.KEY_GIOI_TINH).trim();
            queQuan = thongTinCaNhanSV.getString(JsonUtils.KEY_DIA_CHI).trim();

            //Lay thong tin ngay sinh
            String dataDanhSachLopSV = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_DANH_SACH_LOP_SV);
            JSONArray danhSachSVJsonA = new JSONArray(dataDanhSachLopSV);
            if (danhSachSVJsonA.length() > 0) {
                for (int i = 0; i < danhSachSVJsonA.length(); i++) {
                    JSONObject sinhVienInfo = danhSachSVJsonA.getJSONObject(i);
                    String maSV = sinhVienInfo.getString(JsonUtils.KEY_MA_SV);
                    if (maSV.equals(mssv)) {
                        ngaySinh = sinhVienInfo.getString(JsonUtils.KEY_NGAY_SINH_SV);
                        break;
                    }
                }
            }
            binding.txtMSSV.setText(mssv);
            binding.txtHoTen.setText(hoTen);
            binding.txtKhoaVien.setText(khoaVien);
            binding.txtGioiTinh.setText(gioiTinh);
            binding.txtQueQuan.setText(queQuan.toLowerCase());
            binding.txtQueQuan.setSelected(true);
            binding.txtNgaySinh.setText(ngaySinh);
            String gioiTinhCheck;
            if (gioiTinh.equals("Nữ")) {
                gioiTinhCheck = "nu";
            } else {
                gioiTinhCheck = "nam";
            }
            loveDatabase.child(gioiTinhCheck).child(mssv).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    ThongTinGioiThieuModel thongTinGioiThieuModel = snapshot.getValue(ThongTinGioiThieuModel.class);
                    if (thongTinGioiThieuModel != null) {
                        soDienThoai = thongTinGioiThieuModel.getSoDienThoai();
                        isDaCoGau = thongTinGioiThieuModel.isDaCoGau();
                        tinhTrang = isDaCoGau ? getString(R.string.da_co_gau) : getString(R.string.chua_co_gau);
                        facebook = thongTinGioiThieuModel.getFacebook();
                        dacDiemCaNhanVaNguoiAy = thongTinGioiThieuModel.getDacDiemCaNhanVaNguoiAy();
                        if (dacDiemCaNhanVaNguoiAy == null || dacDiemCaNhanVaNguoiAy.length() < 34) {
                            dacDiemCaNhanVaNguoiAy = "0000000000000000000000000000000000";
                        }
                    } else {
                        soDienThoai = "";
                        tinhTrang = "";
                        facebook = "";
                        dacDiemCaNhanVaNguoiAy = "0000000000000000000000000000000000";
                    }
                    binding.txtSDT.setText(soDienThoai);
                    binding.txtTinhTrang.setText(tinhTrang);
                    binding.txtFacebook.setText(facebook);
                    binding.txtFacebook.setSelected(true);
                    Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                            Constant.SHARE_PREFERENCES_DATA,
                            Constant.KEY_SHARE_PREFERENCES_HO_SO_TIM_NGUOI_YEU,
                            dacDiemCaNhanVaNguoiAy);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getApplicationContext(), getString(R.string.khong_co_internet), Toast.LENGTH_LONG).show();
                }
            });

        } catch (Exception e) {
            Log.e("ThongTinCaNhanSV", "showThongTinCaNhanSV: " + e.toString());
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnChinhSuaThongTInCaNha:
                showDialogChinhSuaThongTinGioiThieu();
                break;
            case R.id.btnDacDiemCaNhanVaNguoiAy:
                Utils.getInstance().sendIntentActivity(this, DacDiemBanThanVaNguoiAyActivity.class);
                break;
            case R.id.txtHuongDanLayLinkFace:
                Intent intent = new Intent(getApplicationContext(), WebviewActivity.class);
                intent.putExtra("link", "https://digilux.vn/cach-lay-link-facebook-tren-dien-thoai/");
                startActivity(intent);
                break;
            case R.id.btnTimNguoiAy:
                if (!isHasAllThongTinGioiThieu()) {
                    Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), getString(R.string.tim_nguoi_yeu_ban_can_dien_du_thong_tin));
                } else if (isDaCoGau) {
                    Utils.getInstance().showNoticeDialog(this, getString(R.string.hazz), getString(R.string.co_gau_roi_thi_tim_ny_lam_gi));
                } else if (dacDiemCaNhanVaNguoiAy == null || dacDiemCaNhanVaNguoiAy.contains("0")) {
                    Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), getString(R.string.can_hoan_thanh_bang_dac_diem_ca_nhan_va_nguoi_ay));
                } else if (mssv.equals("") || hoTen.equals("")) {
                    Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), getString(R.string.ho_so_sinh_vien_chu_du));
                } else if (!Utils.getInstance().isOnline(getApplicationContext())) {
                    Toast.makeText(this, getString(R.string.khong_co_internet), Toast.LENGTH_LONG).show();
                } else if (!Utils.isPhoneNumberValidate(binding.txtSDT.getText().toString().trim(), "84")) {
                    Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), getString(R.string.cap_nhat_so_dien_thoai));
                } else if (!binding.txtFacebook.getText().toString().contains("https://www.facebook.com/")) {
                    Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), getString(R.string.cap_nhat_link_facebook));
                } else {
                    showDialogChonGioiTinh();
                }
                break;
        }
    }

    private void showDialogChonGioiTinh() {
        Dialog dialog = new Dialog(this);
        DialogChonGioiTinhBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_chon_gioi_tinh, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(bindingDialog.getRoot());

        bindingDialog.rdbgBanThich.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rdbNam) {
                gioiTinhCanTim = "nam";
                Glide.with(bindingDialog.getRoot()).load(R.drawable.img_chim_anh_vu).into(bindingDialog.imgMoPhong);
            } else if (checkedId == R.id.rdbNu) {
                gioiTinhCanTim = "nu";
                Glide.with(bindingDialog.getRoot()).load(R.drawable.img_san).into(bindingDialog.imgMoPhong);
            }
        });

        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());

        bindingDialog.btnTimLuon.setOnClickListener(v -> {
            if (bindingDialog.rdbNam.isChecked() || bindingDialog.rdbNu.isChecked()) {
                dialog.dismiss();
                batDauTimNguoiYeu(gioiTinhCanTim);
            }
        });
        dialog.show();
    }

    private void batDauTimNguoiYeu(String gioiTinh) {
        dialogLoadTimNguoiYeu = new Dialog(this);
        DialogTimNguoiYeuBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_tim_nguoi_yeu, null, false);
        dialogLoadTimNguoiYeu.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoadTimNguoiYeu.setCancelable(false);
        dialogLoadTimNguoiYeu.setContentView(bindingDialog.getRoot());

        Glide.with(getApplicationContext()).load(R.drawable.ic_ami_love_2).into(bindingDialog.imgSticker);
        Glide.with(getApplicationContext()).load(R.drawable.loading_trans).into(bindingDialog.imgLoading);

        if (thongTinGioiThieuModels != null && thongTinGioiThieuModels.size() > 0) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(() -> soSanhDacDiemTimNguoiYeu(thongTinGioiThieuModels));
                }
            }, 5000);
        } else {
            loveDatabase.child(gioiTinh).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    thongTinGioiThieuModels = new ArrayList<>();
                    List<String> keys = new ArrayList<>();
                    for (DataSnapshot keyNote : snapshot.getChildren()) {
                        keys.add(keyNote.getKey());
                        ThongTinGioiThieuModel thongTinGioiThieuModel = keyNote.getValue(ThongTinGioiThieuModel.class);
                        if (thongTinGioiThieuModel != null &&
                                !thongTinGioiThieuModel.isDaCoGau() &&
                                thongTinGioiThieuModel.getDacDiemCaNhanVaNguoiAy() != null &&
                                !thongTinGioiThieuModel.getDacDiemCaNhanVaNguoiAy().contains("0") &&
                                thongTinGioiThieuModel.getMssv() != null && !thongTinGioiThieuModel.getMssv().equals("") &&
                                thongTinGioiThieuModel.getHoTen() != null && !thongTinGioiThieuModel.getHoTen().equals("") &&
                                Utils.isPhoneNumberValidate(thongTinGioiThieuModel.getSoDienThoai(), "84") &&
                                thongTinGioiThieuModel.getFacebook().contains("https://www.facebook.com/")) {
                            thongTinGioiThieuModels.add(thongTinGioiThieuModel);
                        }
                    }

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(() -> soSanhDacDiemTimNguoiYeu(thongTinGioiThieuModels));
                        }
                    }, 5000);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        dialogLoadTimNguoiYeu.show();
    }

    private void soSanhDacDiemTimNguoiYeu(ArrayList<ThongTinGioiThieuModel> thongTinGioiThieuModels) {
        int diemPhuHop;
        String thongTinTimNguoiYeuCaNhan = dacDiemCaNhanVaNguoiAy;
        String kyTu_1_BanThan = String.valueOf(thongTinTimNguoiYeuCaNhan.charAt(0));
        String kyTu_2_BanThan = String.valueOf(thongTinTimNguoiYeuCaNhan.charAt(1));
        String kyTu_3_BanThan = String.valueOf(thongTinTimNguoiYeuCaNhan.charAt(2));
        String kyTu_4_BanThan = String.valueOf(thongTinTimNguoiYeuCaNhan.charAt(3));
        String cauTraLoiTracNghiemBanThan = thongTinTimNguoiYeuCaNhan.substring(4);

        String thongTinTimNguoiYeuNguoiAy;
        ArrayList<ThongTinGioiThieuModel> listNguoiPhuHop = new ArrayList<>();
        ArrayList<Integer> diemPhuHopList = new ArrayList<>();

        for (ThongTinGioiThieuModel thongTinGioiThieuModel : thongTinGioiThieuModels) {
            diemPhuHop = 0;
            thongTinTimNguoiYeuNguoiAy = thongTinGioiThieuModel.getDacDiemCaNhanVaNguoiAy();
            String kyTu_1_NguoiAy = String.valueOf(thongTinTimNguoiYeuNguoiAy.charAt(0));
            String kyTu_2_NguoiAy = String.valueOf(thongTinTimNguoiYeuNguoiAy.charAt(1));
            String kyTu_3_NguoiAy = String.valueOf(thongTinTimNguoiYeuNguoiAy.charAt(2));
            String kyTu_4_NguoiAy = String.valueOf(thongTinTimNguoiYeuNguoiAy.charAt(3));
            String cauTraLoiTracNghiemNguoiAy = thongTinTimNguoiYeuNguoiAy.substring(4);
            if (Integer.parseInt(rate) > 0) {
                if (kyTu_1_BanThan.equals(kyTu_3_NguoiAy) && kyTu_2_BanThan.equals(kyTu_4_NguoiAy) &&
                        kyTu_3_BanThan.equals(kyTu_1_NguoiAy) && kyTu_4_BanThan.equals(kyTu_2_NguoiAy)) {
                    diemPhuHop += 4;
                    for (int i = 0; i < 30; i++) {
                        String dapAnBanThan = String.valueOf(cauTraLoiTracNghiemBanThan.charAt(i));
                        String dapAnNguoiAy = String.valueOf(cauTraLoiTracNghiemNguoiAy.charAt(i));
                        if (dapAnBanThan.equals(dapAnNguoiAy)) {
                            diemPhuHop += 1;
                        }
                    }
                    if (diemPhuHop >= Integer.parseInt(rate)) {
                        listNguoiPhuHop.add(thongTinGioiThieuModel);
                        diemPhuHopList.add(diemPhuHop);
                    }
                }
            } else {
                for (int i = 0; i < 34; i++) {
                    String dapAnBanThan = String.valueOf(thongTinTimNguoiYeuCaNhan.charAt(i));
                    String dapAnNguoiAy = String.valueOf(thongTinTimNguoiYeuNguoiAy.charAt(i));
                    if (dapAnBanThan.equals(dapAnNguoiAy)) {
                        diemPhuHop += 1;
                    }
                    listNguoiPhuHop.add(thongTinGioiThieuModel);
                    diemPhuHopList.add(diemPhuHop);
                }
            }
        }

        if (listNguoiPhuHop.size() > 0) {
            Random random = new Random();
            int nguoiPhuHopNhatPos = random.nextInt(listNguoiPhuHop.size());
            showDialogKetQuaTimThayNguoiYeu(listNguoiPhuHop.get(nguoiPhuHopNhatPos), diemPhuHopList.get(nguoiPhuHopNhatPos));
        } else {
            showDialogKhongTimThayNguoiPhuHop();
        }
    }

    private void showDialogKhongTimThayNguoiPhuHop() {
        dialogLoadTimNguoiYeu.dismiss();
        Dialog dialog = new Dialog(this);
        DialogTimNguoiYeuBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_tim_nguoi_yeu, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(bindingDialog.getRoot());

        Glide.with(getApplicationContext()).load(R.drawable.ic_ami_cry).into(bindingDialog.imgSticker);
        bindingDialog.imgLoading.setVisibility(View.GONE);
        bindingDialog.btnXemNgay.setVisibility(View.VISIBLE);
        bindingDialog.btnXemNgay.setText("OK, huhu");
        bindingDialog.txtDecs.setText("Người yêu ơi mày ở đâu, sao ta tìm hoài chưa thấy. Hờn thực sự luôn!!!");

        bindingDialog.btnXemNgay.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void showDialogKetQuaTimThayNguoiYeu(ThongTinGioiThieuModel nguoiPhuHopNhat, int diemCaoNhat) {
        dialogLoadTimNguoiYeu.dismiss();
        Dialog dialog = new Dialog(this);
        DialogTimNguoiYeuBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_tim_nguoi_yeu, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(bindingDialog.getRoot());

        Glide.with(getApplicationContext()).load(R.drawable.ic_ami_quay).into(bindingDialog.imgSticker);
        bindingDialog.imgLoading.setVisibility(View.GONE);
        bindingDialog.btnXemNgay.setVisibility(View.VISIBLE);
        bindingDialog.btnXemNgay.setText("Xem ngay");
        bindingDialog.txtDecs.setText("Tìm thấy người yêu cho thím rồi, xem thông tin người ấy ngay thôi, hí hí");

        bindingDialog.btnXemNgay.setOnClickListener(v -> {
            dialog.dismiss();
            showDialogThongTinNguoiAy(nguoiPhuHopNhat, diemCaoNhat);
        });
        dialog.show();

    }

    @SuppressLint("SetTextI18n")
    private void showDialogThongTinNguoiAy(ThongTinGioiThieuModel nguoiPhuHopNhat, int diemCaoNhat) {
        Dialog dialog = new Dialog(this);
        DialogThongTinNguoiAyBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_thong_tin_nguoi_ay, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(bindingDialog.getRoot());
        bindingDialog.txtHoTen.setText(nguoiPhuHopNhat.getHoTen());
        bindingDialog.txtMSSV.setText(nguoiPhuHopNhat.getMssv().substring(0, 4) + "****");
        bindingDialog.txtKhoaVien.setText(nguoiPhuHopNhat.getKhoaVien());
        bindingDialog.txtSDT.setText(nguoiPhuHopNhat.getSoDienThoai());
        bindingDialog.txtNgaySinh.setText(nguoiPhuHopNhat.getNgaySinh());
        bindingDialog.txtGioiTinh.setText(nguoiPhuHopNhat.getGioiTinh());
        bindingDialog.txtTinhTrang.setText("Chưa có người yêu");
        bindingDialog.txtQueQuan.setText(nguoiPhuHopNhat.getQueQuan().toLowerCase());
        bindingDialog.txtQueQuan.setSelected(true);
        bindingDialog.txtKhoaVien.setSelected(true);
        bindingDialog.txtFacebook.setText(nguoiPhuHopNhat.getFacebook());
        bindingDialog.txtDoPhuHop.setText(diemCaoNhat * 100 / 34 + "%");
        bindingDialog.txtFacebook.setPaintFlags(bindingDialog.txtFacebook.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        bindingDialog.btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        bindingDialog.btnCopyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("", nguoiPhuHopNhat.getFacebook());
                if (clipboard != null) {
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getApplicationContext(), "Đã sao chép", Toast.LENGTH_LONG).show();
                }
            }
        });
        bindingDialog.btnXemTrangCaNhan.setOnClickListener(v -> Utils.getInstance().openFacebookPage(ThongTinCaNhanTimNguoiYeuActivity.this, nguoiPhuHopNhat.getFacebook()));
        bindingDialog.btnTimLai.setOnClickListener(v -> {
            dialog.dismiss();
            batDauTimNguoiYeu(gioiTinhCanTim);
        });
        dialog.show();
    }

    private void showDialogChinhSuaThongTinGioiThieu() {
        Dialog dialog = new Dialog(this);
        DialogBoSungThongTinGioiThieuBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_bo_sung_thong_tin_gioi_thieu, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(bindingDialog.getRoot());

        bindingDialog.edtFacebook.setText(facebook);
        bindingDialog.edtPhone.setText(soDienThoai);
        if (isDaCoGau) {
            bindingDialog.rdbDaCoGau.setChecked(true);
        } else {
            bindingDialog.rdbChuaCoGau.setChecked(true);
        }

        bindingDialog.btnClose.setOnClickListener(v -> dialog.dismiss());

        bindingDialog.rdbgTinhTrang.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rdbChuaCoGau) {
                isDaCoGau = false;
            } else if (checkedId == R.id.rdbDaCoGau) {
                isDaCoGau = true;
            }
        });

        bindingDialog.btnCapNhat.setOnClickListener(v -> {
            String sdt = bindingDialog.edtPhone.getText().toString().trim();
            String facebook = bindingDialog.edtFacebook.getText().toString().trim();
            String linkHeader = "https://www.facebook.com/";
            if (Utils.isPhoneNumberValidate(sdt, "84")) {
                bindingDialog.txtSDTKhongDung.setVisibility(View.GONE);
                if (facebook.contains(linkHeader) && facebook.length() > linkHeader.length()) {
                    bindingDialog.txtLinkFacebookKhongDung.setVisibility(View.GONE);
                    if (Utils.getInstance().isOnline(getApplicationContext())) {
                        ThongTinGioiThieuModel thongTinGioiThieuModel = new ThongTinGioiThieuModel(hoTen, mssv, khoaVien, email, sdt, ngaySinh, gioiTinh, queQuan, facebook, isDaCoGau, dacDiemCaNhanVaNguoiAy);
                        if (gioiTinh.equalsIgnoreCase("Nam")) {
                            loveDatabase.child("nam").child(mssv).setValue(thongTinGioiThieuModel);
                        } else {
                            loveDatabase.child("nu").child(mssv).setValue(thongTinGioiThieuModel);
                        }
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.khong_co_internet), Toast.LENGTH_LONG).show();
                    }
                } else {
                    bindingDialog.txtLinkFacebookKhongDung.setVisibility(View.VISIBLE);
                }
            } else {
                bindingDialog.txtSDTKhongDung.setVisibility(View.VISIBLE);
            }
        });
        dialog.show();
    }

    private boolean isHasAllThongTinGioiThieu() {
        boolean isHasAll = true;
        if (soDienThoai == null || soDienThoai.equals("")) {
            isHasAll = false;
        }
        if (tinhTrang == null || tinhTrang.equals("")) {
            isHasAll = false;
        }
        if (facebook == null || facebook.equals("")) {
            isHasAll = false;
        }
        return isHasAll;
    }
}