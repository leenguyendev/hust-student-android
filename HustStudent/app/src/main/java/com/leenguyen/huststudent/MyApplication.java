package com.leenguyen.huststudent;

import android.app.Application;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Objects;

public class MyApplication extends Application {
    private DatabaseReference adsIdDatabase;
    private DatabaseReference vipMemberDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        adsIdDatabase = FirebaseDatabase.getInstance("https://hust-student-admob-id.firebaseio.com/").getReference();
        vipMemberDatabase = FirebaseDatabase.getInstance("https://hust-student-vip-member.firebaseio.com/").getReference();

        adsIdDatabase.child("adsBanner").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                        Constant.SHARE_PREFERENCES_DATA,
                        Constant.KEY_SHARE_PREFERENCES_ADS_BANNER_ID,
                        Objects.requireNonNull(snapshot.getValue()).toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        adsIdDatabase.child("adsMoUngDung").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                        Constant.SHARE_PREFERENCES_DATA,
                        Constant.KEY_SHARE_PREFERENCES_ADS_OPEN_ID,
                        Objects.requireNonNull(snapshot.getValue()).toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        adsIdDatabase.child("adsNative").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                        Constant.SHARE_PREFERENCES_DATA,
                        Constant.KEY_SHARE_PREFERENCES_ADS_NATIVE_ID,
                        Objects.requireNonNull(snapshot.getValue()).toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        adsIdDatabase.child("maxAds").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                        Constant.SHARE_PREFERENCES_DATA,
                        Constant.KEY_SHARE_PREFERENCES_MAX_ADS,
                        Objects.requireNonNull(snapshot.getValue()).toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

//        adsIdDatabase.child("currentDay").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                int day = snapshot.getValue(Integer.class);
//                Calendar calendar = Calendar.getInstance();
//                int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
//                if (day != currentDay) {
//                    adsIdDatabase.child("currentDay").setValue(currentDay);
//                    adsIdDatabase.child("adsCount").setValue(0);
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });

        try {
            String dataThongTinSV = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
            if (dataThongTinSV == null || dataThongTinSV.equals("")) {
                return;
            }
            JSONObject dataThongTinSVJsonO = new JSONObject(dataThongTinSV);
            String mssv = dataThongTinSVJsonO.getString(JsonUtils.KEY_MA_SO_SV);
            vipMemberDatabase.child("danhSach").child(mssv).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.getValue() == null) {
                        Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER, "0");
                    } else {
                        Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER, "1");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
            adsIdDatabase.child("noAdsListMember").child(mssv).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.getValue() == null) {
                        Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS, "0");
                    } else {
                        Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS, "1");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
