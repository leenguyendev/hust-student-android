package com.leenguyen.huststudent.game;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.databinding.ActivityCaroBinding;
import com.leenguyen.huststudent.utils.Utils;

public class CaroActivity extends AppCompatActivity implements OnItemOCoClick {
    private ActivityCaroBinding binding;
    public static final int LUOT_X = 1;
    public static final int LUOT_O = 2;
    private CaroAdapter adapter;
    private int[] mangQuanCo = new int[169];
    private int luotDanh = LUOT_X;
    private int previousPos = -1;
    private DatabaseReference adsDatabase;
    private boolean isVipMember = false, isNoAds = false;
    private AdView adView;
    private int maxAds = 0, adsCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_caro);
        binding.btnBack.setOnClickListener(v -> finish());
        binding.loNew.setOnClickListener(v -> taoVanMoi());
        binding.btnRollback.setOnClickListener(v -> luiMotBuoc());
        adapter = new CaroAdapter(this, this);
        binding.banCo.setAdapter(adapter);
        GridLayoutManager manager = new GridLayoutManager(this, 13);
        binding.banCo.setLayoutManager(manager);
        adapter.setData(mangQuanCo);
        binding.imgLuotDanh.setImageResource(R.drawable.ic_caro_x);

        adsDatabase = FirebaseDatabase.getInstance("https://hust-student-admob-id.firebaseio.com/").getReference();
        isVipMember = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        String maxAdsString = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_MAX_ADS);
        if (maxAdsString != null && !maxAdsString.equals(""))
            maxAds = Integer.parseInt(maxAdsString);
        adsDatabase.child("adsCount").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                adsCount = snapshot.getValue(Integer.class);
                if (!isVipMember && !isNoAds) {
                    if (adsCount < maxAds) {
                        adsCount++;
                        adsDatabase.child("adsCount").setValue(adsCount);
                        MobileAds.initialize(getApplicationContext(), initializationStatus -> {
                        });
                        initAdsBanner();
                    } else {
                        binding.loAds.setVisibility(View.GONE);
                    }
                } else {
                    binding.loAds.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @SuppressLint("MissingPermission")
    private void initAdsBanner() {
        binding.loAds.setVisibility(View.VISIBLE);
        String adsBannerId = Utils.getInstance().getValueFromSharedPreferences(
                getApplicationContext(),
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_ADS_BANNER_ID);
//        String adsBannerId = "ca-app-pub-3940256099942544/6300978111";
        adView = new AdView(this);
        adView.setAdUnitId(adsBannerId);
        binding.containAdsView.addView(adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        AdSize adSize = getAdSize();
        // Step 4 - Set the adaptive ad size on the ad view.
        adView.setAdSize(adSize);
        // Step 5 - Start loading the ad in the background.
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void luiMotBuoc() {
        if (previousPos != -1) {
            adapter.setQuanCo(previousPos, 0);
            luotDanh = luotDanh == LUOT_X ? LUOT_O : LUOT_X;
            binding.imgLuotDanh.setImageResource(luotDanh == LUOT_X ? R.drawable.ic_caro_x : R.drawable.ic_caro_o);
            previousPos = -1;
        }
    }

    private void taoVanMoi() {
        mangQuanCo = new int[169];
        adapter.setData(mangQuanCo);
        luotDanh = LUOT_X;
        binding.imgLuotDanh.setImageResource(R.drawable.ic_caro_x);
    }

    @Override
    public void onItemOCoClick(int pos) {
        if (luotDanh == LUOT_X) {
            adapter.setQuanCo(pos, LUOT_X);
            luotDanh = LUOT_O;
            binding.imgLuotDanh.setImageResource(R.drawable.ic_caro_o);
        } else {
            adapter.setQuanCo(pos, LUOT_O);
            luotDanh = LUOT_X;
            binding.imgLuotDanh.setImageResource(R.drawable.ic_caro_x);
        }
        previousPos = pos;
    }
}