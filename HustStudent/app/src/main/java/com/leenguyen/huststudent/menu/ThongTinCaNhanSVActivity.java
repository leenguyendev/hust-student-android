package com.leenguyen.huststudent.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.MobileAds;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.LoginActivity;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.databinding.ActivityThongTinCaNhanSVBinding;
import com.leenguyen.huststudent.databinding.DialogRotateImageBinding;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ThongTinCaNhanSVActivity extends AppCompatActivity {
    private ActivityThongTinCaNhanSVBinding binding;
    public static final int PICK_IMAGE = 1;
    private float rotateAngle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_thong_tin_ca_nhan_s_v);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        initLayout();
    }

    private void initLayout() {
        displayAvatar();
        showThongTinCaNhanSV();
        binding.btnBack.setOnClickListener(view -> onBackPressed());

        binding.imgAvatar.setOnClickListener(v -> {
            if (isStoragePermissionGranted()) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });

        binding.btnDangXuat.setOnClickListener(v -> showDiaLogLogout());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void showDiaLogLogout() {
        final Dialog logoutDialog = new Dialog(this);
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setContentView(R.layout.dialog_confirm);
        TextView txtTitle = logoutDialog.findViewById(R.id.txtTitle);
        TextView txtDesc = logoutDialog.findViewById(R.id.txtDesc);
        Button btnYes = logoutDialog.findViewById(R.id.btnContinues);
        Button btnNo = logoutDialog.findViewById(R.id.btnCancel);
        CheckBox checkBox = logoutDialog.findViewById(R.id.cbReadAndAccess);

        checkBox.setVisibility(View.GONE);
        txtTitle.setText(getString(R.string.confirm_logout_title));
        txtDesc.setText(getString(R.string.confirm_logout_desc));
        btnNo.setOnClickListener(v -> logoutDialog.dismiss());

        btnYes.setOnClickListener(v -> {
            logoutDialog.dismiss();
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.SHARE_PREFERENCES_DATA_ALREADY_USER_LOGIN, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_CA_NHAN, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_TONG_KET, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_CAI_THIEN, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_MOI, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_DANH_SACH_LOP_SV, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_DIEM_THI_TOEIC, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_TKB, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR_ROTATE_ANGLE, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_HOC_PHI, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_KIEM_TRA_NHAP_DIEM_KI_MOI, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_TKB_TAM_THOI, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_HOC_PHAN_KHONG_TINH_DIEM, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_HO_SO_TIM_NGUOI_YEU, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_LICH_THI_FULL, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_LICH_THI_TU_THEM, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_LICH_THI_SAVED, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER, "");
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS, "");
            Utils.getInstance().sendIntentActivity(getApplicationContext(), LoginActivity.class);
            finish();
        });
        logoutDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            if (requestCode == PICK_IMAGE && data.getData() != null) {
                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    showDialogEditImage(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showDialogEditImage(final Bitmap bitmap) {
        final Dialog dialog = new Dialog(this);
        final DialogRotateImageBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_rotate_image, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        bindingDialog.image.setImageBitmap(bitmap);

        rotateAngle = 0;
        bindingDialog.rolLeft.setOnClickListener(v -> {
            rotateAngle = bindingDialog.image.getRotation() - 90;
            bindingDialog.image.setRotation(bindingDialog.image.getRotation() - 90);
        });

        bindingDialog.rolRight.setOnClickListener(v -> {
            rotateAngle = bindingDialog.image.getRotation() + 90;
            bindingDialog.image.setRotation(bindingDialog.image.getRotation() + 90);
        });

        bindingDialog.btnCancel.setOnClickListener(view -> dialog.dismiss());

        bindingDialog.btnSave.setOnClickListener(view -> {
            binding.imgAvatar.setImageBitmap(bitmap);
            binding.imgAvatar.setRotation(rotateAngle % 360);
            bindingDialog.btnCancel.setClickable(false);
            bindingDialog.btnSave.setClickable(false);
            bindingDialog.llRotate.setClickable(false);
//              Save
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR, encodedImage);
            Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR_ROTATE_ANGLE, String.valueOf(rotateAngle % 360));

            dialog.dismiss();
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void displayAvatar() {
        String encodedImage = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR);
        String rotateAngle = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR_ROTATE_ANGLE);
        if (encodedImage != null && !encodedImage.equals("")) {
            byte[] imageAsBytes = Base64.decode(encodedImage.getBytes(), Base64.DEFAULT);
            binding.imgAvatar.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
            binding.imgAvatar.setRotation(Float.parseFloat(rotateAngle));
        }
    }

    @SuppressLint("SetTextI18n")
    private void showThongTinCaNhanSV() {
        String dataThongTinCaNhanSV = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        try {
            JSONObject thongTinCaNhanSV = new JSONObject(dataThongTinCaNhanSV);
            binding.txtHoTenSV.setText(thongTinCaNhanSV.getString(JsonUtils.KEY_HO_TEN_SV));
            binding.txtMssv.setText(thongTinCaNhanSV.getString(JsonUtils.KEY_MA_SO_SV));
            binding.txtNamVaoTruong.setText("Năm vào trường: " + thongTinCaNhanSV.getString(JsonUtils.KEY_NAM_VAO_TRUONG));
            binding.txtBacDaoTao.setText("Bậc đào tạo: " + thongTinCaNhanSV.getString(JsonUtils.KEY_BAC_DAO_TAO));
            binding.txtChuongTrinh.setText("Chương trình: " + thongTinCaNhanSV.getString(JsonUtils.KEY_CHUONG_TRINH));
            binding.txtKhoaVienQuanLy.setText("Khoa/viện quản lý: " + thongTinCaNhanSV.getString(JsonUtils.KEY_KHOA_VIEN));
            binding.txtTinhTrangHocTap.setText("Tình trạng học tập: " + thongTinCaNhanSV.getString(JsonUtils.KEY_TINH_TRANG_HOC_TAP));
            binding.txtGioiTinh.setText("Giới tính: " + thongTinCaNhanSV.getString(JsonUtils.KEY_GIOI_TINH));
            binding.txtLop.setText("Lớp: " + thongTinCaNhanSV.getString(JsonUtils.KEY_LOP));
            binding.txtKhoaHoc.setText("Khóa học: " + thongTinCaNhanSV.getString(JsonUtils.KEY_KHOA_HOC));
            binding.txtEmail.setText("Email: " + thongTinCaNhanSV.getString(JsonUtils.KEY_EMAIL_SV));
            binding.txtDanToc.setText("Dân tộc: " + thongTinCaNhanSV.getString(JsonUtils.KEY_DAN_TOC));
            binding.txtNamTotNghiepC3.setText("Năm tốt nghiệp C3: " + thongTinCaNhanSV.getString(JsonUtils.KEY_NAM_TOT_NGHIEP_C3));
            binding.txtDiaChi.setText("Địa chỉ: " + thongTinCaNhanSV.getString(JsonUtils.KEY_DIA_CHI));
            binding.txtSoCMTND.setText("Số CMTND: " + thongTinCaNhanSV.getString(JsonUtils.KEY_SO_CMTND));
            binding.txtNoiCap.setText("Nơi cấp: " + thongTinCaNhanSV.getString(JsonUtils.KEY_NOI_CAP));
            binding.txtTruongTHPT.setText("Trường THPT: " + thongTinCaNhanSV.getString(JsonUtils.KEY_TRUONG_THPT));
            binding.txtHoKhau.setText("Hộ khẩu: " + thongTinCaNhanSV.getString(JsonUtils.KEY_HO_KHAU));
            binding.txtSoDienThoai.setText("Số điện thoại: " + thongTinCaNhanSV.getString(JsonUtils.KEY_SO_DIEN_THOAI_SV));
            binding.txtNamVaoTruong.setText("Năm vào trường: " + thongTinCaNhanSV.getString(JsonUtils.KEY_NAM_VAO_TRUONG));
            binding.txtTonGiao.setText("Tôn giáo: " + thongTinCaNhanSV.getString(JsonUtils.KEY_TON_GIAO));

            binding.txtHoTenBo.setText("Họ tên bố: " + thongTinCaNhanSV.getString(JsonUtils.KEY_HO_TEN_BO));
            binding.txtNgheNghiepBo.setText("Nghề nghiệp: " + thongTinCaNhanSV.getString(JsonUtils.KEY_NGHE_NGHIEP_BO));
            binding.txtNamSinhBo.setText("Năm sinh: " + thongTinCaNhanSV.getString(JsonUtils.KEY_NAM_SINH_BO));
            binding.txtDienThoaiBo.setText("Điện thoại: " + thongTinCaNhanSV.getString(JsonUtils.KEY_DIEN_THOAI_BO));
            binding.txtEmailBo.setText("Email: " + thongTinCaNhanSV.getString(JsonUtils.KEY_EMAIL_BO));

            binding.txtHoTenMe.setText("Họ tên mẹ: " + thongTinCaNhanSV.getString(JsonUtils.KEY_HO_TEN_ME));
            binding.txtNgheNghiepMe.setText("Nghề nghiệp: " + thongTinCaNhanSV.getString(JsonUtils.KEY_NGHE_NGHIEP_ME));
            binding.txtNamSinhMe.setText("Năm sinh: " + thongTinCaNhanSV.getString(JsonUtils.KEY_NAM_SINH_ME));
            binding.txtDienThoaiMe.setText("Điện thoại: " + thongTinCaNhanSV.getString(JsonUtils.KEY_DIEN_THOAI_ME));
            binding.txtEmailMe.setText("Email: " + thongTinCaNhanSV.getString(JsonUtils.KEY_EMAIL_ME));

        } catch (Exception e) {
            Log.e("ThongTinCaNhanSV", "showThongTinCaNhanSV: " + e.toString());
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkReadWriteExternalStorePermission()) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    private boolean checkReadWriteExternalStorePermission() {
        int write = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }
}
