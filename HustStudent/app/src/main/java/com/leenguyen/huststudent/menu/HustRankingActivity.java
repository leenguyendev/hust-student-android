package com.leenguyen.huststudent.menu;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.adapter.HustRankingAdapter;
import com.leenguyen.huststudent.databinding.ActivityHustRankingBinding;
import com.leenguyen.huststudent.databinding.DialogFilterHustRankingBinding;
import com.leenguyen.huststudent.databinding.DialogNoticeBinding;
import com.leenguyen.huststudent.model.HustRankingModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HustRankingActivity extends AppCompatActivity {
    private ActivityHustRankingBinding binding;
    private HustRankingAdapter adapter;
    private ArrayList<HustRankingModel> hustRankingModels, dataShowList;
    private boolean isFirstTime = true, isPrivate = false, isVip = false;
    private DatabaseReference rankingDatabase;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hust_ranking);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        rankingDatabase = FirebaseDatabase.getInstance("https://hust-student-ranking.firebaseio.com/").getReference();
        isVip = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");
        boolean isNoAds = Utils.getInstance().getValueFromSharedPreferences(
                this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_NO_ADS).equals("1");
        if (!isVip && !isNoAds) {
            initAdsBanner();
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            binding.loAds.setLayoutParams(layoutParams);
        }
        initLayout();
    }

    private void initAdsBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        binding.adView.loadAd(adRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initLayout() {
        if (!Utils.getInstance().isOnline(getApplicationContext())) {
            Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), "Tính năng cần có kết nối mạng internet để hoạt động");
        }

        isFirstTime = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_FIRST_TIME).equals(Constant.VALUE_SHARE_PREFERENCES_IS_FIRST_TIME_TRUE);

        checkPrivateUser();

        binding.edtSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (binding.edtSearch.getText() != null) {
                    searchStudent(binding.edtSearch.getText().toString());
                }
                hideKeyboard(HustRankingActivity.this);
                return true;
            }
            return false;
        });

        binding.imgFilter.setOnClickListener(v -> showDialogFilter());

        binding.btnBack.setOnClickListener(v -> onBackPressed());
    }

    private void checkPrivateUser() {
        String mssv;
        String studentInfoData = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        try {
            JSONObject studentInfoJsonO = new JSONObject(studentInfoData);
            mssv = studentInfoJsonO.getString(JsonUtils.KEY_MA_SO_SV);
            String khoaHoc = Utils.getInstance().getKhoaHoc(this);
            rankingDatabase.child("k" + khoaHoc).child(mssv).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    HustRankingModel hustRankingModel = snapshot.getValue(HustRankingModel.class);
                    if (hustRankingModel != null) {
                        isPrivate = hustRankingModel.isPrivate();
                    }
                    if (isFirstTime && !isPrivate) {
                        showDialogFirstTimeGuide();
                    }
                    if (isPrivate && !isVip) {
                        try {
                            binding.lvHustRanking.setVisibility(View.GONE);
                            Utils.getInstance().showNoticeDialog(HustRankingActivity.this, getString(R.string.chua_cap_quyen_xem_hust_ranking), getString(R.string.chua_cap_quyen_xem_hust_ranking_desc));
                            binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
                            binding.loKhongCoDuLieu.bringToFront();
                            Glide.with(HustRankingActivity.this).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        loadDatabase("k" + khoaHoc);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadDatabase(String khoaHoc) {
        rankingDatabase.child(khoaHoc).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                hustRankingModels = new ArrayList<>();
                List<String> keys = new ArrayList<>();
                for (DataSnapshot keyNote : snapshot.getChildren()) {
                    keys.add(keyNote.getKey());
                    HustRankingModel hustRankingModel = keyNote.getValue(HustRankingModel.class);
                    hustRankingModels.add(hustRankingModel);
                }
                hustRankingModels = Utils.sortHustRanking(hustRankingModels);
                showDataToListView();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void showDataToListView() {
        binding.loKhongCoDuLieu.setVisibility(View.GONE);
        binding.lvHustRanking.setVisibility(View.VISIBLE);
        dataShowList = new ArrayList<>();
        for (int i = hustRankingModels.size() - 1; i >= 0; i--) {
            HustRankingModel hustRankingModel = hustRankingModels.get(i);
            if (!hustRankingModel.isPrivate()) {
                dataShowList.add(hustRankingModel);
            }
        }
        adapter = new HustRankingAdapter(HustRankingActivity.this, R.layout.item_hust_ranking, dataShowList);
        binding.lvHustRanking.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void showDialogFirstTimeGuide() {
        final Dialog dialog = new Dialog(this);
        DialogNoticeBinding noticeBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_notice, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(noticeBinding.getRoot());
        noticeBinding.txtTitle.setText(getString(R.string.is_first_time_notice_title));
        noticeBinding.txtDecs.setText(getString(R.string.is_first_time_notice_decs));
        noticeBinding.btnOK.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().saveToSharedPreferences(getApplicationContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_IS_FIRST_TIME, Constant.VALUE_SHARE_PREFERENCES_IS_FIRST_TIME_FALSE);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showDialogFilter() {
        final Dialog filterDialog = new Dialog(this);
        final DialogFilterHustRankingBinding dialogBinding = DataBindingUtil.inflate(LayoutInflater.from(getApplicationContext()), R.layout.dialog_filter_hust_ranking, null, false);
        filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        filterDialog.setContentView(dialogBinding.getRoot());
        //set value for number picker
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int khoaCuoiCung = year - 1965 + 10;
        dialogBinding.pkNumber.setMaxValue(khoaCuoiCung);
        dialogBinding.pkNumber.setMinValue(khoaCuoiCung - 7);
        dialogBinding.pkNumber.setValue(khoaCuoiCung - 3);

        dialogBinding.btnClose.setOnClickListener(v -> filterDialog.dismiss());

        dialogBinding.btnFilter.setOnClickListener(v -> {
            int khoaHoc = dialogBinding.pkNumber.getValue();
            int namHoc = 1965 + (khoaHoc - 10);
            ArrayList<HustRankingModel> arrayListFilter = new ArrayList<>();
            try {
                if (dataShowList != null && dataShowList.size() > 0) {
                    for (int i = 0; i < dataShowList.size(); i++) {
                        HustRankingModel topStudent = dataShowList.get(i);
                        if (topStudent != null) {
                            String mssv = topStudent.getMssv();
                            if (mssv != null && mssv.substring(0, 4).equals(String.valueOf(namHoc))) {
                                arrayListFilter.add(topStudent);
                            }
                        }
                    }
                }
                if (arrayListFilter.size() > 0) {
                    binding.loKhongCoDuLieu.setVisibility(View.GONE);
                    adapter = new HustRankingAdapter(HustRankingActivity.this, R.layout.item_hust_ranking, arrayListFilter);
                    binding.lvHustRanking.setAdapter(adapter);
                } else {
                    binding.loKhongCoDuLieu.setVisibility(View.VISIBLE);
                    binding.loKhongCoDuLieu.bringToFront();
                    Glide.with(HustRankingActivity.this).load(R.drawable.ic_ami_ok).into(binding.imgSticker);
                }
            } catch (Exception e) {
                Log.e("RAKAN", "showDialogFilterRank: " + e.toString());
                e.printStackTrace();
            }
            filterDialog.dismiss();
        });
        filterDialog.show();
    }

    private void searchStudent(String toString) {
        try {
            boolean isHasStudent = false;
            if (dataShowList != null && dataShowList.size() > 0) {
                for (int i = 0; i < dataShowList.size(); i++) {
                    HustRankingModel hustRankingModel = dataShowList.get(i);
                    if (hustRankingModel.getMssv().contains(toString) || hustRankingModel.getName().contains(toString)) {
                        isHasStudent = true;
                        binding.lvHustRanking.smoothScrollToPosition(i);
                        adapter.setHighLightPosition(i);
                        adapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
            if (!isHasStudent) {
                Toast.makeText(this, getString(R.string.khong_tim_thay_sinh_vien), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.e("RAKAN", "searchStudent: " + e.toString());
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.khong_tim_thay_sinh_vien), Toast.LENGTH_LONG).show();
        }
    }

    private static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
