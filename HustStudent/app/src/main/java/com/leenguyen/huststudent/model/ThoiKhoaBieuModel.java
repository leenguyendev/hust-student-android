package com.leenguyen.huststudent.model;

public class ThoiKhoaBieuModel {
    private String thoiGian, tuanHoc, phongHoc, maLop, loaiLop, nhom, maHP, tenLop, ghiChu, hinhThucDay, giangVien, linkOnline, maCode;

    public ThoiKhoaBieuModel(String thoiGian, String tuanHoc, String phongHoc, String maLop, String loaiLop, String nhom, String maHP, String tenLop, String ghiChu, String hinhThucDay, String giangVien, String linkOnline, String maCode) {
        this.thoiGian = thoiGian;
        this.tuanHoc = tuanHoc;
        this.phongHoc = phongHoc;
        this.maLop = maLop;
        this.loaiLop = loaiLop;
        this.nhom = nhom;
        this.maHP = maHP;
        this.tenLop = tenLop;
        this.ghiChu = ghiChu;
        this.hinhThucDay = hinhThucDay;
        this.giangVien = giangVien;
        this.linkOnline = linkOnline;
        this.maCode = maCode;
    }

    public String getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(String thoiGian) {
        this.thoiGian = thoiGian;
    }

    public String getTuanHoc() {
        return tuanHoc;
    }

    public void setTuanHoc(String tuanHoc) {
        this.tuanHoc = tuanHoc;
    }

    public String getPhongHoc() {
        return phongHoc;
    }

    public void setPhongHoc(String phongHoc) {
        this.phongHoc = phongHoc;
    }

    public String getMaLop() {
        return maLop;
    }

    public void setMaLop(String maLop) {
        this.maLop = maLop;
    }

    public String getLoaiLop() {
        return loaiLop;
    }

    public void setLoaiLop(String loaiLop) {
        this.loaiLop = loaiLop;
    }

    public String getNhom() {
        return nhom;
    }

    public void setNhom(String nhom) {
        this.nhom = nhom;
    }

    public String getMaHP() {
        return maHP;
    }

    public void setMaHP(String maHP) {
        this.maHP = maHP;
    }

    public String getTenLop() {
        return tenLop;
    }

    public void setTenLop(String tenLop) {
        this.tenLop = tenLop;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getHinhThucDay() {
        return hinhThucDay;
    }

    public void setHinhThucDay(String hinhThucDay) {
        this.hinhThucDay = hinhThucDay;
    }

    public String getGiangVien() {
        return giangVien;
    }

    public void setGiangVien(String giangVien) {
        this.giangVien = giangVien;
    }

    public String getLinkOnline() {
        return linkOnline;
    }

    public void setLinkOnline(String linkOnline) {
        this.linkOnline = linkOnline;
    }

    public String getMaCode() {
        return maCode;
    }

    public void setMaCode(String maCode) {
        this.maCode = maCode;
    }
}
