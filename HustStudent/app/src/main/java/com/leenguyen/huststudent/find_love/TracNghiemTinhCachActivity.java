package com.leenguyen.huststudent.find_love;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.databinding.ActivityTracNghiemTinhCachBinding;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TracNghiemTinhCachActivity extends AppCompatActivity {

    private ActivityTracNghiemTinhCachBinding binding;
    private ArrayList<CauHoiTracNghiemTinhCachModel> arrayList;
    private TracNghiemTinhCachAdapter adapter;
    private List<String> listCauHoi, listCauTraLoi_1, listCauTraLoi_2;
    private String hoSoTimNguoiYeu;
    private DatabaseReference loveDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trac_nghiem_tinh_cach);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        loveDatabase = FirebaseDatabase.getInstance("https://hust-student-love.firebaseio.com/").getReference();

        listCauHoi = Arrays.asList(getResources().getStringArray(R.array.cau_hoi_trac_nghiem_tinh_cach));
        listCauTraLoi_1 = Arrays.asList(getResources().getStringArray(R.array.cau_tra_loi_trac_nghiem_tinh_cach_1));
        listCauTraLoi_2 = Arrays.asList(getResources().getStringArray(R.array.cau_tra_loi_trac_nghiem_tinh_cach_2));

        hoSoTimNguoiYeu = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_HO_SO_TIM_NGUOI_YEU);
        initLayout();
    }

    private void initLayout() {
        binding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        showListCauHoi();
        binding.btnCapNhatCauTraLoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capNhatCauTraLoiTracNghiemTinhCach();
            }
        });
    }

    private void capNhatCauTraLoiTracNghiemTinhCach() {
        boolean isHasCauChuaTraLoi = false;
        int cauChuaTraLoi = 0;
        ArrayList<CauHoiTracNghiemTinhCachModel> arrayList;
        arrayList = adapter.getListCauHoiTracNghiemModels();
        if (arrayList != null && arrayList.size() == 30) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < arrayList.size(); i++) {
                CauHoiTracNghiemTinhCachModel cauHoiTracNghiemTinhCachModel = arrayList.get(i);
                if (cauHoiTracNghiemTinhCachModel.isDapAn_1()) {
                    builder.append("1");
                } else if (cauHoiTracNghiemTinhCachModel.isDapAn_2()) {
                    builder.append("2");
                } else {
                    builder.append("0");
                    isHasCauChuaTraLoi = true;
                    cauChuaTraLoi = i + 1;
                }
            }
            String cauTraLoi = String.valueOf(builder);
            String dacDiemBanThanVaNguoiAy = Utils.getInstance().getValueFromSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_HO_SO_TIM_NGUOI_YEU);
            String dacDiemBanThanVaNguoiAyNew = dacDiemBanThanVaNguoiAy.substring(0, 4) + cauTraLoi;

            Utils.getInstance().saveToSharedPreferences(getApplicationContext(),
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_HO_SO_TIM_NGUOI_YEU,
                    dacDiemBanThanVaNguoiAyNew);

            String dataThongTinCaNhanSV = Utils.getInstance().getValueFromSharedPreferences(this,
                    Constant.SHARE_PREFERENCES_DATA,
                    Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
            String mssv, gioiTinh;
            try {
                JSONObject thongTinCaNhanSV = new JSONObject(dataThongTinCaNhanSV);
                mssv = thongTinCaNhanSV.getString(JsonUtils.KEY_MA_SO_SV).trim();
                gioiTinh = thongTinCaNhanSV.getString(JsonUtils.KEY_GIOI_TINH).trim();
                String gioiTinhCheck;
                if (gioiTinh.equals("Nữ")) {
                    gioiTinhCheck = "nu";
                } else {
                    gioiTinhCheck = "nam";
                }
                loveDatabase.child(gioiTinhCheck).child(mssv).child("dacDiemCaNhanVaNguoiAy").setValue(dacDiemBanThanVaNguoiAyNew);
            } catch (Exception e) {
                Log.e("RAKAN", "capNhatCauTraLoiTracNghiemTinhCach " + e.toString());
                e.printStackTrace();
            }

            if (Utils.getInstance().isOnline(getApplicationContext())) {
                if (isHasCauChuaTraLoi) {
                    Toast.makeText(getApplicationContext(), "thím chưa trả lời câu " + cauChuaTraLoi, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Cập nhật câu trả lời thành công", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Cập nhật thất bại vì không có kết nối mạng", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void showListCauHoi() {
        arrayList = new ArrayList<>();
        if (hoSoTimNguoiYeu.length() == 34) {
            for (int i = 4; i < 34; i++) {
                String cauTraLoi = String.valueOf(hoSoTimNguoiYeu.charAt(i));
                boolean isDapAn_1 = false, isDapAn_2 = false;
                if (cauTraLoi.equals("1")) {
                    isDapAn_1 = true;
                } else if (cauTraLoi.equals("2")) {
                    isDapAn_2 = true;
                }
                CauHoiTracNghiemTinhCachModel cauHoiTracNghiemTinhCachModel = new CauHoiTracNghiemTinhCachModel(listCauHoi.get(i - 4), listCauTraLoi_1.get(i - 4), listCauTraLoi_2.get(i - 4), isDapAn_1, isDapAn_2);
                arrayList.add(cauHoiTracNghiemTinhCachModel);
            }

        } else {
            for (int i = 0; i < 30; i++) {
                CauHoiTracNghiemTinhCachModel cauHoiTracNghiemTinhCachModel = new CauHoiTracNghiemTinhCachModel(listCauHoi.get(i), listCauTraLoi_1.get(i), listCauTraLoi_2.get(i), false, false);
                arrayList.add(cauHoiTracNghiemTinhCachModel);
            }
        }
        adapter = new TracNghiemTinhCachAdapter(this, R.layout.item_cau_hoi_trac_nghiem_tinh_cach, arrayList);
        binding.lvQuestion.setAdapter(adapter);
    }
}