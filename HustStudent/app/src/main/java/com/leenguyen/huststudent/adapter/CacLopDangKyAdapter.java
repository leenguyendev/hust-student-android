package com.leenguyen.huststudent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.model.CacLopDangKyModel;

import java.util.ArrayList;

public class CacLopDangKyAdapter extends ArrayAdapter<CacLopDangKyModel> {
    private Context context;
    private ArrayList<CacLopDangKyModel> cacLopDangKyModels;
    private int layoutResource;

    public CacLopDangKyAdapter(@NonNull Context context, int resource, @NonNull ArrayList<CacLopDangKyModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.cacLopDangKyModels = objects;
        this.layoutResource = resource;
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("ViewHolder") View row = inflater.inflate(layoutResource, null);
        TextView txtTenHP = row.findViewById(R.id.txtTenHP);
        TextView txtMaLop = row.findViewById(R.id.txtMaLop);
        TextView txtTrangThaiDangKy = row.findViewById(R.id.txtTrangThaiDangKy);
        txtTenHP.setText(cacLopDangKyModels.get(position).getTenLop() + " (" + cacLopDangKyModels.get(position).getMaHP() + ")");
        txtMaLop.setText("Mã lớp: " + cacLopDangKyModels.get(position).getMaLop());
        txtTrangThaiDangKy.setText("Trạng thái ĐK: " + cacLopDangKyModels.get(position).getTrangThaiDK());
        return row;
    }
}
