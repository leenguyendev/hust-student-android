package com.leenguyen.huststudent.menu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.NguonThamKhaoActivity;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.databinding.ActivitySettingsBinding;
import com.leenguyen.huststudent.model.HustRankingModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONObject;

import java.util.Calendar;

public class SettingsActivity extends AppCompatActivity {
    private static final String LINK_FANPAGE = "https://www.facebook.com/HUST-Student-Android-108528431861926";
    private ActivitySettingsBinding binding;
    private DatabaseReference rankingDatabase, listNoAdsMemberDatabase, listVipMemberDatabase;
    private String khoaHoc;
    private String startDayTurnOn;
    private boolean isVip;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings, null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        rankingDatabase = FirebaseDatabase.getInstance("https://hust-student-ranking.firebaseio.com/").getReference();
        listNoAdsMemberDatabase = FirebaseDatabase.getInstance("https://hust-student-admob-id.firebaseio.com/").getReference();
        listVipMemberDatabase = FirebaseDatabase.getInstance("https://hust-student-vip-member.firebaseio.com/").getReference();

        isVip = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_IS_VIP_MEMBER).equals("1");

        initLayout();
    }

    private void initLayout() {
        checkAdmin();
        binding.txtVersion.setText("Version " + Utils.getInstance().getCurrentVersion(this));
        binding.btnBack.setOnClickListener(view -> onBackPressed());
        binding.loFanpage.setOnClickListener(view -> checkNetworkAndOpenChrome());

        initValueSwitchButton();

        binding.btnSetUserRankingPrivate.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Calendar calendar = Calendar.getInstance();
            int currentDay = calendar.get(Calendar.DAY_OF_YEAR);
            if (isChecked || isVip) {
                postPermissionState(isChecked, String.valueOf(currentDay));
            } else {
                if (startDayTurnOn != null && !startDayTurnOn.equals("")) {
                    if ((currentDay - Integer.parseInt(startDayTurnOn)) >= 7) {
                        postPermissionState(isChecked, String.valueOf(currentDay));
                    } else if (currentDay < Integer.parseInt(startDayTurnOn)) {
                        int soNgayTurnOn = (365 - Integer.parseInt(startDayTurnOn)) + currentDay;
                        if (soNgayTurnOn >= 7) {
                            postPermissionState(isChecked, String.valueOf(currentDay));
                        } else {
                            showDialog7DayToTurnOff(7 - soNgayTurnOn);
                            binding.btnSetUserRankingPrivate.setChecked(true);
                        }
                    } else {
                        showDialog7DayToTurnOff(7 - (currentDay - Integer.parseInt(startDayTurnOn)));
                        binding.btnSetUserRankingPrivate.setChecked(true);
                    }
                } else {
                    startDayTurnOn = String.valueOf(currentDay);
                    postPermissionState(isChecked, String.valueOf(currentDay));
                }
            }
        });

        binding.loNguonThamKhao.setOnClickListener(v -> Utils.getInstance().sendIntentActivity(SettingsActivity.this, NguonThamKhaoActivity.class));
    }

    @SuppressLint("QueryPermissionsNeeded")
    private void checkNetworkAndOpenChrome() {
        if (!Utils.getInstance().isOnline(this)) {
            Toast.makeText(getApplicationContext(), "Cần có kết nối mạng", Toast.LENGTH_LONG).show();
        } else {
            try {
                Uri uri = Uri.parse("googlechrome://navigate?url=" + LINK_FANPAGE);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);

                if (i.resolveActivity(this.getPackageManager()) == null) {
                    i.setData(Uri.parse(LINK_FANPAGE));
                }
                this.startActivity(i);

            } catch (ActivityNotFoundException e) {
                // Chrome is not installed
                Toast.makeText(getApplicationContext(), "Cần có ứng dụng Chrome để mở", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void checkAdmin() {
        String dataThongTinCaNhanSV = Utils.getInstance().getValueFromSharedPreferences(this,
                Constant.SHARE_PREFERENCES_DATA,
                Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        try {
            JSONObject thongTinCaNhanSV = new JSONObject(dataThongTinCaNhanSV);
            String mssv = thongTinCaNhanSV.getString(JsonUtils.KEY_MA_SO_SV);
            if (mssv.equals("20132855")) {
                binding.loThemKhoaHoc.setVisibility(View.VISIBLE);
                binding.loThemVipMember.setVisibility(View.VISIBLE);
                binding.btnAddNoAdsMember.setOnClickListener(view -> {
                    Log.d("RAKAN", "Click: " + binding.edtThemNoAdsMember.getText().toString());
                    listNoAdsMemberDatabase.child("noAdsListMember").child(binding.edtThemNoAdsMember.getText().toString())
                            .setValue(binding.edtThemNoAdsMember.getText().toString());
                });

                binding.btnAddVipMember.setOnClickListener(view -> {
                    listVipMemberDatabase.child("danhSach").child(binding.edtThemVipMember.getText().toString())
                            .setValue(binding.edtThemVipMember.getText().toString());
                });

            } else {
                binding.loThemKhoaHoc.setVisibility(View.GONE);
                binding.loThemVipMember.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialog7DayToTurnOff(int soNgayConLai) {
        Utils.getInstance().showNoticeDialog(this, getString(R.string.chu_y), "Để đảm bảo công bằng, sau khi bật chia sẻ điểm thì sẽ cần phải chờ 7 ngày để có thể tắt lại. Thím có thể tắt chia sẻ điểm sau " + soNgayConLai + " ngày nữa!");
    }

    private void postPermissionState(boolean isChecked, String currentDay) {
        if (!Utils.getInstance().isOnline(getApplicationContext())) {
            Utils.getInstance().showNoticeDialog(SettingsActivity.this, "Chú ý", "Cần có kết nối mạng để thực hiện cài đặt này nhé thím ^.^");
        } else {
            String thongTinSV = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
            String mssv;
            try {
                JSONObject thongTinSVJsonO = new JSONObject(thongTinSV);
                mssv = thongTinSVJsonO.getString(JsonUtils.KEY_MA_SO_SV).trim();
                rankingDatabase.child("k" + khoaHoc).child(mssv).child("private").setValue(!isChecked);
                rankingDatabase.child("k" + khoaHoc).child(mssv).child("startDayTurnOn").setValue(currentDay);
            } catch (Exception e) {
                Log.e("RAKAN", "sendDataToTopStudent: " + e.toString());
                e.printStackTrace();
            }
        }
    }

    private void initValueSwitchButton() {
        if (!Utils.getInstance().isOnline(getApplicationContext())) {
            Toast.makeText(this, "Không có kết nối mạng để cập nhật trạng thái của nút đăng ký xem HUST Ranking", Toast.LENGTH_LONG).show();
        }
        String mssv;
        String studentInfoData = Utils.getInstance().getValueFromSharedPreferences(this, Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
        try {
            JSONObject studentInfoJsonO = new JSONObject(studentInfoData);
            mssv = studentInfoJsonO.getString(JsonUtils.KEY_MA_SO_SV);
            khoaHoc = Utils.getInstance().getKhoaHoc(SettingsActivity.this);
            rankingDatabase.child("k" + khoaHoc).child(mssv).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    HustRankingModel hustRankingModel = snapshot.getValue(HustRankingModel.class);
                    if (hustRankingModel != null) {
                        boolean isPrivate = hustRankingModel.isPrivate();
                        binding.btnSetUserRankingPrivate.setChecked(!isPrivate);
                        startDayTurnOn = hustRankingModel.getStartDayTurnOn();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}