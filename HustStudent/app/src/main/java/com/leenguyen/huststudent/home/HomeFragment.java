package com.leenguyen.huststudent.home;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.leenguyen.huststudent.Constant;
import com.leenguyen.huststudent.MainActivity;
import com.leenguyen.huststudent.R;
import com.leenguyen.huststudent.databinding.DialogRotateImageBinding;
import com.leenguyen.huststudent.databinding.FragmentHomeBinding;
import com.leenguyen.huststudent.model.QuangCaoModel;
import com.leenguyen.huststudent.utils.JsonUtils;
import com.leenguyen.huststudent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment implements View.OnClickListener {
    private FragmentHomeBinding binding;
    private static final double DIEM_XUAT_SAC = 3.6;
    private static final double DIEM_GIOI = 3.2;
    private static final double DIEM_KHA = 2.5;
    private static final double DIEM_TRUNG_BINH = 2.0;
    private static final double DIEM_KHONG = 0;
    private String dataThongTinSV, dataTongKet;
    public static final int PICK_IMAGE = 1;
    private float rotateAngle;
    private ReviewInfo reviewInfo;
    private boolean isVip;
    private DatabaseReference quangCaoDatabase;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        if (getContext() != null) {
            dataThongTinSV = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_THONG_TIN_SINH_VIEN);
            dataTongKet = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_TONG_KET);
        }
        quangCaoDatabase = FirebaseDatabase.getInstance("https://hust-student-ad.firebaseio.com/").getReference();
        showGoogleInAppReview();
        if (getActivity() != null) {
            isVip = ((MainActivity) getActivity()).isVip();
            binding.imgVip.setVisibility(isVip ? View.VISIBLE : View.GONE);
        }
        initLayout();
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initLayout() {
        displayAvatar();
        showTenSV();
        drawChartSoTinChi();
        drawChartCpa();
        drawChartGpa();
        showThongTinTongKet();
        showSoThongBao();

        binding.imgAvatar.setOnClickListener(this);
        binding.loThongbao.setOnClickListener(this);
    }

    private void showSoThongBao() {
        quangCaoDatabase.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                binding.txtSoThongBao.setText(String.valueOf(snapshot.getChildrenCount()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void displayAvatar() {
        if (getContext() != null) {
            String encodedImage = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR);
            String rotateAngle = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR_ROTATE_ANGLE);
            if (encodedImage != null && !encodedImage.equals("")) {
                byte[] imageAsBytes = Base64.decode(encodedImage.getBytes(), Base64.DEFAULT);
                binding.imgAvatar.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
                binding.imgAvatar.setRotation(Float.parseFloat(rotateAngle));
            }
        }
    }

    private void showTenSV() {
        if (dataThongTinSV == null || dataThongTinSV.equals("")) {
            return;
        }
        try {
            JSONObject dataThongTinSVJsonO = new JSONObject(dataThongTinSV);
            binding.txtTenSV.setText(dataThongTinSVJsonO.getString(JsonUtils.KEY_HO_TEN_SV));
            binding.txtMssv.setText(dataThongTinSVJsonO.getString(JsonUtils.KEY_MA_SO_SV));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showThongTinTongKet() {
        if (dataTongKet == null || dataTongKet.equals("")) {
            return;
        }
        try {
            JSONArray dataTongKetJsonA = new JSONArray(dataTongKet);
            if (dataTongKetJsonA.length() > 0) {
                JSONObject thongTinTongKetLast = dataTongKetJsonA.getJSONObject(0);
                binding.txtCPA.setText(thongTinTongKetLast.getString(JsonUtils.KEY_CPA));
                binding.txtSoTCTichLuy.setText(thongTinTongKetLast.getString(JsonUtils.KEY_TC_TICH_LUY));
                binding.txtSoTinChiNo.setText(thongTinTongKetLast.getString(JsonUtils.KEY_TC_NO_DANG_KY));
                binding.txtMucCanhCao.setText(thongTinTongKetLast.getString(JsonUtils.KEY_MUC_CANH_CAO));
                double cpa = Double.parseDouble(thongTinTongKetLast.getString(JsonUtils.KEY_CPA));
                if (cpa >= DIEM_XUAT_SAC) {
                    binding.rating.setImageResource(R.drawable.ic_rating_5);
                } else if (cpa >= DIEM_GIOI) {
                    binding.rating.setImageResource(R.drawable.ic_rating_4);
                } else if (cpa >= DIEM_KHA) {
                    binding.rating.setImageResource(R.drawable.ic_rating_3);
                } else if (cpa >= DIEM_TRUNG_BINH) {
                    binding.rating.setImageResource(R.drawable.ic_rating_2);
                } else if (cpa > DIEM_KHONG) {
                    binding.rating.setImageResource(R.drawable.ic_rating_1);
                } else {
                    binding.rating.setImageResource(R.drawable.ic_rating_0);
                }

                JSONObject dataThongTinSVJsonO = new JSONObject(dataThongTinSV);
                String tinhTrangHocTap = dataThongTinSVJsonO.getString(JsonUtils.KEY_TINH_TRANG_HOC_TAP);
                if (tinhTrangHocTap.contains("Tốt nghiệp")) {
                    binding.txtTrinhDo.setText(dataThongTinSVJsonO.getString(JsonUtils.KEY_TINH_TRANG_HOC_TAP));
                    binding.txtTrinhDo.setSelected(true);
                } else {
                    binding.txtTrinhDo.setText(thongTinTongKetLast.getString(JsonUtils.KEY_TRINH_DO));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void drawChartGpa() {
        if (dataTongKet == null || dataTongKet.equals("")) {
            return;
        }
        try {
            JSONArray dataTongKetJsonA = new JSONArray(dataTongKet);
            if (dataTongKetJsonA.length() > 0) {
                ArrayList<Entry> entriesGPA = new ArrayList<>();
                ArrayList<String> labels = new ArrayList<>();
                String gpaString;
                float gpa;
                for (int i = dataTongKetJsonA.length() - 1; i >= 0; i--) {
                    JSONObject dataTongKetHocKyJsonO = dataTongKetJsonA.getJSONObject(i);
                    gpaString = dataTongKetHocKyJsonO.getString(JsonUtils.KEY_GPA);
                    if (!gpaString.equals(" ")) {
                        gpa = Float.parseFloat(gpaString);
                    } else {
                        gpa = 0;
                    }

                    entriesGPA.add(new Entry(gpa, dataTongKetJsonA.length() - 1 - i));
                    String kiHoc = dataTongKetHocKyJsonO.getString(JsonUtils.KEY_HOC_KY);
                    labels.add(kiHoc);
                }
                LineDataSet dataSetGPA = new LineDataSet(entriesGPA, "GPA");
                dataSetGPA.setValueTextSize(12);
                LineData dataLineCPA = new LineData(labels, dataSetGPA);
                dataSetGPA.setColors(ColorTemplate.JOYFUL_COLORS);
                dataSetGPA.setLineWidth(2);
                binding.chartGpa.setData(dataLineCPA);
                binding.chartGpa.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                binding.chartGpa.setDescription("");
                binding.chartGpa.getXAxis().setTextSize(12);
                binding.chartGpa.animateY(1000);
            }
        } catch (Exception e) {
            Log.e("RAKAN", "drawChartGpa: " + e.toString());
            e.printStackTrace();
        }
    }

    private void drawChartCpa() {
        if (dataTongKet == null || dataTongKet.equals("")) {
            return;
        }
        try {
            JSONArray dataTongKetJsonA = new JSONArray(dataTongKet);
            if (dataTongKetJsonA.length() > 0) {
                ArrayList<Entry> entriesCPA = new ArrayList<>();
                ArrayList<String> labels = new ArrayList<>();
                for (int i = dataTongKetJsonA.length() - 1; i >= 0; i--) {
                    JSONObject dataTongKetHocKyJsonO = dataTongKetJsonA.getJSONObject(i);
                    float cpa = Float.parseFloat(dataTongKetHocKyJsonO.getString(JsonUtils.KEY_CPA));
                    entriesCPA.add(new Entry(cpa, dataTongKetJsonA.length() - 1 - i));
                    String kiHoc = dataTongKetHocKyJsonO.getString(JsonUtils.KEY_HOC_KY);
                    labels.add(kiHoc);
                }
                LineDataSet dataSetCPA = new LineDataSet(entriesCPA, "CPA");
                dataSetCPA.setValueTextSize(12);
                dataSetCPA.setLineWidth(2);
                LineData dataLineCPA = new LineData(labels, dataSetCPA);
                dataSetCPA.setColors(ColorTemplate.JOYFUL_COLORS);
                binding.chartCpa.setData(dataLineCPA);
                binding.chartCpa.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                binding.chartCpa.setDescription("");
                binding.chartCpa.getXAxis().setTextSize(12);
                binding.chartCpa.animateY(1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void drawChartSoTinChi() {
        if (getContext() == null) {
            return;
        }
        String dataBangDiemHP = Utils.getInstance().getValueFromSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_BANG_DIEM_HOC_PHAN);
        if (dataBangDiemHP == null || dataBangDiemHP.equals("")) {
            return;
        }
        int soTinChi_Aplus = 0;
        int soTinChi_A = 0;
        int soTinChi_Bplus = 0;
        int soTinChi_B = 0;
        int soTinChi_Cplus = 0;
        int soTinChi_C = 0;
        int soTinChi_Dplus = 0;
        int soTinChi_D = 0;
        int soTinChi_F = 0;
        try {
            JSONArray dataBangDiemHPJsonA = new JSONArray(dataBangDiemHP);
            if (dataBangDiemHPJsonA.length() > 0) {
                for (int i = 0; i < dataBangDiemHPJsonA.length(); i++) {
                    JSONObject dataHPJsonO = dataBangDiemHPJsonA.getJSONObject(i);
                    String diemChu = dataHPJsonO.getString(JsonUtils.KEY_DIEM_TONG_KET);
                    int soTinChi = Integer.parseInt(dataHPJsonO.getString(JsonUtils.KEY_TIN_CHI_HP));
                    switch (diemChu) {
                        case "A+":
                            soTinChi_Aplus += soTinChi;
                            break;
                        case "A":
                            soTinChi_A += soTinChi;
                            break;
                        case "B+":
                            soTinChi_Bplus += soTinChi;
                            break;
                        case "B":
                            soTinChi_B += soTinChi;
                            break;
                        case "C+":
                            soTinChi_Cplus += soTinChi;
                            break;
                        case "C":
                            soTinChi_C += soTinChi;
                            break;
                        case "D+":
                            soTinChi_Dplus += soTinChi;
                            break;
                        case "D":
                            soTinChi_D += soTinChi;
                            break;
                        case "F":
                            soTinChi_F += soTinChi;
                            break;
                    }
                }
            }

            ArrayList<BarEntry> entries = new ArrayList<>();
            entries.add(new BarEntry(soTinChi_Aplus, 0));
            entries.add(new BarEntry(soTinChi_A, 1));
            entries.add(new BarEntry(soTinChi_Bplus, 2));
            entries.add(new BarEntry(soTinChi_B, 3));
            entries.add(new BarEntry(soTinChi_Cplus, 4));
            entries.add(new BarEntry(soTinChi_C, 5));
            entries.add(new BarEntry(soTinChi_Dplus, 6));
            entries.add(new BarEntry(soTinChi_D, 7));
            entries.add(new BarEntry(soTinChi_F, 8));

            BarDataSet dataSet = new BarDataSet(entries, "Số tín chỉ");
            dataSet.setValueTextSize(12);
            ArrayList<String> labels = new ArrayList<>();
            labels.add("A+");
            labels.add("A");
            labels.add("B+");
            labels.add("B");
            labels.add("C+");
            labels.add("C");
            labels.add("D+");
            labels.add("D");
            labels.add("F");
            BarData barData = new BarData(labels, dataSet);
            barData.setValueFormatter(new MyValueFormatter()); //float to integer
            binding.chartSoTinChi.setData(barData);
            binding.chartSoTinChi.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
            dataSet.setColors(ColorTemplate.JOYFUL_COLORS);
            binding.chartSoTinChi.getXAxis().setTextSize(12);
            binding.chartSoTinChi.animateY(1000);
            binding.chartSoTinChi.setDescription("");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgAvatar) {
            if (isStoragePermissionGranted()) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        } else if (v.getId() == R.id.loThongbao) {
            Utils.getInstance().sendIntentActivity(getContext(), TrungTamTinNhanActivity.class);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            if (requestCode == PICK_IMAGE && getActivity() != null && data.getData() != null) {
                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    showDialogEditImage(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showDialogEditImage(final Bitmap bitmap) {
        if (getContext() == null) {
            return;
        }
        final Dialog dialog = new Dialog(getContext());
        final DialogRotateImageBinding bindingDialog = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_rotate_image, null, false);
        dialog.setContentView(bindingDialog.getRoot());
        bindingDialog.image.setImageBitmap(bitmap);

        rotateAngle = 0;
        bindingDialog.rolLeft.setOnClickListener(v -> {
            rotateAngle = bindingDialog.image.getRotation() - 90;
            bindingDialog.image.setRotation(bindingDialog.image.getRotation() - 90);
        });

        bindingDialog.rolRight.setOnClickListener(v -> {
            rotateAngle = bindingDialog.image.getRotation() + 90;
            bindingDialog.image.setRotation(bindingDialog.image.getRotation() + 90);
        });

        bindingDialog.btnCancel.setOnClickListener(view -> dialog.dismiss());

        bindingDialog.btnSave.setOnClickListener(view -> {
            binding.imgAvatar.setImageBitmap(bitmap);
            binding.imgAvatar.setRotation(rotateAngle % 360);
            bindingDialog.btnCancel.setClickable(false);
            bindingDialog.btnSave.setClickable(false);
            bindingDialog.llRotate.setClickable(false);
//              Save
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            if (getContext() != null) {
                Utils.getInstance().saveToSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR, encodedImage);
                Utils.getInstance().saveToSharedPreferences(getContext(), Constant.SHARE_PREFERENCES_DATA, Constant.KEY_SHARE_PREFERENCES_DATA_AVATAR_ROTATE_ANGLE, String.valueOf(rotateAngle % 360));
            }
            dialog.dismiss();
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    //format float to integer
    private static class MyValueFormatter implements ValueFormatter {
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return Math.round(value) + "";
        }
    }

    private void showGoogleInAppReview() {
        if (getContext() == null || getActivity() == null) {
            return;
        }
        ReviewManager reviewManager = ReviewManagerFactory.create(getContext());
        Task<ReviewInfo> request = reviewManager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // We can get the ReviewInfo object
                reviewInfo = task.getResult();
            }
        });


        if (reviewInfo != null && getActivity() != null) {
            Task<Void> flow = reviewManager.launchReviewFlow(getActivity(), reviewInfo);
            flow.addOnCompleteListener(task -> {
                // The flow has finished. The API does not indicate whether the user
                // reviewed or not, or even whether the review dialog was shown. Thus, no
                // matter the result, we continue our app flow.
            });
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkReadWriteExternalStorePermission()) {
                return true;
            } else {
                ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    private boolean checkReadWriteExternalStorePermission() {
        if (getActivity() == null) {
            return false;
        }
        int write = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }
}
