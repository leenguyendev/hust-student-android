package com.leenguyen.huststudent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;
import com.leenguyen.huststudent.databinding.ActivityWebviewBinding;
import com.leenguyen.huststudent.utils.Utils;

public class WebviewActivity extends AppCompatActivity {
    private ActivityWebviewBinding binding;
    private String link, title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_webview);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        Intent intent = getIntent();
        link = intent.getStringExtra("link");
        title = intent.getStringExtra("title");
        binding.txtTitle.setText(title);
        if (Utils.getInstance().isOnline(getApplicationContext())) {
            if (link.equals("https://digilux.vn/cach-lay-link-facebook-tren-dien-thoai/")) {
                binding.txtTitle.setText("Hướng dẫn lấy link Facebook");
            }
            openWebView(link);
        } else {
            Toast.makeText(this, "Bật mạng lên đi thím", Toast.LENGTH_LONG).show();
        }

        binding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void openWebView(String link) {
        binding.webView.setWebViewClient(new MyBrowser());
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setLoadWithOverviewMode(true);
        binding.webView.getSettings().setDomStorageEnabled(true);
        binding.webView.getSettings().setUseWideViewPort(true);
        binding.webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        binding.webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        binding.webView.getSettings().setAppCacheEnabled(true);
        binding.webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        binding.webView.getSettings().setUseWideViewPort(true);
        binding.webView.getSettings().setSavePassword(true);
        binding.webView.getSettings().setSaveFormData(true);
        binding.webView.getSettings().setEnableSmoothTransition(true);
        binding.webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // chromium, enable hardware acceleration
            binding.webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            binding.webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        binding.webView.loadUrl(link);
    }


    private static class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Utils.getInstance().hideLoadingDialog();
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.cancel();
        }
    }
}
